package modules.resource.utils;

import classes.dates.Dates;
import main.Main;
import modules.resource.classes.*;
import utils.ButtonBox;
import utils.ComboBox;
import utils.Numbers;
import utils.Strings;
import utils.ValidateData;

public class Functions {
	public static int menuCRUD() { // MENÚ CRUD
		String[] crudOptions = {"Create", "Read", "Update", "Delete"};
		return ButtonBox.buttonsOptions("CRUD menu:", "Option", crudOptions);
		
	}
	
	//////////////////////////////////////////////////////////////////////////////////
	
	public static void createResource(int resource) { // CREAR RECURSO
		final int BOOK 		= 0;
		final int ARTICLE 	= 1;
		final int MOVIE		= 2;
		
		String key		= ValidateData.inputKey("Key 000-A", "Key");
		String author 	= Strings.inputStringNoClose("Author", "Author");
		String thematic	= Strings.inputStringNoClose("Thematic", "Thematic");
		Dates release 	= ValidateData.inputDateBeforeToday("Release dd/mm/yyyy");
						//Strings.inputString("Release", "Release");
		
		if ( resource == BOOK ) { // CREAR LIBRO
			String editorial = Strings.inputStringNoClose("Editorial", "Editorial");
			boolean presentation = ButtonBox.buttonsBoolean("Presentation?", "Presentation?");
			if (presentation) { // LIBRO CON PRESENTACION
				Dates presentationStart = ValidateData.inputDateAfterToday("Presentation start");
				Dates presentationEnd = ValidateData.inputDateAfterX("Presentation end", presentationStart.toString());
				Main.resource1= new BookPresentation(key, author, thematic, release, editorial, presentationStart, presentationEnd);
			}
			else // LIBRO SIN PRESENTACION
				Main.resource1= new Book(key, author, thematic, release, editorial);
		}
		if ( resource == ARTICLE ) { // CREAR ARTÍCULO
			String means 		= Strings.inputStringNoClose("Means", "Means");
			Dates startShop 	= ValidateData.inputDate("Start shop dd/mm/yyyy");
			Dates endShop 		= ValidateData.inputDateAfterX("End shop dd/mm/yyyy", startShop.toString());
			Dates purchaseDay	= ValidateData.inputDateBetweenXandY("Purchase day dd/mm/yyyy", 
																		startShop.toString(),
																		endShop.toString());
			float unitPrice		= Numbers.inputFloat("Unit price", "Unit price");
			int quantity 		= Numbers.inputNumber("Quantity", "Quantity");
			Main.resource1= new Article(key, author, thematic, release, means, startShop, endShop, purchaseDay, unitPrice, quantity);
		}
		if ( resource == MOVIE ) { // CREAR PELÍCULA
			Dates presentationStart	= ValidateData.inputDateAfterToday("PresentationStart");
			Dates presentationEnd 	= ValidateData.inputDateAfterX("PresentationEnd", presentationStart.toString());
			Dates preview			= ValidateData.inputDateBetweenXandY("Preview dd/mm/yyyy", presentationStart.toString(), presentationEnd.toString());
			String company = Strings.inputStringNoClose("Company", "Company");
			Main.resource1= new Movie(key, author, thematic, release, presentationStart, presentationEnd, preview, company);
		}
		
	}
	
	public static String readResource() { // LEER RECURSO
		String s = "";
		if (Main.resource1 instanceof Book) { // SI ES UN LIBRO
			
			if (Main.resource1 instanceof BookPresentation) { // SI EL LIBRO TIENE PRESENTACIÓN
				String[] options = {"Author", "Thematic", "Release", "AddedDate", "PeriodReleaseAdded",
									//0			1			2			3				4
									"Editorial", "PresentationStart", "PresentationEnd", "PresentationDays", "All"};
									//5				6						7					8				9
				switch (ComboBox.comboBoxNoClose("Option \n" + "BookPresentation", "Option", options)) {
				case 0:
					s = ((BookPresentation)Main.resource1).getAuthor();
					break;
				case 1:
					s = ((BookPresentation)Main.resource1).getThematic();
					break;
				case 2:
					s = ((BookPresentation)Main.resource1).getRelease().toString();
					break;
				case 3:
					s = ((BookPresentation)Main.resource1).getAddedDate().toString();
					break;
				case 4:
					s = String.valueOf(((BookPresentation)Main.resource1).getPeriodReleaseAdded());
					break;
				case 5:
					s = ((BookPresentation)Main.resource1).getEditorial();
					break;
				case 6:
					s = ((BookPresentation)Main.resource1).getPresentationStart().toString();
					break;
				case 7:
					s = ((BookPresentation)Main.resource1).getPresentationEnd().toString();
					break;
				case 8:
					s = String.valueOf(((BookPresentation)Main.resource1).getPresentationDays());
					break;
				case 9:
					s = ((BookPresentation)Main.resource1).toString();
				}
			} else { // SI EL LIBRO NO TIENE PRESENTACIÓN
				String[] options = {"Author", "Thematic", "Release", "AddedDate", "PeriodReleaseAdded", "Editorial", "All"};
									//0			1			2			3				4					5			6
				switch (ComboBox.comboBoxNoClose("Option \n" + "Book", "Option", options)) {
					case 0:
						s = ((Book)Main.resource1).getAuthor();
						break;
					case 1:
						s = ((Book)Main.resource1).getThematic();
						break;
					case 2:
						s = ((Book)Main.resource1).getRelease().toString();
						break;
					case 3:
						s = ((Book)Main.resource1).getAddedDate().toString();
						break;
					case 4:
						s = String.valueOf(((Book)Main.resource1).getPeriodReleaseAdded());
						break;
					case 5:
						s = ((Book)Main.resource1).getEditorial();
						break;
					case 6:
						s = ((Book)Main.resource1).toString();
						break;
				}
			}
			
		} else if (Main.resource1 instanceof Article) {
			String[] options = {"Author", "Thematic", "Release", "AddedDate", "PeriodReleaseAdded", "Means", "All"};
									//0			1		2			3			4						5		6
			switch (ComboBox.comboBoxNoClose("Option \n" + "Article", "Option", options)) {
				case 0:
					s = ((Article)Main.resource1).getAuthor();
					break;
				case 1:
					s = ((Article)Main.resource1).getThematic();
					break;
				case 2:
					s = ((Article)Main.resource1).getRelease().toString();
					break;
				case 3:
					s = ((Article)Main.resource1).getAddedDate().toString();
					break;
				case 4:
					s = String.valueOf(((Article)Main.resource1).getPeriodReleaseAdded());
					break;
				case 5:
					s = ((Article)Main.resource1).getMeans();
					break;
				case 6:
					s = ((Article)Main.resource1).toString();
					break;
			}
		} else if (Main.resource1 instanceof Movie) {
			String[] options = {"Author", "Thematic", "Release", "AddedDate", "PeriodReleaseAdded", "PresentationStart",
								//0				1			2			3				4					5		
								"PresentationEnd", "PresentationDays", "Preview", "Company", "All"};
								//6						7					8		9			10
			switch (ComboBox.comboBoxNoClose("Option \n" + "Movie", "Option", options)) {
				case 0:
					s = ((Movie)Main.resource1).getAuthor();
					break;
				case 1:
					s = ((Movie)Main.resource1).getThematic();
					break;
				case 2:
					s = ((Movie)Main.resource1).getRelease().toString();
					break;
				case 3:
					s = ((Movie)Main.resource1).getAddedDate().toString();
					break;
				case 4:
					s = String.valueOf(((Movie)Main.resource1).getPeriodReleaseAdded());
					break;
				case 5:
					s = ((Movie)Main.resource1).getPresentationStart().toString();
					break;
				case 6:
					s = ((Movie)Main.resource1).getPresentationEnd().toString();
					break;
				case 7:
					s = String.valueOf(((Movie)Main.resource1).getPresentationDays());
					break;
				case 8:
					s = ((Movie)Main.resource1).getPreview().toString();
					break;
				case 9:
					s = ((Movie)Main.resource1).getCompany();
					break;
				case 10:
					s = ((Movie)Main.resource1).toString();
					break;
			}
			
		}
		return s;
	}
	
	public static void deleteResourse() { // ELIMINAR RECURSO
		 Main.resource1 = null;
	}
	
	public static void updateResource() { // MODIFICAR RECURSO
		
		if (Main.resource1 instanceof Book) { // SI EL OBJETO ES DEL TIPO LIBRO
			if (Main.resource1 instanceof BookPresentation) { // SI TIENE PRESENTACIÓN
				String[] options = {"Author", "Thematic", "Release", "AddedDate" , "Editorial", "Presentation start", "Presentation end"};
					//					0			1			2			3			4						5				6
				switch (ComboBox.comboBoxNoClose("Option:", "Option", options)) { // SI TIENE PRESENTACIÓN0
					case 0: // AUTHOR
						((BookPresentation)Main.resource1).setAuthor(Strings.inputStringNoClose("Author", "Author"));
						break;
					case 1: // THEMATIC
						((BookPresentation)Main.resource1).setThematic(Strings.inputStringNoClose("Thematic", "Thematic"));
						break;
					case 2: // RELEASE
						((BookPresentation)Main.resource1).setRelease(ValidateData.inputDate("Release"));
						break;
					case 3: // ADDEDDATE
						((BookPresentation)Main.resource1).setAddedDate(ValidateData.inputDate("AddedDate dd/mm/yyyy"));
						break;
					case 4: // EDITORIAL
						((BookPresentation)Main.resource1).setEditorial(Strings.inputStringNoClose("Editorial", "Editorial"));
						break;
					case 5: //PRESENTATION START
						((BookPresentation)Main.resource1).setPresentationStart(ValidateData.inputDateBeforeX(
								"Presentation start", 
								((BookPresentation)Main.resource1).getPresentationEnd().toString()
								));
						break;
					case 6: //PRESENTATION END
						((BookPresentation)Main.resource1).setPresentationEnd(ValidateData.inputDateAfterX("Presentation end", ((BookPresentation)Main.resource1).getPresentationStart().toString()));
						break;
				}

			} else { // SI NO TIENE PRESENTACIÓN
				String[] options = {"Author", "Thematic", "Release", "AddedDate", "Editorial"};
				
				switch (ComboBox.comboBoxNoClose("Option:", "Option", options)) {
					case 0: // AUTHOR
						((Book)Main.resource1).setAuthor(Strings.inputStringNoClose("Author", "Author"));
						break;
					case 1: // THEMATIC
						((Book)Main.resource1).setThematic(Strings.inputStringNoClose("Thematic", "Thematic"));
						break;
					case 2: // RELEASE
						((Book)Main.resource1).setRelease(ValidateData.inputDate("Release dd/mm/yyyy"));
						break;
					case 3: // ADDEDDATE
						((Book)Main.resource1).setAddedDate(ValidateData.inputDate("AddedDate dd/mm/yyyy"));
						break;
					case 4: // EDITORIAL
						((Book)Main.resource1).setEditorial(Strings.inputStringNoClose("Editorial", "Editorial"));
						break;
				}
			
			}
		
		}

		
		if (Main.resource1 instanceof Article) { // SI EL OBJETO ES DEL TIPO ARTÍCULO
			String[] options = {"Author", "Thematic", "Release", "AddedDate", "Means",
								//0				1			2		3			4
								"StartShop", "EndShop", "PurchaseDay", "Quantity", "UnitPrice"};
								//5				6			7				8			9
			switch (ComboBox.comboBoxNoClose("Option:", "Option", options)) {
				case 0: // AUTHOR
					((Article)Main.resource1).setAuthor(Strings.inputStringNoClose("Author", "Author"));
					break;
				case 1: // THEMATIC
					((Article)Main.resource1).setThematic(Strings.inputStringNoClose("Thematic", "Thematic"));
					break;
				case 2: // RELEASE
					((Article)Main.resource1).setRelease(ValidateData.inputDate("Release dd/mm/yyyy"));
					break;
				case 3: // ADDEDDATE
					((Article)Main.resource1).setAddedDate(ValidateData.inputDateAfterX(
							"AddedDate dd/mm/yyyy", 
							((Article)Main.resource1).getRelease().toString() 
							));
					break;
				case 4: // MEANS
					((Article)Main.resource1).setMeans(Strings.inputStringNoClose("Means", "Means"));
					break;
				case 5: // STARTSHOP
					((Article)Main.resource1).setStartShop(ValidateData.inputDateBeforeX("StartShop", ((Article)Main.resource1).getEndShop().toString()));
					break;
				case 6: // ENDSHOP
					((Article)Main.resource1).setEndShop(ValidateData.inputDateAfterX("EndShop", ((Article)Main.resource1).getStartShop().toString()));
					break;
				case 7: // PURCHASEDAY
					((Article)Main.resource1).setPurchaseDay(ValidateData.inputDateBetweenXandY("PurchaseDay", 
															((Article)Main.resource1).getStartShop().toString(),
															((Article)Main.resource1).getEndShop().toString()));
					break;
				case 8: // QUANTITY
					((Article)Main.resource1).setQuantity(Numbers.inputNumber("Quantity", "Quantity"));
					break;
				case 9: // UNITPRICE
					((Article)Main.resource1).setUnitPrice(Numbers.inputFloat("UnitPrice", "UnitPrice"));
					break;
			}
			
		}
		
		if (Main.resource1 instanceof Movie) { // SI EL OBJETO ES DEL TIPO PELÍCULA
			String[] options = {"Author", "Thematic", "Release", "AddedDate", "PresentationStart", "PresentationEnd", "Preview", "Company"};
								// 0		1				2		3					4				5				6			7
			
			switch (ComboBox.comboBoxNoClose("Option:", "Option", options)) {
				case 0: // AUTHOR
					((Movie)Main.resource1).setAuthor(Strings.inputStringNoClose("Author", "Author"));
					break;
				case 1: // THEMATIC
					((Movie)Main.resource1).setThematic(Strings.inputStringNoClose("Thematic", "Thematic"));
					break;
				case 2: // RELEASE
					((Movie)Main.resource1).setRelease(ValidateData.inputDate("Release dd/mm/yyyy"));
					break;
				case 3:
					((Movie)Main.resource1).setAddedDate(ValidateData.inputDateAfterX(
							"AddedDate dd/mm/yyyy", 
							((Movie)Main.resource1).getRelease().toString() 
							));
					break;
				case 4:
					((Movie)Main.resource1).setPresentationStart(ValidateData.inputDateBeforeX(
							"PresentationStart",
							((Movie)Main.resource1).getPresentationEnd().toString()
							));
					break;
				case 5:
					((Movie)Main.resource1).setPresentationEnd(ValidateData.inputDateAfterX(
							"PresentationEnd", 
							((Movie)Main.resource1).getPresentationStart().toString()
							));
					break;
				case 6:
					((Movie)Main.resource1).setPreview(ValidateData.inputDateBetweenXandY(
							"Preview", 
							((Movie)Main.resource1).getPresentationStart().toString(), 
							((Movie)Main.resource1).getPresentationEnd().toString()
							));
					break;
				case 7: // COMPANY
					((Movie)Main.resource1).setCompany(Strings.inputStringNoClose("Company", "Company"));
					break;
			}
			
		}
		
	}
	
}
