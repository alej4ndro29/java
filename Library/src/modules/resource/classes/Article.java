package modules.resource.classes;

import java.text.NumberFormat;

import classes.dates.Dates;

public class Article extends Resource{
	private String	means;
	private Dates 	startShop;
	private Dates 	endShop;
	private int 	daysShop;
	private Dates 	purchaseDay;
	private int		quantity;
	private float 	unitPrice;
	private float	totalPrice;
	
	public Article(String key, String author, String thematic, Dates release, String means,
					Dates startShop, Dates endShop, Dates purchaseDay, float unitPrice, int quantity) {
		super(key, author, thematic, release); // ATRIBUTOS DE LA CLASE Resource
		this.means 			= means;
		this.startShop		= startShop;
		this.endShop 		= endShop;
		this.purchaseDay	= purchaseDay;
		this.quantity		= quantity;
		this.unitPrice		= unitPrice;
		setDaysShop();
		setTotalPrice();
	}
	
	
	// GETTER
	public String getMeans() {
		return means;
	}
	
	public Dates getStartShop() {
		return startShop;
	}

	public Dates getEndShop() {
		return endShop;
	}
	
	public int getDaysShop() {
		return daysShop;
	}
	
	public Dates getPurchaseDay() {
		return purchaseDay;
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	public String getUnitPrice() {
		NumberFormat formatterPrice = NumberFormat.getCurrencyInstance();
		return formatterPrice.format(unitPrice);
	}

	public String getTotalPrice() {
		NumberFormat formatterPrice = NumberFormat.getCurrencyInstance();
		return formatterPrice.format(totalPrice);
	}
	
	// SETTER
	public void setMeans(String means) {
		this.means = means;
	}
	
	public void setStartShop(Dates startShop) {
		this.startShop = startShop;
		setDaysShop();
	}
	
	public void setEndShop(Dates endShop) {
		this.endShop = endShop;
		setDaysShop();
	}
	
	public void setPurchaseDay(Dates purchaseDay) {
		this.purchaseDay = purchaseDay;
	}
	
	public void setQuantity(int quantity) {
		this.quantity = quantity;
		setTotalPrice();
	}
	
	public void setUnitPrice(float unitPrice) {
		this.unitPrice = unitPrice;
		setTotalPrice();
	}
	
	public void setDaysShop() {
		daysShop = startShop.getDaysInterval(endShop.toString());
	}
	
	public void setTotalPrice() {
		float discount = 15f; // %
		totalPrice = unitPrice * quantity;
		System.out.println("totalPrice = " + totalPrice);
		if (quantity > 20) { // OFERTA DEL discount% 
			System.out.println("discount quantity");
			totalPrice = (totalPrice - (totalPrice * (discount / 100)));
			System.out.println("totalPrice = " + totalPrice);
		} else {
			System.out.println("no discount quantity");
		}
	}
	
	
	// TOSTRING
	
	public String toString() {
		return "Article [author = " 		+ getAuthor() 				+ ", \n" +
				"thematic = " 				+ getThematic() 			+ ", \n" +
				"release = " 				+ getRelease() 				+ ", \n" +
				"addedDate = " 				+ getAddedDate() 			+ ", \n" +
				"periodReleaseAdded = "		+ getPeriodReleaseAdded() 	+ ", \n" +
				"means = " 					+ means						+ ", \n" +
				"startShop = "				+ startShop					+ ", \n" +
				"endShop = "				+ endShop					+ ", \n" +
				"daysShop = "				+ daysShop					+ ", \n" +
				"purchaseDay = "			+ purchaseDay				+ ", \n" +
				"unitPrice = "				+ getUnitPrice()			+ ", \n" +
				"quantity = "				+ quantity					+ ", \n" +
				"totalPrice = "				+ getTotalPrice()			+ ", \n" +
				"]";
	}
	
}
