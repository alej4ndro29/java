package modules.resource.classes;

import classes.dates.Dates;

public class Book extends Resource {
	
	private String editorial;
	
	public Book(String key, String author, String thematic, Dates release, String editorial) {
		super(key, author, thematic, release);
		this.editorial = editorial;

	}
	
	// SETTERS 
	
	public void setEditorial (String editorial) {
		this.editorial = editorial;
	}
	
	
	// GETTERS
	
	public String getEditorial () {
		return editorial;		
	}

	
	// TOSTRING
	public String toString() {
		return 	"Book [author = " 			+ getAuthor() 				+ ", \n" +
				"thematic = " 				+ getThematic() 			+ ", \n" +
				"release = " 				+ getRelease() 				+ ", \n" +
				"addedDate = "				+ getAddedDate() 			+ ", \n" +
				"periodReleaseAdded = " 	+ getPeriodReleaseAdded()	+ ", \n" +
				"editorial = " 				+ editorial					+ 
				"]";
	}

	
}
