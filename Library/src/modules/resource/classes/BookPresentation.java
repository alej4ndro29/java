package modules.resource.classes;

import classes.dates.Dates;

public class BookPresentation extends Book {

	private Dates presentationStart;
	private Dates presentationEnd;
	private int presentationDays;
	
	public BookPresentation(String key, String author, String thematic, Dates release, String editorial, 
							Dates presentationStart, Dates presentationEnd) {
		super(key, author, thematic, release, editorial);

		this.presentationStart	= presentationStart;
		this.presentationEnd 	= presentationEnd;
		//this.presentationDays	= presentationStart.getDaysInterval(presentationEnd.toString());
		setPresentationDays();

	}

	// GETTERS
	public Dates getPresentationStart() {
		return presentationStart;
	}

	public Dates getPresentationEnd() {
		return presentationEnd;
	}

	public int getPresentationDays() {
		return presentationDays;
	}

	
	// SETTERS
	public void setPresentationStart(Dates presentationStart) {
		this.presentationStart = presentationStart;
		setPresentationDays();
	}

	public void setPresentationEnd(Dates presentationEnd) {
		this.presentationEnd = presentationEnd;
		setPresentationDays();
	}

	public void setPresentationDays() {
		this.presentationDays = presentationStart.getDaysInterval(presentationEnd.toString());
	}
	
	
	public String toString() {
		return 	"Book [author = " 			+ getAuthor() 				+ ", \n" +
				"thematic = " 				+ getThematic() 			+ ", \n" +
				"release = " 				+ getRelease() 				+ ", \n" +
				"addedDate = "				+ getAddedDate() 			+ ", \n" +
				"periodReleaseAdded = " 	+ getPeriodReleaseAdded()	+ ", \n" +
				"editorial = " 				+ getEditorial()			+ ", \n" +
				"presentationStart = "		+ presentationStart			+ ", \n" +
				"presentationEnd = " 		+ presentationEnd			+ ", \n" +
				"presentationDays = "		+ presentationDays			+
				"]";
	}
	
	
	
}