package modules.resource.classes;

import classes.dates.Dates;

public class Movie extends Resource {

	private Dates 	presentationStart;
	private Dates	presentationEnd;
	private int 	presentationDays;
	private Dates 	preview;
	private String 	company;
	
	public Movie(String key, String author, String thematic, Dates release, Dates presentationStart, Dates presentationEnd, Dates preview, String company) {
		super(key, author, thematic, release);
		this.presentationStart	= presentationStart;
		this.presentationEnd 	= presentationEnd;
		this.presentationDays	= presentationStart.getDaysInterval(presentationEnd.toString());
		this.preview			= preview;		
		this.company 			= company;
	}

	// SETTERS
	public void setPresentationStart(Dates presentationStart) {
		this.presentationStart = presentationStart;
		setPresentationDays();
	}
	
	public void setPresentationEnd(Dates presentationEnd) {
		this.presentationEnd = presentationEnd;
		setPresentationDays();
	}
	
	public void setPresentationDays() {
		this.presentationDays = presentationStart.getDaysInterval(presentationEnd.toString());
	}
	
	public void setPreview(Dates preview) {
		this.preview = preview;
	}
	
	public void setCompany(String company) {
		this.company = company;
	}
	
	
	// GETTERS
	public Dates getPresentationStart() {
		return presentationStart;
	}
	
	public Dates getPresentationEnd() {
		return presentationEnd;
	}
	
	public int getPresentationDays() {
		return presentationDays;
	}
	
	public Dates getPreview() {
		return preview;
	}
	
	public String getCompany() {
		return company;
	}
	
	
	// TOSTRING
	public String toString() {
		return "Movie [author = "	 	+ getAuthor() 				+ ", \n" +
				"thematic = " 			+ getThematic() 			+ ", \n" +
				"release = " 			+ getRelease() 				+ ", \n" +
				"addedDate = " 			+ getAddedDate() 			+ ", \n" +
				"periodReleaseAdded = " + getPeriodReleaseAdded()	+ ", \n" + 
				"presentationStart = "	+ presentationStart 		+ ", \n" +
				"presentationEnd = "	+ presentationEnd			+ ", \n" +
				"presentationDays = "	+ presentationDays			+ ", \n" +
				"preview = "			+ preview					+ ", \n" +
				"company = " 			+ company +
				"]";
	}
	
	
}
