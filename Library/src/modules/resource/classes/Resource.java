package modules.resource.classes;

import classes.dates.Dates;
import utils.ValidateData;

public abstract class Resource {
	
		private String code;
		private String author;
		private String thematic;
		private Dates release;
		private Dates addedDate;
		private int periodReleaseAdded;
		
		// CONSTRUCTOR
		public Resource(String code, String author, String thematic, Dates release) { 
			this.code = code;
			this.author = author;
			this.thematic = thematic;
			this.release = release;
			this.addedDate = ValidateData.today();
			this.periodReleaseAdded = release.getDaysInterval(addedDate.toString());
		}


		// SETTERS
		public void setAuthor(String author) {
			this.author = author;
		}
		
		public void setThematic(String thematic) {
			this.thematic = thematic;
		}
		
		public void setRelease(Dates release) {
			this.release = release;
			setPeriodReleaseAdded();
		}
		
		public void setAddedDate() {
			this.addedDate = ValidateData.today();
			setPeriodReleaseAdded();
		}
		
		public void setAddedDate(Dates addedDate) {
			this.addedDate = addedDate;
			setPeriodReleaseAdded();
		}
		
		public void setPeriodReleaseAdded() {
			this.periodReleaseAdded = release.getDaysInterval(addedDate.toString());
		}
		
		
		// GETTERS
		public String getAuthor() {
			return author;
		}
		
		public String getThematic() {
			return thematic;
		}
		
		public Dates getRelease() {
			return release;
		}
		
		public Dates getAddedDate() {
			return addedDate;
		}
		
		public int getPeriodReleaseAdded() {
			return periodReleaseAdded;
		}
		
		
		// TOSTRING
		public String toString() {
			return "Resources [author="		+ author 				+ ", \n" +
					"thematic= " 			+ thematic 				+ ", \n" +
					"release= " 			+ release.toString() 	+ ", \n" +
					"addedDate = " 			+ addedDate.toString() 	+ ", \n" +
					"periodReleaseAdded" 	+ periodReleaseAdded 	+ 
					"]";
		}
		
		
}
