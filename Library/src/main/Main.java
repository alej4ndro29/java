package main;

import javax.swing.JOptionPane;

import modules.resource.classes.Resource;
import modules.resource.utils.Functions;
import utils.ButtonBox;

public class Main {
	
	private final static int CREATE	= 0;
	private final static int READ	= 1;
	private final static int UPDATE	= 2;
	private final static int DELETE	= 3;
	
	public static Resource resource1 = null; // CREAR EL OBJETO resource1 AL QUE PUEDEN ACCEDER TODOS LOS FICHEROS.

	public static void main(String[] args) {
		int resourceOption = 0;
		boolean exit = false;
		String[] options = { "Book", "Article", "Movie"}; // 0 1 2

		do {
			resourceOption = ButtonBox.buttonsOptions("Option:", "Option", options); // GUARDAR QUE SE HA ELEGIDO
			if (resourceOption == -1) {
				exit = true;
			} else {
				switch (Functions.menuCRUD()) {
				
				case CREATE: // CREATE
					Functions.createResource(resourceOption);
					break;
					
				case READ: // READ
					if (((Resource) resource1) != null) {
						JOptionPane.showMessageDialog(null, Functions.readResource(), "Read",
													JOptionPane.INFORMATION_MESSAGE);
					} else
						JOptionPane.showMessageDialog(null, "Empty", "Empty", JOptionPane.INFORMATION_MESSAGE);
					break;
					
				case UPDATE:	// UPDATE
					if (((Resource) resource1) != null) {
					Functions.updateResource();
				} else
					JOptionPane.showMessageDialog(null, "Empty", "Empty", JOptionPane.INFORMATION_MESSAGE);
					break;
					
				case DELETE: // DELETE
					Functions.deleteResourse();
					JOptionPane.showMessageDialog(null, "Deleted", "Deleted", JOptionPane.INFORMATION_MESSAGE);
					break;
				}
					
			}
	
		} while (!exit);

	}

}
