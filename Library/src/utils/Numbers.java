package utils;

import javax.swing.JOptionPane;

public class Numbers {
	
	public static int inputNumber (String message, String title) { // SOLICITAR NÚMERO ENTERO
		String snum = "";
		int num = 0;
		boolean check = false;

		do {
			snum = JOptionPane.showInputDialog(null, message, title, JOptionPane.QUESTION_MESSAGE);
			try {
				num = Integer.parseInt(snum);
				check = true;
				System.out.println("check = " + check);
			} catch (Exception e) {
				check = false;
				System.out.println("check = " + check);
			}
		} while (!check);
		
		return num;
	}
	
	/////////////////////////
	
	public static float inputFloat (String message, String title) { // SOLICITAR NÚMERO FLOAT
		String snum = "";
		float num = 0;
		boolean check = false;

		do {
			snum = JOptionPane.showInputDialog(null, message, title, JOptionPane.QUESTION_MESSAGE);
			try {
				num = Float.parseFloat(snum);
				check = true;
				System.out.println("check = " + check);
			} catch (Exception e) {
				check = false;
				System.out.println("check = " + check);
			}
		} while (!check);
		
		return num;
	}
	
	/////////////////////////
	
	public static int inputNumberPos (String message, String title) { // SOLICITAR NÚMERO ENTERO POSITIVO
		int num = 0;
		boolean check = false;

		do {
			num = Numbers.inputNumber(message, title);
			if (num < 0) {
				check = false;
				System.out.println("El número es negativo.");
			} else { 
				check = true;
			}
		} while (!check);
		
		return num;
	}
	
	/////////////////////////
	
	public static int inputNumberDif0 (String message, String title, String error) { // SOLICITAR NÚMERO DISTINTO A 0
		int num = 0;
		boolean check = false;

		do {
			num = Numbers.inputNumber(message, title);
			if (num == 0) {
				check = false;
				JOptionPane.showMessageDialog(null, error, "Error", JOptionPane.WARNING_MESSAGE);
			} else { 
				check = true;
			}
		} while (!check);
		
		return num;
	}
	
	/////////////////////////
	
	public static int inputNumberQuest0 (String message, String title, String question, String tquestion) { // SOLICITAR NÚMERO PREGUNTAR SI ES 0
		String[] options = {"Sí", "No"}; // Sí = 0 No = 1
		int num = 0;
		int numIsZero = 0;
		boolean exit = false;
		do {
			num = Numbers.inputNumber(message, title);
			if (num == 0) {
				numIsZero = ButtonBox.buttonsOptionsNoClose(question, tquestion, options);
				if (numIsZero == 0) { // Usuario indica que quiere continuar
					exit = true;
				} else { // Usuario indica que no quiere continuar
					exit = false;
				}
			} else {
				exit = true;
			}
		} while (!exit);
		return num;
	}
	
	/////////////////////////
	
	public static int inputNumberGreaterX (String message, String title, int x) { // SOLICITAR NÚMERO MAYOR A x
			int num = 0;
			
			do {
				num = inputNumber(message, title);
			} while (num <= x);
			
			return num;
		}
	
	/////////////////////////
	
	public static int inputNumberLessX (String message, String title, int x) { // SOLICITAR NÚMERO MENOR A x
		int num = 0;
		
		do {
			num = inputNumber(message, title);
		} while (num >= x);
		
		return num;
	}
	
	/////////////////////////
	
	public static int inputBeetXandY (String message, String title, int x, int y) { // SOLICITAR NÚMERO ENTRE x <--> y
			int num = 0;
			
			if (x < y) {
				do {
					num = inputNumber(message, title);
				} while (num < x || num > y);
				
			} else {
				System.out.println(x + " must be less than " + y);
			}
			
			return num;
		}
	
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
