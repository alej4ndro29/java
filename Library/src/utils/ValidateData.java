package utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import classes.dates.Dates;

public class ValidateData {
	private static final String dateTemplate = "(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/]((175[7-9])|(17[6-9][0-9])|(1[8-9][0-9][0-9])|([2-9][0-9][0-9][0-9]))";
	private static final String keyTemplate = "([0-9]{3}-[A-Z]{1})";
	
	public static boolean validateKey(String key) { // COMPRUEBA FORMATO DEL CÓDIGO
		Pattern pattern = Pattern.compile(keyTemplate);
		Matcher matcher = pattern.matcher(key);
		return matcher.matches();
	}
	
	public static String inputKey(String message, String title) {
		String key = "";
		boolean check = true;
		do {
			check = true;
			key = Strings.inputStringNoClose(message, title);
			if (validateKey(key) == false) {
				check = false;
			} 
		} while (!check);
		return key;
	}
	
	public static boolean validateDate(String date) { // COMPRUEBA FORMATO DE LA FECHA dd/mm/yyyy
		Pattern pattern = Pattern.compile(dateTemplate);
		Matcher matcher = pattern.matcher(date);
		return matcher.matches();
	}
	
	
	public static boolean leapYear(int year) { // COMPRUEBA AÑO BISIESTO
		if ((year % 4) == 0 && (year % 100) != 0 || (year % 400) == 0)
			return true;
		else 
			return false;
	}
	
	
	public static Dates inputDate(String message) { // SOLICITA FECHA CON EL FORMATO dd/mm/yyyy
		String date = "";
		Dates dateObj = null;
		String[] parameters = null;
		int day = 0, month = 0, year = 0;
		int[] dayMonths={0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
		boolean check = true;
		do {
			check = true;
			dayMonths[2] = 28;
			date = Strings.inputStringNoClose(message, "Date");
			if (validateDate(date) == false) {
				check = false;
			} else {
				parameters = date.split("/");
				day = Integer.parseInt(parameters[0]);
				month = Integer.parseInt(parameters[1]);
				year = Integer.parseInt(parameters[2]);
				System.out.println("day = " + day);
				System.out.println("month = " + month);
				System.out.println("year = " + year);
				
				if (month == 2 && leapYear(year) == true) { // SI EL AÑO ES BISIESTO
					dayMonths[2] = 29;
				}

				if (day > dayMonths[month]) { // SI EL DIA SUPERA EL MÁXIMO DEL MES
					System.out.println("incorrect day");
					check = false;
				}
					
			}
						
		} while (!check);
		
		dateObj = new Dates(date);
		return dateObj;
	}
	
	
	public static Dates inputDateBeforeToday (String message) { // SOLICITAR FECHA ANTES DE HOY
		Dates dateObj = null;
		boolean check = false;
		do {
			dateObj = inputDate(message);
			if (!dateObj.isAfterToday())
				check = true;
		} while (!check);
		return dateObj;
	}
	
	
	public static Dates inputDateAfterToday (String message) { // SOLICITAR FECHA DESPUÉS DE HOY
		Dates dateObj = null;
		boolean check = false;
		do {
			dateObj = inputDate(message);
			if (dateObj.isAfterToday())
				check = true;
		} while (!check);
		return dateObj;
	}
	
	
	public static Dates inputDateAfterX (String message, String date1) { // INTRODUCIR FECHA DESPUES DE X
		Dates dateObj = null; // DEBE SER DESPUÉS DE dateObj (DATE2)
		Dates dateComp = new Dates(date1); // DATE 1
		boolean check = false;
		do {
			dateObj = inputDate(message);
			if (dateComp.getDaysInterval(dateObj.toString()) > 0)
				check = true;
		} while (!check);
		return dateObj;
	}

	
	public static Dates inputDateBeforeX (String message, String date1) { // INTRODUCIR FECHA ANTES DE X
		Dates dateObj = null; // DEBE SER DESPUÉS DE dateObj (DATE2)
		Dates dateComp = new Dates(date1); // DATE 1
		boolean check = false;
		do {
			dateObj = inputDate(message);
			if (dateComp.getDaysInterval(dateObj.toString()) < 0)
				check = true;
		} while (!check);
		return dateObj;
	}
	
	
	public static Dates inputDateBetweenXandY (String message, String x, String y) {
		Dates dateObj = null;
		boolean check = false;
			do {
				dateObj = inputDate(message);
				// System.out.println("dateObj.isAfterOrEqualX(x) = " + dateObj.isAfterOrEqualX(x));
				// System.out.println("dateObj.isBeforeOrEqualX(y) = " + dateObj.isBeforeOrEqualX(y));
				if (dateObj.isAfterOrEqualX(x) && dateObj.isBeforeOrEqualX(y))
					check = true;
			} while (!check);
		return dateObj;
	}
	
	
	public static Dates today() { // PASA LA FECHA DE HOY
		Dates dateObj = new Dates("01/01/2000"); 
		dateObj.setToday();
		return dateObj;
	}
	
}








