package classes.dates;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;


public class Dates {
	private int day, day1;
	private int month, month1;
	private int year, year1;
	private LocalDate date, date1 = null;
	SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	
	
	
	public Dates (String date) { // CONSTRUCTOR
		String dmy[] = date.split("/");
		this.day 	= Integer.parseInt(dmy[0]);
		this.month	= Integer.parseInt(dmy[1]);
		this.year	= Integer.parseInt(dmy[2]);
		
		this.date = LocalDate.of(year, month, day); 
	}
	
	private void secondaryDate (String date1) {
		String dmy[] = date1.split("/");
		this.day1 	= Integer.parseInt(dmy[0]);
		this.month1	= Integer.parseInt(dmy[1]);
		this.year1	= Integer.parseInt(dmy[2]);
		
		this.date1 = LocalDate.of(year1, month1, day1);
	}
	
	// GETTERS
	public int getDay() {
		return this.day;
	}
	
	public int getMonth() {
		return this.month;
	}

	public int getYear() {
		return this.year;
	}

	public int getDaysInterval(String date1) { // INTERVALO ENTRE DOS FECHAS
		secondaryDate(date1);
		return (int) ChronoUnit.DAYS.between(this.date, this.date1);
	}
	
	// SETTERS
	public void setDay(int day) {
		this.day = day;
		this.date = LocalDate.of(year, month, day);
	}

	public void setMonth(int month) {
		this.month = month;
		this.date = LocalDate.of(year, month, day);
	}

	public void setYear(int year) {
		this.year = year;
		this.date = LocalDate.of(year, month, day);
	}
	
	public void setToday() { // FECHA --> HOY
		this.date = LocalDate.now();
	}
	
	
	public void addDays(int days) { // SUMAR x DÍAS
		this.date = this.date.plusDays(days);
	}
    
	public void minusDays(int days) { // RESTAR x DÍAS
		this.date = this.date.minusDays(days);
	}
	
	
	public String toString() {
		//System.out.println("date = " + date);
		return formatter.format(date);

	}
	
	
	public boolean isBeforeOrEqualX(String date1) { // TRUE SI LA FECHA ESTÁ ANTES DE X 
		secondaryDate(date1);
		if (this.date.isBefore(this.date1) || this.date.isEqual(this.date1))
			return true;
		else
			return false;
	}
	
	public boolean isAfterOrEqualX(String date1) { // TRUE SI LA FECHA ESTÁ DESPUÉS DE X
		secondaryDate(date1);
		if (this.date.isAfter(this.date1) || this.date.isEqual(this.date1))
			return true;
		else
			return false;
	}
	
	public boolean isEqualX(String date1) {
		secondaryDate(date1);
		return this.date.isEqual(this.date1);
	}
	
	public boolean isAfterToday() { // TRUE SI LA FECHA PASA HOY
		return this.date.isAfter(LocalDate.now());
	}
	
	public boolean isToday() { // ¿ES HOY?
		if (ChronoUnit.DAYS.between(this.date, LocalDate.now()) == 0)
			return true;
		else
			return false;
	}

	
}
