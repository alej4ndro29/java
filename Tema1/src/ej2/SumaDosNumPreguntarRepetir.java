package ej2;

import javax.swing.JOptionPane;

public class SumaDosNumPreguntarRepetir {

	public static void main(String[] args) {
		String snum = "";
		int repetir = 0, num = 0, suma = 0;
		boolean correct = false;		
		
		//BUCLE PREGUNTA REPETIR
		do {
			suma = 0;
			//SOLICITAR 2 NUMEROS
			for (int i = 0 ; i < 2 ; i++) {
				System.out.println("Repetición " + (i+1));
				//SOLICITAR 1 NUMERO
				do {
					snum = JOptionPane.showInputDialog(null, "Introduce número " + (i+1) + "º");
					try {
						num = Integer.parseInt(snum);
						System.out.println("correct = true");
						correct = true;
					} catch (Exception e) {
						System.out.println("correct = false");
						correct = false;
					}
				} while (correct != true);
				System.out.println("num = " + num);

				//SUMAR NUMERO
				suma = suma + num;
			}
			JOptionPane.showMessageDialog(null, "La suma es " + suma, "Resultado", JOptionPane.INFORMATION_MESSAGE);
			
			//PREGUNTAR SI SE REPITE
			repetir = JOptionPane.showOptionDialog(
					null, 
					"¿Repetir?",
					"¿Repetir?",
					JOptionPane.YES_NO_CANCEL_OPTION,
					JOptionPane.QUESTION_MESSAGE,
					null,
					new Object[] {"Sí" , "No"}, //Sí = 0, No = 1
					null);
		} while (repetir == 0);
	}

}
