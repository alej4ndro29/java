package ej2;

import javax.swing.JOptionPane;

public class ParImparPosNeg {

	public static void main(String[] args) {
		String snum = "" , cad = "";
		int num = 0;
		boolean correct = false;
		
		//SOLICITAR NÚMERO
		do {
			snum = JOptionPane.showInputDialog(null, "Introduce un número.");
			try {
				num = Integer.parseInt(snum);
				correct = true;
			} catch (Exception e) {
				System.out.println("Número incorrecto");
				correct = false;
			}
		} while (correct != true);
		
		//COMPROBAR CARACTERÍSTICAS DEL NÚMERO
		if (num < 0) {
			cad = (num + " es negativo.");
		} else if ((num % 2) == 0) {
			cad = (num + " es positivo par.");
		} else {
			cad = (num + " es positivo impar.");
		}
		
		//MOSTRAR RESULTADO
		JOptionPane.showMessageDialog(null, cad, "Resultado", JOptionPane.INFORMATION_MESSAGE);
	}

}
