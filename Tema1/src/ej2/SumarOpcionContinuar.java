package ej2;

import javax.swing.JOptionPane;

public class SumarOpcionContinuar {

	public static void main(String[] args) {
		String snum = "";
		int continuar_op = 0 , num = 0 , suma = 0;	
		boolean correct = false;
		
		do {
			
			//SOLICITAR NUMERO
			do {
				snum = JOptionPane.showInputDialog(null, "Introduce un número.");
				try {
					num = Integer.parseInt(snum);
					correct = true;
					System.out.println("correct --> true");
				} catch (Exception e) {
					correct = false;
					System.out.println("correct --> false");
				}
			} while (correct != true);
			
			//REALIZAR SUMA
			suma = suma + num;
			System.out.println("suma = " + suma);
			
			//OPCIÓN CONTINUAR
			continuar_op = JOptionPane.showOptionDialog(
					null,
					"¿Continuar?",
					"¿Continuar?",
					JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE,
					null,
					new Object[] {"Sí" , "No"}, //Sí = 0, No = 1
					null);
			System.out.println("continuar_op = " + continuar_op);
		} while (continuar_op == 0);
		
		//MOSTRAR RESULTADO
		JOptionPane.showMessageDialog(null, "La suma es " + suma, "Resultado", JOptionPane.INFORMATION_MESSAGE);
	}

}
