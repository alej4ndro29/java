package ej2;

import javax.swing.JOptionPane;

public class MultCincoPrimerosImpares {
	public static void main(String[] args) {
		String snum = "";
		int num = 0, cont_imp = 0;
		int multiplica = 1;
		boolean correct = false;
		
		
		while (cont_imp < 5) {
			System.out.println("cont_imp = " + cont_imp);
			
			//SOLICITAR NÚMERO
			do {
				snum = JOptionPane.showInputDialog(null, "Has introducido " + cont_imp + " números impares." + "\n" + "Introduce número" );
				try {
					num = Integer.parseInt(snum);
					correct = true;
					System.out.println("correct = "+ correct);
				} catch (Exception e ) {
					correct = false;
					System.out.println("correct = "+ correct);
					JOptionPane.showMessageDialog(null, "No has introducido un número válido", "Error", JOptionPane.WARNING_MESSAGE);
				}
			} while (correct != true);
			
			//COMPROBAR IMPAR
			if ((num % 2) != 0) {
				System.out.println(num + " impar");
				multiplica = (multiplica * num);
				System.out.println("multiplica = "+ multiplica);
				cont_imp++;
			} else {
				System.out.println(num + " par");
			}
			
		}
		
		//RESULTADO
		JOptionPane.showMessageDialog(null, "La multiplicación es: " + multiplica, "Resultado", JOptionPane.INFORMATION_MESSAGE);
	}

}
