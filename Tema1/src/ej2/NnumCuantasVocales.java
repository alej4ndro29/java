package ej2;

import javax.swing.JOptionPane;

public class NnumCuantasVocales {

	public static void main(String[] args) {
		//VARIABLES
		String sn = "";
		int n = 0 , v_a = 0 , v_e = 0 , v_i = 0 , v_o = 0 , v_u = 0;
		char letra = ' '; 
		boolean correcto = false;		
		
		
		//PEDIR N
		do {
			sn = JOptionPane.showInputDialog(null, "¿Cuántas veces?", "Entrada" , JOptionPane.QUESTION_MESSAGE);
			try {
				n = Integer.parseInt(sn);
				System.out.println(sn + " sn_OK");
				correcto = true;
			} catch (Exception e) {
				correcto = false;
				System.out.println(sn + " sn_ERROR");
			}
		} while (correcto != true);
		
		
		for (int i = 0 ; i < n ; i++) {
			System.out.println("Repetición " + (i+1) + " de " + n);
			//PEDIR CARACTER
			do {
				sn = JOptionPane.showInputDialog(null, "Introduce caracter " + (i+1) + "º de " + n , "Caracter", JOptionPane.QUESTION_MESSAGE);
				System.out.println(sn);
				//COMPRUEBA SI SOLO SE INTRODUCE UN CARACTER O MÁS
				if (sn.length() != 1) {
					System.out.println("No es un caracter");
					correcto = false;
				} else {
					System.out.println("Es un caracter");
					correcto = true;
					letra = sn.charAt(0);
					System.out.println("El caracter es " + letra);
				}
			} while (correcto != true);
			
			//CONTAR VOCALES
			switch (letra) {
			case 'a':
				v_a++;
				break;
			case 'e':
				v_e++;
				break;
			case 'i':
				v_i++;
				break;
			case 'o':
				v_o++;
				break;
			case 'u':
				v_u++;
				break;
			}
			
			//FALTA MOSTRAR SALIDA
			System.out.println("A E I O U");
			System.out.println(v_a + " " + v_e + " " + v_i  + " " + v_o + " " + v_u);
		}
		JOptionPane.showMessageDialog(null, "A se ha utilizado " + v_a + " veces." + "\n" +
				"E se ha utilizado " + v_e + " veces." + "\n" + 
				"I se ha utilizado " + v_i + " veces." + "\n" +
				"O se ha utilizado " + v_o + " veces." + "\n" +
				"U se ha utilizado " + v_u + " veces.",
				"Resultado" , JOptionPane.INFORMATION_MESSAGE);
	}

}
