package ej2;

import javax.swing.JOptionPane;

public class SumarSiEsPositivo {

	public static void main(String[] args) {
		String snum = "";
		int num = 0, suma = 0;
		boolean interruptor = false, correcto = false;
		
		while (interruptor != true) {
			//SOLICITAR NÚMERO
			do {
				snum = JOptionPane.showInputDialog(null, "Introduce un número.", "Entrada de número", JOptionPane.DEFAULT_OPTION);
				try {
					num = Integer.parseInt(snum);
					System.out.println(snum + " ok");
					correcto = true;
				} catch (Exception e) {
					System.out.println(snum + " mal");
					correcto = false;
				}
			} while (correcto != true);
			
			
			if (num < 0) {
				System.out.println("Negativo");
				interruptor = true;
			} else {
				System.out.println("Positivo");
				suma = (suma + num);
				System.out.println(suma);
			}
		}
		
		JOptionPane.showMessageDialog(null, "La suma de los números introducidos es " + suma , "Suma", JOptionPane.PLAIN_MESSAGE);
	}
}
