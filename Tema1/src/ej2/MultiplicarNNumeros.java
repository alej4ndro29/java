package ej2;

import javax.swing.JOptionPane;

public class MultiplicarNNumeros {

	public static void main(String[] args) {
			String snum = "";
			int n = 0 , num = 0;
			int product = 1;
			boolean correct = false;
			
			//SOLICITAR n
			do {
				snum = JOptionPane.showInputDialog(null, "¿Cuántas repeticiones?");
				try {
					n = Integer.parseInt(snum);
					System.out.println("correct = " + correct);
					correct = true;
				} catch (Exception e) {
					System.out.println("correct = " + correct);
					correct = false;
				}
			} while (correct != true);
			
			//REPETIR n VECES
			for (int i = 0 ; i < n ; i++) {
				System.out.println("Repetición " + (i+1) + " de " + n);
				//SOLICITAR NUM
				do {
					snum = JOptionPane.showInputDialog(null, "Número " + (i+1) + "º de " + n);
					try {
						num = Integer.parseInt(snum);
						System.out.println("correct --> true");
						correct = true;
					} catch (Exception e) {
						System.out.println("correct --> false");
						correct = false;
					}
				} while (correct != true);
				System.out.println("num = " + num);
				
				//CALCULAR PRODUCTO
				product = product * num;
				System.out.println("product = " + product);
			}
			
			//MOSTRAR PRODUCTO
			JOptionPane.showMessageDialog(null, "Producto de los números introducidos: " + product, "Resultado", JOptionPane.INFORMATION_MESSAGE);
	}

}
