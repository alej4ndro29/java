import javax.swing.JOptionPane;

public class Calculator {

	public static void main(String[] args) {
			String num1 = "";
			String num2 = "";
			int opcion = 0, i_num1 = 0, i_num2 = 0;
			boolean correcto = false;
			
			
			opcion = JOptionPane.showOptionDialog(
					null,
					"¿Qué deseas realizar?",
					"Operación",
					JOptionPane.YES_NO_CANCEL_OPTION,
					JOptionPane.QUESTION_MESSAGE,
					null,
					new Object[] {"+", "-", "*" , "/", "Salir"}, //0+ 1- 2* 3/ 4S
					null);
			if (opcion == 4 ) {
				System.out.println("Salir");
				System.exit(0);
			}
			
			
			//PRIMER NÚMERO
			do {
				try {
					num1 = JOptionPane.showInputDialog(null, "Primer número");
					i_num1 = Integer.parseInt(num1);
					correcto = true;
				} catch (Exception e) {
					System.out.println(num1 + " no es un número entero");
					correcto = false;
				}
			} while (correcto != true);
			
			//SEGUNDO NÚMERO
			do {
				try {
					num2 = JOptionPane.showInputDialog(null, "Segundo número");
					i_num2 = Integer.parseInt(num2);
					if (opcion == 3 && i_num2 == 0) {
						System.out.println("División entre 0");
						correcto = false;
					} else {
						correcto = true;
					}
				} catch (Exception e) {
					System.out.println(num2 + " no es un número entero");
					correcto = false;
				}
			} while (correcto != true);
			
			switch (opcion) {
				case 0:
					JOptionPane.showMessageDialog(null, "La suma de " + i_num1 + " y " + i_num2 + " es " + (i_num1 + i_num2));
					break;
				case 1:
					JOptionPane.showMessageDialog(null, "La resta de " + i_num1 + " y " + i_num2 + " es " + (i_num1 - i_num2));
					break;
				case 2:
					JOptionPane.showMessageDialog(null, "La multiplicación de " + i_num1 + " y " + i_num2 + " es " + (i_num1 * i_num2));
					break;
				case 3:
					JOptionPane.showMessageDialog(null, "La división de " + i_num1 + " y " + i_num2 + " es " + (i_num1 / i_num2));
					break;
			}
	}
}
