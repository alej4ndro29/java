package ej5;

import javax.swing.JOptionPane;

public class CuadradoSumaPosParNegImpar {

	public static void main(String[] args) {
		String snum = "";
		int num = 0 , n = 0 , suma = 0;
		int pos_par = 0, neg_impar = 0;
		int product = 1;
		boolean correct = false;
		
		//SOLICITAR n
		do {
			snum = JOptionPane.showInputDialog(null, "¿Cuántas repeticiones?");
			try {
				n = Integer.parseInt(snum);
				System.out.println("correct = " + correct);
				correct = true;
			} catch (Exception e) {
				System.out.println("correct = " + correct);
				correct = false;
			}
		} while (correct != true);


		//REPETIR n VECES
		for (int i = 0 ; i < n ; i++) {
			System.out.println("Repetición " + (i+1) + " de " + n);
			//SOLICITAR NUM
			do {
				snum = JOptionPane.showInputDialog(null, "Número " + (i+1) + "º de " + n , "Entrada" , JOptionPane.QUESTION_MESSAGE);
				try {
					num = Integer.parseInt(snum);
					correct = true;
					System.out.println("correct = " + correct);
				} catch (Exception e) {
					correct = false;
					System.out.println("correct = " + correct);
				}
			} while (correct != true);
			
			//MOSTRAR CUADRADO
			JOptionPane.showMessageDialog(null, "El cuadrado de " + num + " es " + (num*num), "Cuadrado" , JOptionPane.INFORMATION_MESSAGE);
			
			//CALCULAR SUMA
			suma = (suma + num);

			//CALCULAR PRODUCTO
			product = product * num;
			
			System.out.println("suma  = " + suma + " product = " + product);
			
			if ((num > 0) && ((num % 2) == 0)) {
				//POSITIVO PAR
				pos_par++;
			} else if ((num < 0) && ((num % 2) != 0)) {
				//NEGATIVO IMPAR
				neg_impar++;
			}
			
			System.out.println("pos_par = " + pos_par + " neg_impar = " + neg_impar);
		}
		
		JOptionPane.showMessageDialog(null, "Suma: " + suma + "\n" + "Producto: " + product + "\n" + "Positivo par: " + pos_par + "\n" + "Negativo impar: " + neg_impar, "Resultado", JOptionPane.INFORMATION_MESSAGE);
	}

}
