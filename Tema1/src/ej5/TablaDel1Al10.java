package ej5;

import javax.swing.JOptionPane;

public class TablaDel1Al10 {

	public static void main(String[] args) {
		String cad = "";
		
		for (int i = 1 ; i <= 10 ; i++) {
			//SE INDICA LA TABLA ACTUAL
			cad = ("Tabla del " + i + ":" + "\n") ;
			System.out.println("i = " + i);
			for (int j = 1 ; j <= 10 ; j++) {
				//SE CALCULA CADA VALOR DE LA TABLA
				cad = (cad + j + " x " + i + " = " + (j*i) + "\n");
				System.out.println("j = " + j);
			}
			JOptionPane.showMessageDialog(null, cad, "Tabla del " + i, JOptionPane.INFORMATION_MESSAGE);
			System.out.println(cad);
		}
	}

}
