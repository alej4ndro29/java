package ej5;

import javax.swing.JOptionPane;

public class MayorYMenor {

	public static void main(String[] args) {
		String snum = "";
		int num = 0 , mayor = 0 , menor = 0;
		boolean interrupt = false , interrupt2 = false , comprobador = false;
		
		
		while (interrupt == false) {
			//INTRODUCIR NUM
			do {
				 snum = JOptionPane.showInputDialog(null, "Introduce un número, salir con -1", "Número", JOptionPane.QUESTION_MESSAGE);
				 try {
					 num = Integer.parseInt(snum);
					 comprobador = true;
					 System.out.println("comprobador = " + comprobador);
				 } catch (Exception e) {
					 comprobador = false;
					 System.out.println("comprobador = " + comprobador);
				 }
			} while (comprobador != true);
			
			//SALIR
			if (num == -1) {
				interrupt = true;
				System.out.println("Salir");
			}
			
			//PRIMER NUMERO
			if (interrupt2 == false) {
				mayor = num;
				menor = num;
				interrupt2 = true;
			}
			
			//MENOR
			if (num != -1 && num < menor) {
				menor = num;
			}
			
			//MAYOR
			if (num != -1 && num > mayor) {
				mayor = num;
			}
			
			System.out.println("mayor = " + mayor);
			System.out.println("menor = " + menor);
			
		}
		
		//RESULTADO
		JOptionPane.showMessageDialog(null, "El número mayor ha sido " + mayor + "\n" + "El número menor ha sido " + menor, "Resultado", JOptionPane.INFORMATION_MESSAGE);
			
	}

}
