package ej4;

import javax.swing.JOptionPane;

public class DivisoresDeNum {

	public static void main(String[] args) {
		String snum = "" , cad = "";
		int num = 0;
		boolean correct = false;
		
		//SOLICITAR num
		do {
			snum = JOptionPane.showInputDialog(null, "¿Qué número?", "Entrada de datos", JOptionPane.QUESTION_MESSAGE);
			try {
				num = Integer.parseInt(snum);
				correct = true;
				if (num <= 0) {
					System.out.println("Menor o igual a 0");
					correct = false;
				}
				System.out.println("correct = " + correct);
			} catch (Exception e) {
				correct = false;
				System.out.println("correct = " + correct);
			}
		} while (correct != true);
		
		//COMPROBAR CADA NUMERO DE 1 A num
		for (int i = 1 ; i <= num ; i++) {
			if ((num % i) == 0) {
				cad = (cad + i + " es divisor de " + num + "\n");
			}
		}
		if (cad == "") {
			
		}
		
		JOptionPane.showMessageDialog(null, cad, "Divisores", JOptionPane.INFORMATION_MESSAGE);
	}
}