package ej4;

import javax.swing.JOptionPane;

public class SumarParesDeNum {

	public static void main(String[] args) {
		String snum = "" , cad = "";
		int num = 0 , aux = 0 , digit = 0 , suma = 0;;
		boolean correct = false;
		
		//SOLICITAR num
		do {
			snum = JOptionPane.showInputDialog(null, "¿Qué número?", "Entrada de datos", JOptionPane.QUESTION_MESSAGE);
			try {
				num = Integer.parseInt(snum);
				correct = true;
				System.out.println("correct = " + correct);
			} catch (Exception e) {
				correct = false;
				System.out.println("correct = " + correct);
			}
		} while (correct != true);
		
		aux = num;
		System.out.println("aux = " + aux);
		
		while (aux != 0) {
			digit = (aux % 10);
			aux = (aux / 10);
			System.out.println("digit = " + digit);
			System.out.println("aux = " + aux);
			
			if ((digit % 2) == 0) {
				System.out.println(digit + " es par");
				cad = (cad + digit + ", ");
				suma = (suma + digit);
			} else {
				System.out.println(digit + " no es par");
			}
		}
		
		JOptionPane.showMessageDialog(null, "Los dígitos pares de " + num + " son:" + "\n" + cad + "\n\n" + "La suma es " + suma, "Resultado", JOptionPane.PLAIN_MESSAGE);
	}

}
