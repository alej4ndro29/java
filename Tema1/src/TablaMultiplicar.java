import javax.swing.JOptionPane;

public class TablaMultiplicar {

	public static void main(String[] args) {
		//VARIABLES
		String snum = " " , cad = " ";
		boolean correcto = false;
		int num = 0;
		
		//SOLICITAR NÚMERO
		do {
			snum = JOptionPane.showInputDialog(null, "Introduce número");
			try {
				num = Integer.parseInt(snum);
				correcto = true;
				System.out.println(snum + " OK");
			} catch (Exception e) {
				correcto = false;
				System.out.println(snum + " no es un entero.");
			}
		} while (correcto != true);
		
		//CALCULAR TABLA
		cad = ("Tabla de " + num + ":" + "\n");
		System.out.println(cad);
		for (int i = 1 ; i <= 10 ; i++) {
			cad = (cad + num + " x " + i + " = " + (num*i) + "\n");
		}
		
		//MOSTRAR RESULTADO
		JOptionPane.showMessageDialog(null, cad, "Tabla de multiplicar", JOptionPane.INFORMATION_MESSAGE);
	}
}
