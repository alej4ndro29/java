import javax.swing.JOptionPane;

public class MayorMenor {

	public static void main(String[] args) {
		//DECLARAR VARIABLES
		String snum1 = " ", snum2 = " " , cad = " ";
		int num1 = 0 , num2 = 0 ;
		boolean correcto = false;
		
		//PEDIR PRIMER NÚMERO
		do {
			snum1 = JOptionPane.showInputDialog(null, "Introduce primer número");
			try {
				num1 = Integer.parseInt(snum1);
				correcto = true;
				System.out.println("Correcto num1 --> " + num1);
			} catch (Exception e) {
				correcto = false;
				System.out.println("Incorrecto num 1");
			}
		} while (correcto != true);
		
		//PEDIR SEGUNDO NÚMERO
		do {
			snum2 = JOptionPane.showInputDialog(null, "Introduce segundo número");
			try {
				num2 = Integer.parseInt(snum2);
				correcto = true;
				System.out.println("Correcto num2 --> " + num2);
			} catch (Exception e) {
				correcto = false;
				System.out.println("Incorrecto num 2");
			}
		} while (correcto != true);
		
		//DETECTAR MAYOR Y MENOR
		if ( num1 > num2) {
			cad = ("El número mayor es " + num1 + "\n" + "El número menor es " + num2); 
		} else if (num2 > num1) {
			cad = ("El número mayor es " + num2 + "\n" + "El número menor es " + num1);
		} else {
			cad = ("Los números son iguales");
		}
		
		//MOSTRAR SALIDA
		JOptionPane.showMessageDialog(null, cad , "Resultado", JOptionPane.INFORMATION_MESSAGE);
	}

}
