import javax.swing.JOptionPane;

public class Sumar5Num {

	public static void main(String[] args) {
		//VARIABLES
		String snum = " ";
		int num = 0 , suma = 0;
		boolean correcto = false;
		
		//REALIZAR SUMA
		for (int i = 1; i <= 5 ; i++) {
			//PEDIR Y COMPROBAR NÚMERO
			do {
				snum = JOptionPane.showInputDialog(null, "Introduce el numero " + i + "º");
				try {
					num = Integer.parseInt(snum);
					correcto = true;
				} catch (Exception e) {
					correcto = false;
				}
			} while (correcto != true);
			suma = suma + num;
		}
		
		//MOSTRAR RESULTADO
		JOptionPane.showMessageDialog(null, "El resultado de la suma es " + suma, "Resultado", JOptionPane.INFORMATION_MESSAGE);
	}
}
