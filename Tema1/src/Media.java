import javax.swing.JOptionPane;

public class Media {

	public static void main(String[] args) {
		//DECLARAR VARIABLES
		String snum1 = "", snum2 = "", snum3 = "";
		int num1 = 0, num2 = 0 , num3 = 0;
		float media = 0.0f;
		boolean correcto = false;
		
		//PEDIR PRIMER NÚMERO
		System.out.println("Primer número");
		do {
			snum1 = JOptionPane.showInputDialog(null, "Introduce primer número");
			try {
				num1 = Integer.parseInt(snum1);
				correcto = true;
				System.out.println("Correcto num1");
			} catch (Exception e) {
				correcto = false;
				System.out.println("Incorrecto num 1");
			}
		} while (correcto != true);

		//PEDIR SEGUNDO NÚMERO
		System.out.println("Pasando al segundo número");
		do {
			snum2 = JOptionPane.showInputDialog(null, "Introduce segundo número");
			try {
				num2 = Integer.parseInt(snum2);
				correcto = true;
				System.out.println("Correcto num2");
			} catch (Exception e) {
				correcto = false;
				System.out.println("Incorrecto num 2");
			}
		} while (correcto != true);
		
		//PEDIR TERCER NUMERO
		System.out.println("Pasando al tercer número");
		do {
			snum3 = JOptionPane.showInputDialog(null, "Introduce tercero número");
			try {
				num3 = Integer.parseInt(snum3);
				correcto = true;
				System.out.println("Correcto num3");
			} catch (Exception e) {
				correcto = false;
				System.out.println("Incorrecto num 3");
			}
		} while (correcto != true);
		
		//PROCESAMIENTO DE DATOS
		media = ((num1 + num2 + num3) / 3);
		System.out.println(media);
		
		//MOSTRAR RESULTADO
		JOptionPane.showMessageDialog(null, "El resultado de la media es " + media, "Resultado", JOptionPane.INFORMATION_MESSAGE);
	}
}
