import javax.swing.JOptionPane;

public class SumarHastaIntr0 {

	public static void main(String[] args) {
		//VARIABLES
		String snum = "";
		int num = 0 , suma = 0;
		boolean correct = false , interrupt = false;
		
		//BUCLE HASTA INTRODUCIR 0
		while (interrupt == false) {
			//SOLICITAR NÚMERO
			do {
				try {
					snum = JOptionPane.showInputDialog(null, "Introduce un número");
					num = Integer.parseInt(snum);
					System.out.println("Número correcto");
					correct = true;
				} catch (Exception e) {
					correct = false;
					System.out.println("Número incorrecto");
				}
			} while (correct != true);
			
			//COMPROBAR SI HAY QUE SALIR O SUMAR
			if (num == 0) {
				interrupt = true;
				System.out.println("Salir");
			} else {
				suma = (suma + num);
			}
		} //END WHILE
		JOptionPane.showMessageDialog(null, "El resultado de la suma es " + suma, "Resultado", JOptionPane.INFORMATION_MESSAGE);
	}
}
