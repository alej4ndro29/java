package obras;
public class Articulo extends Obra {
  private String revista;
  private int volumen;
  private int numero;

  public Articulo(String autor, String titulo, String tema, int anyo, String r, int v, int n){
    super(autor, titulo, tema, anyo);
    revista = r;
    volumen = v;
    numero = n;
  }
  public void asignarRevista(String rev){
    revista = rev;
  }
  public void asignarVolumen(int v){
      volumen = v;
  }
  public void asignarNumero(int n){
	  numero = n;
  }
  public String obtenerRevista(){
    return revista;
  }
  public int obtenerVolumen(){
    return volumen;
  }
  public int obtenerNumero(){
    return numero;
  }
  public String toString() {
	  return "Autor: "+obtenerAutor () + "\n"+ "T�tulo: "+obtenerTitulo () + "\n"+ 
		"Tema: "+obtenerTema () + "\n" + "A�o: "+obtenerAno ()+ "\n"+ "Revista: "+obtenerRevista() + "\n" 
		+ "Volumen: "+obtenerVolumen() + "\n" + "N�: "+obtenerNumero()+ "\n";
  }
}
