package obras;
public abstract class Obra {
  private String autor;
  private String titulo;
  private String tema;
  private int anyo;

  public Obra(String autor, String titulo, String tema, int anyo){
    this.autor = autor;
    this.titulo = titulo;
    this.tema = tema;
    this.anyo = anyo;
  }
  public void asignarAutor (String nombre) {
	  autor = nombre;
  }
  public void asignarTitulo (String t) {
	  titulo = t;
  }
  public void asignarTema (String t) {
	  tema = t;
  }
  public void asignaranyo (int a) {
	  anyo = a;
  }
  public String obtenerAutor () {
    return autor;
  }
  public String obtenerTema () {
    return tema;
  }
  public String obtenerTitulo () {
    return titulo;
  }
  public int obtenerAno () {
    return anyo;
  }
  public abstract String toString();
}
