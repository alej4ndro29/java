package obras;
import javax.swing.JOptionPane;
public class ppal {
	static int menu1(){
	 int resp1;
			String[] tipo1 = { "Libro","Art�culo","Tesis","Salir" };
			resp1 = JOptionPane.showOptionDialog(null,
					"Tipo de obra?","Inicio",0,
					JOptionPane.QUESTION_MESSAGE,null,tipo1,tipo1[0]);
	  return resp1;
	}
	static int menu2(int i){
		String s="";
		int resp2;
		if(i==1)
			s=s+"Libro";
		else if(i==2)
			s=s+"Art�culo";
		else if(i==3)
			s=s+"Tesis";

			String[] tipo2 = { "Crear","Cambiar datos","Imprimir","Volver","Salir" };
			resp2 = JOptionPane.showOptionDialog(null,
					"Pulsa la operaci�n elegida",s,0,
					JOptionPane.QUESTION_MESSAGE,null,tipo2,tipo2[0]);	
	  return resp2;
	}
	static String pedircad(int i){
		String s="";
		String cad="";
		char c=' ';
		boolean val=true;
		
		 if(i==1)
			 cad="el autor";
		 else if(i==2)
			 cad="el titulo";
		 else if(i==3)
			 cad="el tema";
		 else if(i==4)
			 cad="la editorial";
		 else if(i==5)
			 cad="la revista";
		 else if(i==6)
			 cad="el director";
		 else if(i==7)
			 cad="el grado";
		 do{
				val=true;
				try{
					s=JOptionPane.showInputDialog(null,
						"Dame "+cad+"\n",
						"Introducci�n de datos",
						JOptionPane.QUESTION_MESSAGE);	
					c=s.charAt(0);
				}catch(Exception e){
					System.err.println("Error: "+e.getMessage());
					val=false;
				}
			}while( val==false );
		return s;
	}
	static int pediranyo(){
		int anyo;
		do{
				try{
				  String s=JOptionPane.showInputDialog(null,
						"Dame el anyo de la obra (1800 - 2010)"+"\n",
						"Introducci�n de datos",
						JOptionPane.QUESTION_MESSAGE);
				  anyo=Integer.parseInt(s);
				}catch(Exception e){
					System.err.println("Error: "+e.getMessage());
					anyo=-1;
				}
		}while( ((anyo<1800)||(anyo>2010)) );
		return anyo;
	}
	static int pedirint(int i){
		String cad="";
		if(i==1)
			 cad="el volumen";
		 else if(i==2)
			 cad="el n�";
		
		int j;
		do{
				try{
				  String s=JOptionPane.showInputDialog(null,
						"Dame "+cad+" del art�culo (>0)"+"\n",
						"Introducci�n de datos",
						JOptionPane.QUESTION_MESSAGE);
				  j=Integer.parseInt(s);
				  if(j<=0)
					  j=-1;
				}catch(Exception e){
					System.err.println("Error: "+e.getMessage());
					j=-1;
				}
		}while( j==-1 );		
		return j;
	}
	static Obra pedirdatos(int i){		
		Obra o=null;
		int anyo;
		String autor,titulo,tema,ed,rev,dir,grado;
		int vol,num;
		
		if(i==1)//libro
		{
			autor=pedircad(1);
			titulo=pedircad(2);
			tema=pedircad(3);
			anyo=pediranyo();
			ed=pedircad(4);
			o=new Libro(autor,titulo,tema,anyo,ed);
		}
		else if(i==2)//art�culo
		{
			autor=pedircad(1);
			titulo=pedircad(2);
			tema=pedircad(3);
			anyo=pediranyo();
			rev=pedircad(5);
			vol=pedirint(1);
			num=pedirint(2);
			o=new Articulo(autor,titulo,tema,anyo,rev,vol,num);
		}
		else if(i==3)//tesis
		{	
			autor=pedircad(1);
			titulo=pedircad(2);
			tema=pedircad(3);
			anyo=pediranyo();
			dir=pedircad(6);
			grado=pedircad(7);
			o=new Tesis(autor,titulo,tema,anyo,dir,grado);
		}
		return o;
	}
	static void cambiardatos(Obra o){
		String s = null;
		String s1="";
		int n;

		if(o instanceof Libro){
			s1=s1+("1. Autor \n"+"2. T�tulo \n"+"3. Tema \n"+"4. Anyo \n"+"5. Editorial \n");
			do{
				try{
					s=JOptionPane.showInputDialog(null,
						"Dato a cambiar del libro"+"\n"+s1,
						"Introducci�n de datos",
						JOptionPane.QUESTION_MESSAGE);
					n=Integer.parseInt(s);
				}catch(Exception e){
					System.err.println("Error: "+e.getMessage());
					n=-1;
				}
			}while( ((n<1)||(n>5)) );
			switch(n){
				case 1:
					((Libro)o).asignarAutor(pedircad(1));
					break;
				case 2:
					((Libro)o).asignarTitulo(pedircad(2));
					break;
				case 3: 
					((Libro)o).asignarTema(pedircad(3));
					break;
				case 4: 
					((Libro)o).asignaranyo(pediranyo());
					break;
				case 5: 
					((Libro)o).asignarEditorial(pedircad(4));
					break;
			}
		}
		if(o instanceof Articulo){
			s1=s1+("1. Autor \n"+"2. T�tulo \n"+"3. Tema \n"+"4. Anyo \n"+"5. Revista \n"+"6. Volumen \n"+"7. N� \n");
			do{
				try{
					s=JOptionPane.showInputDialog(null,
						"Dato a cambiar del art�culo"+"\n"+s1,
						"Introducci�n de datos",
						JOptionPane.QUESTION_MESSAGE);
					n=Integer.parseInt(s);
				}catch(Exception e){
					System.err.println("Error: "+e.getMessage());
					n=-1;
				}
			}while( ((n<1)||(n>7)) );
			switch(n){
			case 1:
				((Articulo)o).asignarAutor(pedircad(1));
				break;
			case 2:
				((Articulo)o).asignarTitulo(pedircad(2));
				break;
			case 3: 
				((Articulo)o).asignarTema(pedircad(3));
				break;
			case 4: 
				((Articulo)o).asignaranyo(pediranyo());
				break;
			case 5: 
				((Articulo)o).asignarRevista(pedircad(5));
				break;
			case 6: 
				((Articulo)o).asignarVolumen(pedirint(1));
				break;
			case 7: 
				((Articulo)o).asignarNumero(pedirint(2));
				break;
			}
		}
		if(o instanceof Tesis){
			s1=s1+("1. Autor \n"+"2. T�tulo \n"+"3. Tema \n"+"4. Anyo \n"+"5. Director \n"+"6. Grado \n");
			do{
				try{
					s=JOptionPane.showInputDialog(null,
						"Dato a cambiar de la tesis"+"\n"+s1,
						"Introducci�n de datos",
						JOptionPane.QUESTION_MESSAGE);
					n=Integer.parseInt(s);
				}catch(Exception e){
					System.err.println("Error: "+e.getMessage());
					n=-1;
				}
			}while( ((n<1)||(n>6)) );
			switch(n){
			case 1:
				((Tesis)o).asignarAutor(pedircad(1));
				break;
			case 2:
				((Tesis)o).asignarTitulo(pedircad(2));
				break;
			case 3: 
				((Tesis)o).asignarTema(pedircad(3));
				break;
			case 4: 
				((Tesis)o).asignaranyo(pediranyo());
				break;
			case 5: 
				((Tesis)o).asignarDirector(pedircad(6));
				break;
			case 6: 
				((Tesis)o).asignarGrado(pedircad(7));
				break;
			}
		}
	}
	public static void main(String[] args) {
		int op1=0,op2 = 0,op3 = 0,op4 = 0;
		boolean val1=true;
		Libro l=null;
		Articulo a=null;
		Tesis t=null;
		
		do{
			op1=menu1();
			if(op1==0)//libro
			{
				do{
					val1=true;
					op2=menu2(1);
					switch(op2){
						case 0: //Crear
							l=(Libro)pedirdatos(1);
							break;
						case 1: //Cambiar datos
							if(l!=null)
								cambiardatos(l);
								break;
						case 2: //Imprimir
							if(l!=null)
								JOptionPane.showMessageDialog(null,
								  	l.toString(),
									"Datos del libro",
									JOptionPane.INFORMATION_MESSAGE);
							break;
						case 3://volver
							break;
						default: //Salir
								val1=false;
					}
				}while( (op2<3)&&(val1==true) );
			}
			else if(op1==1)//art�culo
			{
				do{
					val1=true;
					op3=menu2(2);
					switch(op3){
						case 0: //Crear
							a=(Articulo)pedirdatos(2);
							break;
						case 1: //Cambiar datos
							if(a!=null)
								cambiardatos(a);
								break;
						case 2: //Imprimir
							if(a!=null)
								JOptionPane.showMessageDialog(null,
								  	a.toString(),
									"Datos del art�culo",
									JOptionPane.INFORMATION_MESSAGE);
							break;
						case 3://volver
							break;
						default: //Salir
							val1=false;		
					}
				}while( (op3<3)&&(val1==true) );
			}
			else if(op1==2)//tesis
			{
				do{
					val1=true;
					op4=menu2(3);
					switch(op4){
						case 0: //Crear
							t=(Tesis)pedirdatos(3);
							break;
						case 1: //Cambiar datos
							if(t!=null)
								cambiardatos(t);
								break;
						case 2: //Imprimir
							if(t!=null)
								JOptionPane.showMessageDialog(null,
								  	t.toString(),
									"Datos de la tesis",
									JOptionPane.INFORMATION_MESSAGE);
							break;
						case 3://volver
							break;
						default: //Salir
							val1=false;		
					}
				}while( (op4<3)&&(val1==true) );
			}
			else
				val1=false;
		}while( (val1==true) );
	}
}
