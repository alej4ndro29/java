package obras;
public class Libro extends Obra {
  private String editorial;

  public Libro(String autor, String titulo, String tema, int anyo, String ed){
    super(autor, titulo, tema, anyo);
    editorial = ed;
  }
  public String obtenerEditorial(){
    return editorial;
  }
  public void asignarEditorial(String ed){
    editorial = ed;
  }
  public String toString() {
	return "Autor: "+obtenerAutor () + "\n"+ "T�tulo: "+obtenerTitulo () + "\n"+ 
	"Tema: "+obtenerTema () + "\n" + "A�o: "+obtenerAno ()+ "\n"+ "Editorial: "+obtenerEditorial()+ "\n";
  }
}
