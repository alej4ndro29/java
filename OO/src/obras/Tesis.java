package obras;
public class Tesis extends Obra {
  private String director;
  private String grado;

  public Tesis(String autor, String titulo, String tema, int anyo, String d, String g){
    super(autor, titulo, tema, anyo);
    director = d;
    grado = g;
  }
  public String obtenerDirector(){
    return director;
  }
  public void asignarDirector(String d){
    director = d;
  }
  public String obtenerGrado(){
    return grado;
  }
  public void asignarGrado(String g){
    grado = g;
  }
  public String toString() {
	  return "Autor: "+obtenerAutor () + "\n"+ "T�tulo: "+obtenerTitulo () + "\n"+ 
	  "Tema: "+obtenerTema () + "\n" + "A�o: "+obtenerAno ()+ "\n"+ "Director: "+obtenerDirector() +"\n" +
	  "Grado: "+obtenerGrado()+"\n";
  }
}
