package library;

public class Article extends Resource{
	private String means;
	
	public Article(String author, String thematic, int year, String means) {
		super(author, thematic, year);
		this.means = means;
	}
	
	
	// GETTER
	public String getMeans() {
		return means;
	}


	// SETTER
	public void setMeans(String means) {
		this.means = means;
	}

	
	// TOSTRING
	
	public String toString() {
		return "Article [author=" + getAuthor() + ", thematic=" + getThematic() + ", year=" + getYear() + ", means=" + means + "]";
	}
	
}
