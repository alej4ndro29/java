package library;

import utils.*;
import javax.swing.JOptionPane;


public class Main {
	
	public static void main(String[] args) {
		Book book1=null;
		Article article1 = null;
		Movie movie1 = null;
		int resourceOption = 0, crudOption=0;
		boolean exit=false;
		String[] options = {"Book", "Article", "Movie"}; // 0 1 2
		

		do {
			resourceOption = ButtonBox.buttonsOptions("Option:", "Option", options); // GUARDAR QUE SE HA ELEGIDO
			switch (resourceOption) {
				case 0: // BOOK
					crudOption = Functions.menuCRUD("Book");
					if (crudOption == 0) { // CREATE
						book1 = (Book) Functions.createBook();
						
					} else if (crudOption == 1) { // READ
						if ( book1 != null) {
							JOptionPane.showMessageDialog(null, Functions.readBook(book1), "Read", JOptionPane.INFORMATION_MESSAGE);
						}
						
					} else if (crudOption == 2) { // UPDATE
						if ( book1 != null) {
							book1 = Functions.updateBook(book1);
						}
					} else if (crudOption == 3) { // DELETE
						book1 = null;
						JOptionPane.showMessageDialog(null, "Deleted", "Deleted", JOptionPane.INFORMATION_MESSAGE);
					}
					
					break;
					
				case 1: // ARTICLE
					crudOption = Functions.menuCRUD("Article");
					if (crudOption == 0) { // CREATE
						article1 = (Article) Functions.createArticle();
					} else if (crudOption == 1) { // READ
						if (article1 != null) {
							JOptionPane.showMessageDialog(null, Functions.readArticle(article1), "Read", JOptionPane.INFORMATION_MESSAGE);
						}
					} else if (crudOption == 2) { // UPDATE
						if (article1 != null) {
							article1 = Functions.updateArticle(article1);
						}
					} else if (crudOption == 3) { // DELETE
						article1 = null;
						JOptionPane.showMessageDialog(null, "Deleted", "Deleted", JOptionPane.INFORMATION_MESSAGE);
					}
					break;
					
				case 2: // MOVIE
					crudOption = Functions.menuCRUD("Movie");
					if (crudOption == 0) { // CREATE
						movie1 = Functions.createMovie();
					} else if (crudOption == 1) { // READ
						if (movie1 != null) {
							JOptionPane.showMessageDialog(null, Functions.readMovie(movie1), "Read", JOptionPane.INFORMATION_MESSAGE);
						}
					} else if (crudOption == 2) { // UPDATE
						if (movie1 != null) {
							movie1 = Functions.updateMovie(movie1);
						}
					} else if (crudOption == 3) { // DELETE
						movie1 = null;
						JOptionPane.showMessageDialog(null, "Deleted", "Deleted", JOptionPane.INFORMATION_MESSAGE);
					}
					break;

			default:
				exit=true;
				break;
			}
			
			
		} while(!exit);
			
		
	}

}
