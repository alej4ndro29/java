package library;

public class Resource {

		private String author;
		private String thematic;
		private int year;
		
		
		// CONSTRUCTOR
		public Resource(String author, String thematic, int year) { 
			System.out.println("Constructor");
			this.author = author;
			this.thematic = thematic;
			this.year = year;
		}


		
		// SETTERS
		
		public void setAuthor(String author) {
			this.author = author;
		}
		
		public void setThematic(String thematic) {
			this.thematic = thematic;
		}
		
		public void setYear(int year) {
			this.year = year;
		}
		
		
		
		// GETTERS
		
		public String getAuthor() {
			return author;
		}
		
		public String getThematic() {
			return thematic;
		}
		
		public int getYear() {
			return year;
		}
		
		
		// TOSTRING
		public String toString() {
			return "Resources [author=" + author + ", thematic=" + thematic + ", year=" + year + "]";
		}
		
		
}
