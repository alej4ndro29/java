package library;

import utils.ButtonBox;
import utils.Numbers;
import utils.Strings;

public class Functions {
	static int menuCRUD(String resource) { // MENÚ CRUD
		String[] crudOptions = {"Create", "Read", "Update", "Delete"};
		return ButtonBox.buttonsOptions("CRUD menu:" + "\n" + resource, "Option", crudOptions);
		
	}
	
	//////////////////////////////////////////////////////////////////////////////////
	
	static Book createBook() { // CREAR LIBRO
		String author = Strings.inputString("Author", "Author");
		String thematic = Strings.inputString("Thematic", "Thematic");
		int year = Numbers.inputBeetXandY("Year", "Year", 1800, 2018);
		String editorial = Strings.inputString("Editorial", "Editorial");
		
		return new Book(author, thematic, year, editorial);
	}
	
	static String readBook(Book object) { // MOSTRAR LIBRO
		String read = "";
		String[] options = {"Author", "Thematic", "Year", "Editorial", "All"};
		
		switch (ButtonBox.buttonsOptionsNoClose("Option:", "Option", options)) {
			case 0: // AUTHOR
				read = ("Author: " + object.getAuthor());
				break;
			case 1: // THEMATIC
				read = ("Thematic: " + object.getThematic());
				break;
			case 2: // YEAR
				read = ("Year: " + String.valueOf(object.getYear()));
				break;
			case 3: // EDITORIAL
				read = ("Editorial: " + object.getEditorial());
				break;
			case 4: // ALL
				read = object.toString();
				break;
		}
		
		return read;
		
	}
	
	static Book updateBook(Book object) { // MODIFICAR LIBRO
		
		String[] options = {"Author", "Thematic", "Year", "Editorial"};
		
		switch (ButtonBox.buttonsOptionsNoClose("Option:", "Option", options)) {
			case 0: // AUTHOR
				object.setAuthor(Strings.inputString("Author", "Author"));
				break;
			case 1: // THEMATIC
				object.setThematic(Strings.inputString("Thematic", "Thematic"));
				break;
			case 2: // YEAR
				object.setYear(Numbers.inputBeetXandY("Year", "Year", 1800, 2018));
				break;
			case 3: // EDITORIAL
				object.setEditorial(Strings.inputString("Editorial", "Editorial"));
				break;
		}
		
		return object;
	}
	
	//////////////////////////////////////////////////////////////////////////////////
	
	static Article createArticle() { // CREAR ARTÍCULO
		String author = Strings.inputString("Author", "Author");
		String thematic = Strings.inputString("Thematic", "Thematic");
		int year = Numbers.inputBeetXandY("Year", "Year", 1800, 2018);
		String means = Strings.inputString("Means", "Means");
		
		return new Article(author, thematic, year, means);
	}
	
	static String readArticle(Article object) { // LEER ARTÍCULO
		String read = "";
		String[] options = {"Author", "Thematic", "Year", "Means", "All"};
		
		switch (ButtonBox.buttonsOptionsNoClose("Option:", "Option", options)) {
			case 0: // AUTHOR
				read = object.getAuthor();
				break;
			case 1: // THEMATIC
				read = object.getThematic();
				break;
			case 2: // YEAR
				read = String.valueOf(object.getYear());
				break;
			case 3: // MEANS
				read = object.getMeans();
				break;
			case 4: // ALL
				read = object.toString();
				break;
		}
		
		return read;
	}

	static Article updateArticle (Article object) { // MODIFICAR ARTÍCULO
		String[] options = {"Author", "Thematic", "Year", "Means"};
		
		switch (ButtonBox.buttonsOptionsNoClose("Option:", "Option", options)) {
			case 0: // AUTHOR
				object.setAuthor(Strings.inputString("Author", "Author"));
				break;
			case 1: // THEMATIC
				object.setThematic(Strings.inputString("Thematic", "Thematic"));
				break;
			case 2: // YEAR
				object.setYear(Numbers.inputBeetXandY("Year", "Year", 1800, 2018));
				break;
			case 3: // MEANS
				object.setMeans(Strings.inputString("Means", "Means"));
				break;
		}
		
		return object;
	}
	
	//////////////////////////////////////////////////////////////////////////////////
	
	static Movie createMovie () { // CREAR PELÍCULA
		String author = Strings.inputString("Author", "Author");
		String thematic = Strings.inputString("Thematic", "Thematic");
		int year = Numbers.inputBeetXandY("Year", "Year", 1800, 2018);
		String company = Strings.inputString("Company", "Company");
		
		return new Movie(author, thematic, year, company);
	}
	
	static String readMovie (Movie object) { // LEER PELÍCULA
		String read = "";
		String[] options = {"Author", "Thematic", "Year", "Company", "All"};
		
		switch (ButtonBox.buttonsOptionsNoClose("Option:", "Option:", options)) {
			case 0:
				read = object.getAuthor(); 
				break;
			case 1:
				read = object.getThematic();
				break;
			case 2:
				read = String.valueOf(object.getYear());
				break;
			case 3:
				read = object.getCompany();
				break;
			case 4:
				read = object.toString();
				break;
		}
		
		return read;
	}
	
	static Movie updateMovie (Movie object) { // MODIFICAR PELÍCULA
		String[] options = {"Author", "Thematic", "Year", "Company", "All"};
		
		switch (ButtonBox.buttonsOptionsNoClose("Option:", "Option:", options)) {
			case 0:
				object.setAuthor(Strings.inputString("Author", "Author")); 
				break;
			case 1:
				object.setThematic(Strings.inputString("Thematic", "Thematic"));
				break;
			case 2:
				object.setYear(Numbers.inputBeetXandY("Year", "Year", 1800, 2018));
				break;
			case 3:
				object.setCompany(Strings.inputString("Company", "Company"));
				break;
		}
		
		return object;
		
	}
	
}
