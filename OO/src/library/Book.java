package library;

public class Book extends Resource {
	
	private String editorial;
	
	public Book(String author, String thematic, int year, String editorial) {
		super(author, thematic, year);
		this.editorial = editorial;
	}
	
	// SETTERS 
	
	public void setEditorial (String editorial) {
		this.editorial = editorial;
	}
	
	
	// GETTERS
	
	public String getEditorial () {
		return editorial;		
	}

	
	// TOSTRING
	public String toString() {
		return "Book [author=" + getAuthor() + ", thematic=" + getThematic() + ", year=" + getYear() + ", editorial=" + editorial + "]";
	}

	
}
