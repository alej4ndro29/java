package library;

public class Movie extends Resource {

	private String company;
	
	public Movie(String author, String thematic, int year, String company) {
		super(author, thematic, year);
		this.company = company;
	}

	// SETTERS
	
	public void setCompany(String company) {
		this.company = company;
	}
	
	
	// GETTERS
	
	public String getCompany() {
		return company;
	}
	
	
	// TOSTRING
	
	public String toString() {
		return "Movie [author=" + getAuthor() + ", thematic=" + getThematic() + ", year=" + getYear() + ", company=" + company + "]";
	}
	
	
}
