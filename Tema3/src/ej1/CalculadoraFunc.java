package ej1;

import javax.swing.JOptionPane;

import utils.*;

public class CalculadoraFunc {

	public static void main(String[] args) {
		int num0 = 0, num1 = 0;
		boolean exit = false;
		String[] options = {" + ", " - ", " * ", " / "}; // +0, -1, *2 , /3 
		
		do {
			switch (ButtonBox.buttonsOptions("¿Qué desea realizar?", "Operación", options)) {
				case 0: // Suma
					do {
						num0 = Numbers.inputNumber("Introducir primer valor", "Introducir número");
						num1 = Numbers.inputNumber("Introducir segundo valor", "Introducir número");
						JOptionPane.showMessageDialog(null, num0 + " + " + num1 + " = " + (num0 + num1), "Resultado", JOptionPane.INFORMATION_MESSAGE);
						
					} while (ButtonBox.continueButton() == 0);
					break;
				
				case 1: // Resta
					do {
						num0 = Numbers.inputNumber("Introducir primer valor", "Introducir número");
						num1 = Numbers.inputNumber("Introducir segundo valor", "Introducir número");
						JOptionPane.showMessageDialog(null, num0 + " - " + num1 + " = " + (num0 - num1), "Resultado", JOptionPane.INFORMATION_MESSAGE);
						
					} while (ButtonBox.continueButton() == 0);
					break;
					
				case 2: // Multiplicación
					do {
						num0 = Numbers.inputNumberQuest0("Introducir primer valor", "Introducir número", "¿Multiplicar por 0?", "Número 0");
						num1 = Numbers.inputNumberQuest0("Introducir segundo valor", "Introducir número", "¿Multiplicar por 0?", "Número 0");
						JOptionPane.showMessageDialog(null, num0 + " * " + num1 + " = " + (num0 * num1), "Resultado", JOptionPane.INFORMATION_MESSAGE);

					} while (ButtonBox.continueButton() == 0);
					break;
					
				case 3: // División
					do {
						num0 = Numbers.inputNumber("Introducir primer valor", "Introducir número");
						num1 = Numbers.inputNumberDif0("Introducir segundo valor", "Introducir número", "No se puede dividir entre 0");
						JOptionPane.showMessageDialog(null, num0 + " / " + num1 + " = " + (num0 / num1), "Resultado", JOptionPane.INFORMATION_MESSAGE);
						
					} while (ButtonBox.continueButton() == 0);
					break;
	
				default:
					exit = true;
					break;
			}
			
		} while (!exit);
		
	}

}
