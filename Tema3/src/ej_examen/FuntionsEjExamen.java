package ej_examen;

import java.util.Calendar;

import javax.swing.JOptionPane;

import utils.ButtonBox;
import utils.Digits;
import utils.Dividers;
import utils.Numbers;

public class FuntionsEjExamen {
	
	public static int ej1 () { // EJERCICIO 1
		int repeatOption = 0, num = 0, aux = 0, cont = 0, max = 4 , media = 0;
		do {
			System.out.println("Ej 1");
			cont = 1;
			
			
			while (cont <= max) {
				media = 0;
				num = Numbers.inputNumber("Input number 20 <--> 80 " + cont + "/" + max, "Number");
				aux = num;
				if (num >= 20 && num <= 80) {
					
					if (num % 2 == 0 ) { // SI SE INTRODUCE UN NÚMERO PAR
						aux = (aux + 1);
						System.out.println("media = " + media + " " + aux);
						media = (media + aux);
						for (int i = 1 ; i <= 9 ; i++) {
							aux = aux + 2;
							System.out.println("media = " + media + " " + aux);
							media = (media + aux);
						}
						
					} else { // SI SE INTRODUCE UN NÚMERO IMPAR 
						for (int i = 0 ; i < 10 ; i++) {
							System.out.println("media = " + media + " " + aux);
							media = (media + aux);
							aux = (aux + 2);
						}
					}
					
					media = media / 10;
					System.out.println("media = " + media);
					
					JOptionPane.showMessageDialog(null, "Average of " + num + " --> " + media, "Result", JOptionPane.INFORMATION_MESSAGE);
					
					cont++;
				}
			}
			
			repeatOption = ButtonBox.repeatButton();
		} while (repeatOption == 0);
		
		return repeatOption;
	}
	
	/////////////////////////
	
	public static int ej2 () { // EJERCICIO 2
		int repeatOption = 0, num = 0, sumOddDig = 0, productPairDig = 1;
		boolean exit = false;
		
		do {
			System.out.println("Ej 2");
			
			do {
				num = Numbers.inputNumber("Number: -1 for exit", "Number");
				if (num == -1) {
					exit = true;
				} else {
					productPairDig = Digits.multiplyPairDigits(num);
					sumOddDig = Digits.sumOddDigits(num);
					JOptionPane.showMessageDialog(null, "Product of pair digits --> " + productPairDig + "\n" 
												+ "Sum of odd digits --> " + sumOddDig, "Result", JOptionPane.INFORMATION_MESSAGE);
				}
			} while (!exit);
			
			
			repeatOption = ButtonBox.repeatButton();
		} while (repeatOption == 0);
		
		return repeatOption;
	}
	
	/////////////////////////
	
	public static int ej3 () { // EJERCICIO 3
		int repeatOption = 0, n = 0, num = 0;
		n = Numbers.inputNumberGreaterX("Limit value", "Limit", 1);
		
		do {
			System.out.println("Ej 3");
			num = Numbers.inputNumber("Imput number 1 <--> " + n, "Number");
			
			if (num >= 1 && num <= n) {

			} else {
				JOptionPane.showMessageDialog(null, num + " is not between the specified range (" + 1 + " <-->" + n + ")", "Error", JOptionPane.WARNING_MESSAGE);
			}
			
			
			repeatOption = ButtonBox.repeatButton();
		} while (repeatOption == 0);
		
		return repeatOption;
	}
	
	/////////////////////////
	
	public static int ej4 () { // EJERCICIO 4
		
		String cad = "";
		int repeatOption = 0 , num = 0, average = 0 , max = 0 , repeat = 0 , aux = 0 , rest = 0 , digitsSum = 0;
		boolean interrupt = false;
		do {
			System.out.println("Ej 4");
		
			while (!interrupt) {
				cad = "";
				average = 0;
				num = Numbers.inputNumber("Input number, -5 for exit", "Number");
				if (num == -5) {
					interrupt = true;
					System.out.println("interrupt = " + interrupt);
				} else {
					average = Digits.averageMultX(num, 3);
					System.out.println("average = " + average);
					// MEDIA
					if (average == 0 ) {
						cad = (cad + (num + " does not have multiples of 3 digits" + "\n"));
					} else {
						cad = (cad + (num + " average of digits multiples of 3 --> " + average + "\n"));
					}
					
					// DIGITO MAYOR Y REPETICIONES
					
					aux = num;
					num = Math.abs(num);
					
					while (num != 0) {
						rest = (num % 10);
						num = (num / 10);
						if (rest > max) {
							max = rest;
							repeat = 1;
						} else if (rest == max) {
							repeat++;
						}
					}
					
					num = aux;
					
					cad = (cad + ("Max digit is " + max + " --> " + repeat + " repetitions"));
					
					// SUMAR LOS DÍGITOS SI TIENE 2 PARES
					
					if (Digits.countDigitsPair(num) == 2) {
						digitsSum = Digits.sumDigits(num);
						cad = (cad + ("\n" + num + " has 2 pairs digits --> " + digitsSum));
					}
					
					JOptionPane.showMessageDialog(null, cad, "Result", JOptionPane.INFORMATION_MESSAGE);
				}
				
				
			}
		
		repeatOption = ButtonBox.repeatButton();
	} while (repeatOption == 0);
	
	return repeatOption;
	}
	
	/////////////////////////
	
	public static int ej5 () {
		int repeatOption = 0, n = 0 , age = 0, sexOpt = 0, civilStatusOpt = 0, yearBirth = 0 , year = 0 , movies = 0;
		//int high65 = 0, 
		Calendar calendar = Calendar.getInstance();
		year = calendar.get(Calendar.YEAR);
		String[] sex = {"H" , "M"};
		String[] civilStatus = {"Single", "Married"};
		
		do {
			System.out.println("Ej 5");
			n = Numbers.inputNumberPos("How many members?", "Members");
			
			for (int i = 0 ; i < n ; i++) {
				age = Numbers.inputNumberGreaterX("Age. -1 for exit.", "Age", -2);

				if (age == -1) { // SALIR CON -1
					i = n;
					System.out.println("Exit");
				} else {
					if (age >= 18 && age <= 99) {
						sexOpt = ButtonBox.buttonsOptionsNoClose("Sex:", "Sex", sex); // 0H 1M
						System.out.println(sexOpt);
						civilStatusOpt = ButtonBox.buttonsOptionsNoClose("Civil status:", "Civil status", civilStatus); // 0S 1M
						do {
							yearBirth = Numbers.inputNumberPos("Year of birth", "Year of birth");
						} while (yearBirth != (year - age));
						do {
							movies = Numbers.inputNumberPos("Movies", "Movies");
						} while (movies < 0 || movies > 5);
						
					}
					
				}
				
			}
			
			repeatOption = ButtonBox.repeatButton();
		} while (repeatOption == 0);
		
		return repeatOption;
	}
	
	/////////////////////////
	
	public static int ej6 () { // EJERCICIO 6
	int repeatOption = 0;
	int num = 0, chars = 0 , contA = 1, contB = 0;
	boolean palindrome = false;
	
	do {
		System.out.println("Ej 6");
		
		contA = 1;
		
		
		num = Numbers.inputNumberPos("Num", "Num");
		chars = Integer.toString(num).length();
		contB = chars;
		
		for (int i = 0 ; i < (chars / 2) ; i++) {
			System.out.println(Digits.extractDigit(num, contA) + " " + Digits.extractDigit(num, contB));
			if (Digits.extractDigit(num, contA) == Digits.extractDigit(num, contB)) {
				System.out.println("Dentro if");
				palindrome = true;
			} else {
				System.out.println("Dentro else");
				palindrome = false;
				i = chars;
			}
			
			contA++;
			contB--;
			System.out.println("contA = " + contA + " contB = " + contB);
		}
		
		System.out.println("palindrome = " + palindrome);
		
		if (!palindrome) {
			JOptionPane.showMessageDialog(null, num + " isn't palindrome");
		} else {
			JOptionPane.showMessageDialog(null, num + " is palindrome");
		}
		
	
		repeatOption = ButtonBox.repeatButton();
	} while (repeatOption == 0);
	
	return repeatOption;
	}
	
	////////////////////////	
	
	public static int ej7 () { // EJERCICIO 7
		int repeatOption = 0;
		do {
			System.out.println("Ej 7");
			
			int num = 0;
			num = Numbers.inputNumberPos("Number", "Number");
			
			if (Dividers.sumDividers(num) == (num + 1)) {
				JOptionPane.showMessageDialog(null, num + " is prime number.", "Result", JOptionPane.INFORMATION_MESSAGE);
			} else {
				JOptionPane.showMessageDialog(null, num + " isn't prime number.", "Result", JOptionPane.INFORMATION_MESSAGE);
			}
			
			repeatOption = ButtonBox.repeatButton();
		} while (repeatOption == 0);
		
		return repeatOption;
	}

	
	
	
	
	
	
	
	/*
	public static int ej1 () {
	int repeatOption = 0;
	do {
		System.out.println("Ej 1");
		
		repeatOption = ButtonBox.repeatButton();
	} while (repeatOption == 0);
	
	return repeatOption;
	}
	*/
	
}
