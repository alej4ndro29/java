package ej_examen;

import utils.*;

public class EjExamen {

	public static void main(String[] args) {
		
		int repeatOption = 0;
		boolean exit = false;
		
		String[] options = {"Ej 1", "Ej 2", "Ej 3", "Ej 4", "Ej 5","Ej 6", "Ej 7"};
		
		do {
			
			switch (ComboBox.comboBox("Ej:", "Selector", options)) {
			case 0:
				repeatOption = FuntionsEjExamen.ej1();
				break;
			case 1:
				repeatOption = FuntionsEjExamen.ej2();
				break;
			case 2:
				repeatOption = FuntionsEjExamen.ej3();
				break;
				
			case 3:
				repeatOption = FuntionsEjExamen.ej4();
				break;
				
			case 4:
				repeatOption = FuntionsEjExamen.ej5();
				break;
				
			case 5:
				repeatOption = FuntionsEjExamen.ej6();
				break;
				
			case 6:
				repeatOption = FuntionsEjExamen.ej7();
				break;
							
			default:
				exit = true;
				break;
			}
			
			
			if (repeatOption == 2) {
				exit = true;
			}
			
		} while (!exit);
		
	}

}
