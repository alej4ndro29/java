package classes.config;

import java.util.ArrayList;
import java.util.Locale;

import modules.resource.classes.Article;
import modules.resource.classes.Book;
import modules.resource.classes.BookPresentation;
import modules.resource.classes.Movie;
import modules.resource.classes.Singleton;

public class Config {
	public final static int ENG = 0;
	public final static int ESP = 1;
	public final static int VAL = 2;
	
	private int language;
	private boolean dummies;
	private String formatDate;
	private Locale currency;
	
	public Config() {
		this.language = ENG;
		this.dummies = false;
		this.formatDate = "dd/MM/yyyy";
//		this.formatDate = "dd-MM-yyyy";
		this.currency = new Locale("es", "ES");
		
		Singleton.article = new ArrayList <Article>();
		Singleton.book = new ArrayList<Book>();
		Singleton.bookPresentation = new ArrayList<BookPresentation>();
		Singleton.movie	= new ArrayList<Movie>();
	}
	
	public int getLanguage() {
		return language;
	}
	
	public boolean getDummies() {
		return dummies;
	}
	
	public String getFormatDate() {
		return formatDate;
	}
	
	public Locale getCurrency() {
		return currency;
	}
	
	public void setLanguage(int language) {
		this.language = language;
	}
	
	public void setDummies(boolean dummies) {
		this.dummies = dummies;
	}
	
	public void setFormatDate(String format) {
		this.formatDate = format;
	}
	
	public void setCurrencyEUR() {
		this.currency = new Locale("fr", "FR");
	}
	
	public void setCurrencyDOL() {
		this.currency = new Locale("en", "US");
	}
	
}
