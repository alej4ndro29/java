package main;


import classes.config.Config;
import modules.resource.classes.*;
import modules.resource.classes.dummies.DumResource;
import modules.resource.classes.order.OrderMenu;
import modules.resource.utils.crud.Create;
import modules.resource.utils.crud.Delete;
import modules.resource.utils.crud.Menu;
import modules.resource.utils.crud.Read;
import modules.resource.utils.crud.Update;
import utils.ComboBox;
import utils.languajes.Languaje;

public class Main {
	
	private final static int CREATE	= 0;
	private final static int READ	= 1;
	private final static int UPDATE	= 2;
	private final static int DUMMIES= 3;
	private final static int ORDER= 4;
	private final static int DELETE	= 5;
	
	private final static int CONFIG	= 4;
	
	public static int resourceOption = -1;
	public static Config config = new Config();
	
	public static Resource resource1 = null; // CREAR EL OBJETO resource1 AL QUE PUEDEN ACCEDER TODOS LOS FICHEROS.

	public static void main(String[] args) {
		boolean exit = false;
		String[] options = {Languaje.show("book"),
				Languaje.show("bookPresentation"),
				Languaje.show("article"),
				Languaje.show("movie"),
				Languaje.show("config")
				};

		do {
			resourceOption = ComboBox.comboBox(Languaje.show("option") + ":", Languaje.show("option"), options); // GUARDAR QUE SE HA ELEGIDO
			if (resourceOption == -1) {
				exit = true;
			} else if ((resourceOption == CONFIG)) {
				System.out.println("config");
				classes.config.Functions.configMenu();
				options[0] = Languaje.show("book");
				options[1] = Languaje.show("bookPresentation");
				options[2] = Languaje.show("article");
				options[3] = Languaje.show("movie");
				options[4] = Languaje.show("config");
			} else {
				switch (Menu.menuCRUD()) {
				case CREATE:
					Create.createResource();
					break;
				case READ:
					Read.readResource();
					break;
				case UPDATE:
					Update.updateResource();
					break;
				case ORDER:
					OrderMenu.orderResource();
					break;
				case DUMMIES:
					DumResource.create();
					break;
				case DELETE:
					Delete.deleteResource();
					break;
				}
					
			}
	
		} while (!exit);

	}

}
