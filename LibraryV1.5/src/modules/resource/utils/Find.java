package modules.resource.utils;

import modules.resource.classes.*;

public class Find {
	
	/*
	 * return -1 CUANDO LA CLAVE NO ESTÁ GUARDADA
	 * SI ESTÁ GUARDADA DEVUELVE LA POSICIÓN
	 * DEL ArrayList
	 */
	
	public static int findBook (String key) { // ENCONTRAR LIBRO
		return findBook(key, 0);
	}
	
	
	public static int findBook (String key, int pos) { // ENCONTRAR LIBRO
		if (Singleton.book.size() == pos) {
			return -1;
		} else {
			if (Singleton.book.get(pos).getKey().equals(key)) {
				return pos;
			} else {
				return findBook(key, (pos + 1));
			}
		}
	}
	
	public static int findBookPresentation (String key) { // ENCONTRAR LIBRO CON PRESENTACIÓN
		return findBookPresentation(key, 0);
	}
	
	public static int findBookPresentation (String key, int pos) { // ENCONTRAR LIBRO CON PRESENTACIÓN
		if (Singleton.bookPresentation.size() == pos) {
			return -1;
		} else {
			if (Singleton.bookPresentation.get(pos).getKey().equals(key)) {
				return pos;
			} else {
				return findBookPresentation(key, (pos + 1));
			}
		}
	}
	
	public static int findArticle (String key) { // ENCONTRAR ARTÍCULO
		return findArticle(key, 0);
	}
	
	public static int findArticle (String key, int pos) { // ENCONTRAR ARTÍCULO
		if (Singleton.article.size() == pos) {
			return -1;
		} else {
			if (Singleton.article.get(pos).getKey().equals(key)) {
				return pos;
			} else {
				return findArticle(key, (pos + 1));
			}
		}
	}
	
	public static int findMovie (String key) { // ENCONTRAR PELÍCULA
		return findMovie(key, 0);
	}
	
	public static int findMovie (String key, int pos) { // ENCONTRAR PELÍCULA
		if (Singleton.movie.size() == pos) {
			return -1;
		} else {
			if (Singleton.movie.get(pos).getKey().equals(key)) {
				return pos;
			} else {
				return findMovie(key, (pos + 1));
			}
		}
		
	}
	
}


