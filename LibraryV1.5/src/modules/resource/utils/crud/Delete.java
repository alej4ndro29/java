package modules.resource.utils.crud;

import javax.swing.JOptionPane;

import main.Main;
import modules.resource.classes.Resource;
import modules.resource.classes.Singleton;
import modules.resource.utils.ChooseObject;
import modules.resource.utils.Find;
import utils.languajes.Languaje;

public class Delete {
	
	public static void deleteResource() {
		switch (Main.resourceOption) {
			case Resource.BOOK:
				deleteBook();
				break;
			case Resource.BOOKPRESENTATION:
				deleteBookPresentation();
				break;
			case Resource.ARTICLE:
				deleteArticle();
				break;
			case Resource.MOVIE:
				deleteMovie();
				break;
		}
	}
	
	public static void deleteBook() {
		String key = "";
		int position = -1;
		
		if (Singleton.book.isEmpty()) {
			JOptionPane.showMessageDialog(null, Languaje.show("noBooks"), "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			key = ChooseObject.book();
			position = Find.findBook(key);
			//JOptionPane.showConfirmDialog(null, "Remove? \n" + Singleton.book.get(position).toString());
			//System.out.println(JOptionPane.showConfirmDialog(null, "Remove? \n" + Singleton.book.get(position).toString()));
			if (JOptionPane.showConfirmDialog(null, Languaje.show("remove") + "? \n" + Singleton.book.get(position).toString()) == 0) {
				Singleton.book.remove(position);
				JOptionPane.showMessageDialog(null, key + " deleted", "Ok", JOptionPane.INFORMATION_MESSAGE);
			}
		}
			
	}
	
	public static void deleteBookPresentation() {
		String key = "";
		int position = -1;
		
		if (Singleton.bookPresentation.isEmpty()) {
			JOptionPane.showMessageDialog(null, Languaje.show("noBooks"), "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			key = ChooseObject.bookPresentation();
			position = Find.findBookPresentation(key);
			if (JOptionPane.showConfirmDialog(null, Languaje.show("remove") + " \n" + Singleton.bookPresentation.get(position).toString()) == 0) {
				Singleton.bookPresentation.remove(position);
				JOptionPane.showMessageDialog(null, key + Languaje.show("deleted"), "Ok", JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}
	
	public static void deleteArticle() {
		String key = "";
		int position = -1;
		
		if (Singleton.article.isEmpty()) {
			JOptionPane.showMessageDialog(null, Languaje.show("noArticles"), "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			key = ChooseObject.article();
			position = Find.findArticle(key);
			if (JOptionPane.showConfirmDialog(null, Languaje.show("remove") + " \n" + Singleton.article.get(position).toString()) == 0) {
				Singleton.article.remove(position);
				JOptionPane.showMessageDialog(null, key + Languaje.show("deleted"), "Ok", JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}
	
	public static void deleteMovie() {
		String key = "";
		int position = -1;
		
		if (Singleton.movie.isEmpty()) {
			JOptionPane.showMessageDialog(null, Languaje.show("noMovies"), "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			key = ChooseObject.movie();
			position = Find.findMovie(key);
			if (JOptionPane.showConfirmDialog(null, Languaje.show("remove") + " \n" + Singleton.movie.get(position).toString()) == 0) {
				Singleton.movie.remove(position);
				JOptionPane.showMessageDialog(null, key + Languaje.show("deleted"), "Ok", JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}
}
