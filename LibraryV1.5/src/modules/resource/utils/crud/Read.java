package modules.resource.utils.crud;

import javax.swing.JOptionPane;

import main.Main;
import modules.resource.classes.Resource;
import modules.resource.classes.Singleton;
import modules.resource.utils.ChooseObject;
import modules.resource.utils.Find;
import utils.ButtonBox;
import utils.languajes.Languaje;

public class Read {
	
	public static void readResource() {
		switch (Main.resourceOption) {
			case Resource.BOOK:
				readBook();
				break;
			case Resource.BOOKPRESENTATION:
				readBookPresentation();
				break;
			case Resource.ARTICLE:
				readArticle();
				break;
			case Resource.MOVIE:
				readMovie();
				break;
		}
	}
	
	
	public static void readBook() {
		String cad = "";
		String[] options = {Languaje.show("all"), Languaje.show("one")};
		int size = Singleton.book.size();
		int position = 0;
		
		if (Singleton.book.isEmpty()) {
			JOptionPane.showMessageDialog(null, Languaje.show("noBooks"), "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			
			switch (ButtonBox.buttonsOptionsNoClose(Languaje.show("allOrOne"), Languaje.show("allOrOne"), options)) {
			case 0:
				for (int i = 0; i <= (size - 1); i++) {
					cad = cad + Singleton.book.get(i).toString() + "\n\n";
				}
				JOptionPane.showMessageDialog(null, cad, Languaje.show("object"), JOptionPane.INFORMATION_MESSAGE);
				break;

			case 1:
				position = Find.findBook(ChooseObject.book());
				JOptionPane.showMessageDialog(null, Singleton.book.get(position).toString(), Languaje.show("object"), JOptionPane.INFORMATION_MESSAGE);
				break;
			}
		}
	}
	
	public static void readBook(String key) {
		try {
			int position = 0;
			position = Find.findBook(key);
			JOptionPane.showMessageDialog(null, Singleton.book.get(position).toString(), Languaje.show("object"), JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception e) {
			System.err.println("Invalid key --> " + key);
		}
	}
	
	public static void readBook(int position) {
		try {
			JOptionPane.showMessageDialog(null, Singleton.book.get(position).toString(), Languaje.show("object"), JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception e) {
			System.err.println("invalid position --> " + position);
		}
	}
	
	//////////////////////////////////////////////////////
	
	public static void readBookPresentation() {
		String cad = "";
		String[] options = {Languaje.show("all"), Languaje.show("one")};
		int size = Singleton.bookPresentation.size();
		int position = 0;
		
		if (Singleton.bookPresentation.isEmpty()) {
			JOptionPane.showMessageDialog(null, Languaje.show("noBooks"), "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			
			switch (ButtonBox.buttonsOptionsNoClose(Languaje.show("allOrOne"), Languaje.show("allOrOne"), options)) {
			case 0:
				for (int i = 0; i <= (size - 1); i++) {
					cad = cad + Singleton.bookPresentation.get(i).toString() + "\n\n";
				}
				JOptionPane.showMessageDialog(null, cad, Languaje.show("object"), JOptionPane.INFORMATION_MESSAGE);
				break;

			case 1:
				position = Find.findBookPresentation(ChooseObject.bookPresentation());
				JOptionPane.showMessageDialog(null, Singleton.bookPresentation.get(position).toString(), Languaje.show("object"), JOptionPane.INFORMATION_MESSAGE);
				break;
			}
		}
	}
	
	public static void readBookPresentation(String key) {
		try {
			int position = 0;
			position = Find.findBookPresentation(key);
			JOptionPane.showMessageDialog(null, Singleton.bookPresentation.get(position).toString(), Languaje.show("object"), JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception e) {
			System.err.println("Invalid key --> " + key);
		}
	}
	
	public static void readBookPresentation(int position) {
		try {
			JOptionPane.showMessageDialog(null, Singleton.bookPresentation.get(position).toString(), Languaje.show("object"), JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception e) {
			System.err.println("invalid position --> " + position);
		}
	}
	
	//////////////////////////////////////////////////////
	
	public static void readArticle() {
		String cad = "";
		String[] options = {Languaje.show("all"), Languaje.show("one")};
		int size = Singleton.article.size();
		int position = 0;
		
		if (Singleton.article.isEmpty()) {
			JOptionPane.showMessageDialog(null, Languaje.show("noArticles"), "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			
			switch (ButtonBox.buttonsOptionsNoClose(Languaje.show("allOrOne"), Languaje.show("allOrOne"), options)) {
			case 0:
				for (int i = 0; i <= (size - 1); i++) {
					cad = cad + Singleton.article.get(i).toString() + "\n\n";
				}
				JOptionPane.showMessageDialog(null, cad, "Object", JOptionPane.INFORMATION_MESSAGE);
				break;

			case 1:
				position = Find.findArticle(ChooseObject.article());
				JOptionPane.showMessageDialog(null, Singleton.article.get(position).toString(), Languaje.show("object"), JOptionPane.INFORMATION_MESSAGE);
				break;
			}
		}
	}

	public static void readArticle(String key) {
		try {
			int position = 0;
			position = Find.findArticle(key);
			JOptionPane.showMessageDialog(null, Singleton.article.get(position).toString(), Languaje.show("object"), JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception e) {
			System.err.println("Invalid key --> " + key);
		}
	}
	
	public static void readArticle(int position) {
		try {
			JOptionPane.showMessageDialog(null, Singleton.article.get(position).toString(), Languaje.show("object"), JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception e) {
			System.err.println("invalid position --> " + position);
		}
	}
	
	//////////////////////////////////////////////////////

	public static void readMovie() {
		String cad = "";
		String[] options = {Languaje.show("all"), Languaje.show("one")};
		int size = Singleton.movie.size();
		int position = 0;
		
		if (Singleton.movie.isEmpty()) {
			JOptionPane.showMessageDialog(null, Languaje.show("noMovies"), "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			
			switch (ButtonBox.buttonsOptionsNoClose(Languaje.show("allOrOne"), Languaje.show("allOrOne"), options)) {
			case 0:
				for (int i = 0; i <= (size - 1); i++) {
					cad = cad + Singleton.movie.get(i).toString() + "\n\n";
				}
				JOptionPane.showMessageDialog(null, cad, "Object", JOptionPane.INFORMATION_MESSAGE);
				break;

			case 1:
				position = Find.findMovie(ChooseObject.movie());
				JOptionPane.showMessageDialog(null, Singleton.movie.get(position).toString(), Languaje.show("object"), JOptionPane.INFORMATION_MESSAGE);
				break;
			}
		}
	}
	
	public static void readMovie(String key) {
		try {
			int position = 0;
			position = Find.findMovie(key);
			JOptionPane.showMessageDialog(null, Singleton.movie.get(position).toString(), Languaje.show("object"), JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception e) {
			System.err.println("Invalid key --> " + key);
		}
	}

	public static void readMovie(int position) {
		try {
			JOptionPane.showMessageDialog(null, Singleton.movie.get(position).toString(), Languaje.show("object"), JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception e) {
			System.err.println("invalid position --> " + position);
		}
	}
	
}
