package modules.resource.utils.crud;

import javax.swing.JOptionPane;

import main.Main;
import modules.resource.classes.*;
import modules.resource.utils.Find;
import modules.resource.utils.InputResourceData;
import utils.ValidateData;
import utils.languajes.Languaje;

public class Create {
	
	public static void createResource() {
		switch (Main.resourceOption) {
			case Resource.BOOK:
				createBook();
				break;
			case Resource.BOOKPRESENTATION:
				createBookPresentation();
				break;
			case Resource.ARTICLE:
				createArticle();
				break;
			case Resource.MOVIE:
				createMovie();
				break;
		}
	}
	
	public static void createBook () {  // CREAR LIBRO
		boolean check = false;
		String key = "";
		Book book = null;
		int location = -1;
		
		do {
			key = ValidateData.inputKey(Languaje.show("askKey"), Languaje.show("askKeyTitle"));
			book = new Book(key);
			location = Find.findBook(book.getKey()); 
			System.out.println("location = " + location);
			
			if (location != -1) { // SI LA CLAVE YA ESTÁ REGISTRADA
				System.out.println(Singleton.book.get(location).toString());
				JOptionPane.showMessageDialog(null, Languaje.show("errKeyRegistered"), Languaje.show("errKeyRegistered"), JOptionPane.ERROR_MESSAGE);
			} else {
				Singleton.book.add((Book) InputResourceData.askBook(key));
				check = true;
			}
		} while (!check);
		
	}

	
	public static void createBookPresentation() { // CREAR LIBRO CON PRESENTACIÓN
		boolean check = false;
		String key = "";
		BookPresentation bookpresentation = null;
		int location = -1;
		
		do {
			key = ValidateData.inputKey(Languaje.show("askKey"), Languaje.show("askKeyTitle"));
			bookpresentation = new BookPresentation(key); 
			
			location = Find.findBookPresentation(bookpresentation.getKey());
			if (location != -1) {
				System.out.println(Singleton.bookPresentation.get(location).toString());
				JOptionPane.showMessageDialog(null, Languaje.show("errKeyRegistered"), Languaje.show("errKeyRegistered"), JOptionPane.ERROR_MESSAGE);
			} else {
				Singleton.bookPresentation.add((BookPresentation) InputResourceData.askBookPresentation(key));
				check = true;
			}
		
		} while (!check);
		
	}
	
	
	public static void createArticle() { // CREAR ARTÍCULO
		boolean check = false;
		String key = "";
		Article article = null;
		int location = -1;
		
		do {
			key = ValidateData.inputKey(Languaje.show("askKey"), Languaje.show("askKeyTitle"));
			article = new Article(key);
			
			location = Find.findArticle(article.getKey());
			if (location != -1) {
				System.out.println(Singleton.article.get(location).toString()); 
				JOptionPane.showMessageDialog(null, Languaje.show("errKeyRegistered"), Languaje.show("errKeyRegistered"), JOptionPane.ERROR_MESSAGE);
			} else {
				Singleton.article.add((Article) InputResourceData.askArticle(key));
				check = true;
			}
		} while (!check);
	}
	
	public static void createMovie() { // CREAR PELÍCULA
		boolean check = false;
		String key = "";
		Movie movie = null;
		int location = -1;
		
		do {
			key = ValidateData.inputKey(Languaje.show("askKey"), Languaje.show("askKeyTitle"));
			movie = new Movie(key); 
			
			location = Find.findMovie(movie.getKey());
			if (location != -1) {
				System.out.println(Singleton.movie.get(location).toString()); 
				JOptionPane.showMessageDialog(null, Languaje.show("errKeyRegistered"), Languaje.show("errKeyRegistered"), JOptionPane.ERROR_MESSAGE);
			} else {
				Singleton.movie.add((Movie) InputResourceData.askMovie(key));
				check = true;
			}
		} while (!check);
		
	}
	
}
