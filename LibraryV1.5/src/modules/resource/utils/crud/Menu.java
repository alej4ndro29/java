package modules.resource.utils.crud;

import utils.ButtonBox;
import utils.languajes.Languaje;

public class Menu {
	public static int menuCRUD() { // MENÚ CRUD
		String[] crudOptions = { Languaje.show("create"),
				Languaje.show("read"), 
				Languaje.show("update"), 
				Languaje.show("dummies"),
				Languaje.show("order"),
				Languaje.show("delete") };
		return ButtonBox.buttonsOptions(Languaje.show("option") + ":", Languaje.show("option"), crudOptions);

	}
}
