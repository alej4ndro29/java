package modules.resource.utils;

import javax.swing.JOptionPane;

import modules.resource.classes.Singleton;
import utils.ValidateData;
import utils.languajes.Languaje;

public class ChooseObject {
	/*
	 * MENÚ PARA ELEGIR UNA DE LAS POSICIONES
	 * DEL ARRAYLIST
	 */
	public static String book() {
		String key = "";
		String cad = "";
		int position = 0;
		Boolean check = false;
		
		cad = Languaje.show("chooseTitle") + "\n";
		for (int i = 0; i <= (Singleton.book.size() - 1); i++) {
			cad = cad + Singleton.book.get(i).getKey() + " -- " + Singleton.book.get(i).getName() + "\n";
		}
		
		do {
			key = ValidateData.inputKey(cad, Languaje.show("askKeyTitle"));
			position = Find.findBook(key);
			if (position == -1) {
				JOptionPane.showMessageDialog(null, key + " " + Languaje.show("errNoExist"), "Error", JOptionPane.ERROR_MESSAGE);
			} else {
				check = true;
			}
		} while (!check);
		
		return key;
		
	}
	
	
	public static String bookPresentation() {
		String key = "";
		String cad = "";
		int position = 0;
		Boolean check = false;
		
		cad = Languaje.show("chooseTitle") + "\n";
		for (int i = 0; i <= (Singleton.bookPresentation.size() - 1); i++) {
			cad = cad + Singleton.bookPresentation.get(i).getKey() + " -- " + Singleton.bookPresentation.get(i).getName() + "\n";
		}
		
		do {
			key = ValidateData.inputKey(cad, Languaje.show("askKeyTitle"));
			position = Find.findBookPresentation(key);
			if (position == -1) {
				JOptionPane.showMessageDialog(null, key + " " + Languaje.show("errNoExist"), "Error", JOptionPane.ERROR_MESSAGE);
			} else {
				check = true;
			}
		} while (!check);
		
		return key;
	}

	public static String article() {
		String key = "";
		String cad = "";
		int position = 0;
		Boolean check = false;
		
		cad = Languaje.show("chooseTitle") + "\n";
		for (int i = 0; i <= (Singleton.article.size() - 1); i++) {
			cad = cad + Singleton.article.get(i).getKey() + " -- " + Singleton.article.get(i).getName() + "\n";
		}
		
		do {
			key = ValidateData.inputKey(cad, Languaje.show("askKeyTitle"));
			position = Find.findArticle(key);
			if (position == -1) {
				JOptionPane.showMessageDialog(null, key + " " + Languaje.show("errNoExist"), "Error", JOptionPane.ERROR_MESSAGE);
			} else {
				check = true;
			}
		} while (!check);
		
		return key;
	}
	
	public static String movie() {
		String key = "";
		String cad = "";
		int position = 0;
		Boolean check = false;
		
		cad = Languaje.show("chooseTitle") + "\n";
		for (int i = 0; i <= (Singleton.movie.size() - 1); i++) {
			cad = cad + Singleton.movie.get(i).getKey() + " -- " + Singleton.movie.get(i).getName() + "\n";
		}
		
		do {
			key = ValidateData.inputKey(cad, "Key");
			position = Find.findMovie(key);
			if (position == -1) {
				JOptionPane.showMessageDialog(null, key + " " + Languaje.show("errNoExist"), "Error", JOptionPane.ERROR_MESSAGE);
			} else {
				check = true;
			}
		} while (!check);
		
		return key;
	}

	
}
