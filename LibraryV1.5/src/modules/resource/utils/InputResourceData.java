package modules.resource.utils;

import classes.dates.Dates;
import modules.resource.classes.*;
import utils.Numbers;
import utils.Strings;
import utils.ValidateData;
import utils.languajes.Languaje;


public class InputResourceData {
	public static Resource askBook (String key) {
		System.out.println("askBook");
		String name 		= Strings.inputStringNoClose(Languaje.show("askName"), Languaje.show("askName"));
		String author 		= Strings.inputStringNoClose(Languaje.show("askAuthor"), Languaje.show("askAuthor"));
		String thematic		= Strings.inputStringNoClose(Languaje.show("askThematic"), Languaje.show("askThematic"));
		Dates release 		= ValidateData.inputDateBeforeToday(Languaje.show("askRelease"), Languaje.show("askRelease"));
		String editorial 	= Strings.inputStringNoClose(Languaje.show("askEditorial"), Languaje.show("askEditorial"));
		return new Book(key, name, author, thematic, release, editorial);
	}
	
	
	public static Resource askBookPresentation (String key) {
		String name 			= Strings.inputStringNoClose(Languaje.show("askName"), Languaje.show("askName"));
		String author 			= Strings.inputStringNoClose(Languaje.show("askAuthor"), Languaje.show("askAuthor"));
		String thematic			= Strings.inputStringNoClose(Languaje.show("askThematic"), Languaje.show("askThematic"));
		Dates release 			= ValidateData.inputDateBeforeToday(Languaje.show("askRelease"), Languaje.show("askRelease"));
		String editorial 		= Strings.inputStringNoClose("Editorial", "Editorial");
		Dates presentationStart	= ValidateData.inputDateAfterToday(Languaje.show("askPresentationStart"), Languaje.show("askPresentationStart"));
		Dates presentationEnd 	= ValidateData.inputDateAfterX(Languaje.show("askPresentationEnd"), Languaje.show("askPresentationEnd"), presentationStart.toString());
		return new BookPresentation(key, name, author, thematic, release, editorial, presentationStart, presentationEnd);
	}
	
	public static Resource askArticle (String key) {
		String name 		= Strings.inputStringNoClose(Languaje.show("askName"), Languaje.show("askName"));
		String author 		= Strings.inputStringNoClose(Languaje.show("askAuthor"), Languaje.show("askAuthor"));
		String thematic		= Strings.inputStringNoClose(Languaje.show("askThematic"), Languaje.show("askThematic"));
		Dates release 		= ValidateData.inputDateBeforeToday(Languaje.show("askRelease"), Languaje.show("askRelease"));
		String means 		= Strings.inputStringNoClose(Languaje.show("askMeans"), Languaje.show("askMeans"));
		Dates startShop 	= ValidateData.inputDate(Languaje.show("askStartShop"), Languaje.show("askStartShop"));
		Dates endShop 		= ValidateData.inputDateAfterX(Languaje.show("askEndShop"), Languaje.show("askEndShop"), startShop.toString());
		Dates purchaseDay	= ValidateData.inputDateBetweenXandY(Languaje.show("askPurchaseDay"), Languaje.show("askPurchaseDay"), 
																	startShop.toString(),
																	endShop.toString());
		float unitPrice		= Numbers.inputFloat(Languaje.show("askUnitPrice"), Languaje.show("askUnitPrice"));
		int quantity 		= Numbers.inputNumber(Languaje.show("askQuantity"), Languaje.show("askQuantity"));
		return new Article(key, name, author, thematic, release, means, startShop, endShop, purchaseDay, unitPrice, quantity);
	}
	
	public static Resource askMovie (String key) {
		String name 			= Strings.inputStringNoClose(Languaje.show("askName"), Languaje.show("askName"));
		String author 			= Strings.inputStringNoClose(Languaje.show("askAuthor"), Languaje.show("askAuthor"));
		String thematic			= Strings.inputStringNoClose(Languaje.show("askThematic"), Languaje.show("askThematic"));
		Dates release 			= ValidateData.inputDateBeforeToday(Languaje.show("askRelease"), Languaje.show("askRelease"));
		Dates presentationStart	= ValidateData.inputDateAfterToday(Languaje.show("askPresentationStart"), Languaje.show("askPresentationStart"));
		Dates presentationEnd 	= ValidateData.inputDateAfterX(Languaje.show("askPresentationEnd"), Languaje.show("askPresentationEnd"), presentationStart.toString());
		Dates preview			= ValidateData.inputDateBetweenXandY(Languaje.show("askPreview"), Languaje.show("askPreview"), presentationStart.toString(), presentationEnd.toString());
		String company 			= Strings.inputStringNoClose(Languaje.show("askCompany"), Languaje.show("askCompany"));
		return new Movie(key, name, author, thematic, release, presentationStart, presentationEnd, preview, company);
	}
	
}
