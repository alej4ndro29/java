package modules.resource.classes;

import classes.dates.Dates;
import utils.ValidateData;

public abstract class Resource implements Comparable<Resource> {

	public final static int BOOK = 0;
	public final static int BOOKPRESENTATION = 1;
	public final static int ARTICLE = 2;
	public final static int MOVIE = 3;

	private String key;
	private String name;
	private String author;
	private String thematic;
	private Dates release;
	private Dates addedDate;
	private int periodReleaseAdded;

	// CONSTRUCTOR
	public Resource(String key, String name, String author, String thematic, Dates release) {
		this.key = key;
		this.name = name;
		this.author = author;
		this.thematic = thematic;
		this.release = release;
		this.addedDate = ValidateData.today();
		this.periodReleaseAdded = release.getDaysInterval(addedDate.toString());
	}

	// CONSTRUCTOR VACÍO
	public Resource() {

	}

	// ID_CONTRUCTOR
	public Resource(String key) {
		this.key = key;
	}

	// SETTERS
	public void setKey(String key) {
		this.key = key;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setAuthor(String author) {
		this.author = author;
	}

	public void setThematic(String thematic) {
		this.thematic = thematic;
	}

	public void setRelease(Dates release) {
		this.release = release;
		setPeriodReleaseAdded();
	}

	public void setAddedDate() {
		this.addedDate = ValidateData.today();
		setPeriodReleaseAdded();
	}

	public void setAddedDate(Dates addedDate) {
		this.addedDate = addedDate;
		setPeriodReleaseAdded();
	}

	public void setPeriodReleaseAdded() {
		this.periodReleaseAdded = release.getDaysInterval(addedDate.toString());
	}

	// GETTERS
	public String getKey() {
		return key;
	}

	public String getName() {
		return name;
	}

	public String getAuthor() {
		return author;
	}

	public String getThematic() {
		return thematic;
	}

	public Dates getRelease() {
		return release;
	}

	public Dates getAddedDate() {
		return addedDate;
	}

	public int getPeriodReleaseAdded() {
		return periodReleaseAdded;
	}
	
	public int compareTo(Resource param) {//para ordenar los empleados por nombre
		if(this.getKey().compareTo(param.getKey())>0)
			return 1;
		if(this.getKey().compareTo(param.getKey())<0)
			return -1;
		return 0;
	  }


	// TOSTRING
	public String toString() {
		return "Resources [key " + key + ", \n" + "name = " + name + ", \n" + "author=" + author + ", \n" + "thematic= "
				+ thematic + ", \n" + "release= " + release.toString() + ", \n" + "addedDate = " + addedDate.toString()
				+ ", \n" + "periodReleaseAdded" + periodReleaseAdded + "]";
	}

}
