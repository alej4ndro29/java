package modules.resource.classes;

import classes.dates.Dates;
import utils.languajes.Languaje;

public class BookPresentation extends Book {

	private Dates presentationStart;
	private Dates presentationEnd;
	private int presentationDays;
	
	// CONSTRUCTOR
	public BookPresentation(String key, String name, String author, String thematic, Dates release, String editorial, 
							Dates presentationStart, Dates presentationEnd) {
		super(key, name, author, thematic, release, editorial);

		this.presentationStart	= presentationStart;
		this.presentationEnd 	= presentationEnd;
		//this.presentationDays	= presentationStart.getDaysInterval(presentationEnd.toString());
		setPresentationDays();

	}
	
	// CONSTRUCTOR VACÍO
	public BookPresentation() {
		super();
	}
	
	//  ID_CONSTRUCTOR
	public BookPresentation(String key) {
		super(key);
	}

	// GETTERS
	public Dates getPresentationStart() {
		return presentationStart;
	}

	public Dates getPresentationEnd() {
		return presentationEnd;
	}

	public int getPresentationDays() {
		return presentationDays;
	}

	
	// SETTERS
	public void setPresentationStart(Dates presentationStart) {
		this.presentationStart = presentationStart;
		setPresentationDays();
	}

	public void setPresentationEnd(Dates presentationEnd) {
		this.presentationEnd = presentationEnd;
		setPresentationDays();
	}

	public void setPresentationDays() {
		this.presentationDays = presentationStart.getDaysInterval(presentationEnd.toString());
	}
	
	
	public BookPresentation copy() {
		return new BookPresentation(this.getKey(), this.getName(), this.getAuthor(), 
									this.getThematic(), this.getRelease(), this.getEditorial(), 
									this.presentationStart, this.presentationEnd);
	}
	
	public String toString() {
		return 	"BookPresentation [" + Languaje.show("askKeyTitle") + " = "			+ getKey()		+ ", \n" +
				Languaje.show("askName")	 	+ " = "		+ getName()					+ ", \n" +
				Languaje.show("askAuthor")		+" = "		+ getAuthor() 				+ ", \n" +
				Languaje.show("askThematic") 	+ " = " 	+ getThematic() 			+ ", \n" +
				Languaje.show("release") 		+ " = " 	+ getRelease() 				+ ", \n" +
				Languaje.show("addedDate") 		+ " = " 	+ getAddedDate() 			+ ", \n" +
				Languaje.show("periodReleaseAdded") + " = " + getPeriodReleaseAdded()	+ ", \n" + 
				Languaje.show("askEditorial") + " = "		+ getEditorial()			+ ", \n" +
				Languaje.show("presentationStart") + " = "	+ presentationStart			+ ", \n" +
				Languaje.show("presentationEnd") + " = " 	+ presentationEnd			+ ", \n" +
				Languaje.show("presentationDays") + " = "		+ presentationDays			+
				"]";
	}

	
}