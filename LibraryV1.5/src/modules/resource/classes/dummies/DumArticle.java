package modules.resource.classes.dummies;

import java.util.Random;

import classes.dates.Dates;
import modules.resource.classes.Article;
import modules.resource.classes.Singleton;
import modules.resource.utils.Find;

public class DumArticle {
	
	public static void createDumArticle(int repeats) {
		Article article = null;
		Random random = new Random();
		Dates rel = new Dates();
		Dates start = new Dates();
		Dates end = new Dates();
		Dates purchase = new Dates();
		String key = "";
		String name 		= "Artículo de muestra";
		String author 		= "Anónimo";
		String thematic		= "Didáctica";
		
		Dates release 		= rel;
		String means 		= "La noticia";
		Dates startShop 	= start;
		end.setToday();
		end.addDays(60);
		Dates endShop 		= end;
		purchase.setToday();
		purchase.addDays(40);
		Dates purchaseDay	= purchase;
		float unitPrice		= 0.70f;
		int quantity 		= 0;
		
		for (int i = 0; i < repeats; i++) {
			rel.setToday();
			rel.minusDays(random.nextInt(60) + 1);
			start.setToday();
			start.addDays(random.nextInt(30) + 1);
			
			release = rel.copy();
			startShop = start.copy();
			do {
				key = DumResource.randomKey();
				quantity = (random.nextInt(50)+1);
			} while (Find.findArticle(key) != -1);
		
			article = new Article(key, name, author, thematic, release, means, startShop, endShop, purchaseDay, unitPrice, quantity);
			Singleton.article.add(article);
			
		}
	}
	
}
