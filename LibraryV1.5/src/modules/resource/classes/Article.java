package modules.resource.classes;

import java.text.NumberFormat;

import classes.dates.Dates;
import main.Main;
import utils.languajes.Languaje;

public class Article extends Resource {
	private String	means;
	private Dates 	startShop;
	private Dates 	endShop;
	private int 	daysShop;
	private Dates 	purchaseDay;
	private int		quantity;
	private float 	unitPrice;
	private float	totalPrice;
	
	// CONSTRUCTOR
	public Article(String key, String name, String author, String thematic, Dates release, String means,
					Dates startShop, Dates endShop, Dates purchaseDay, float unitPrice, int quantity) {
		super(key, name, author, thematic, release); // ATRIBUTOS DE LA CLASE Resource
		this.means 			= means;
		this.startShop		= startShop;
		this.endShop 		= endShop;
		this.purchaseDay	= purchaseDay;
		this.quantity		= quantity;
		this.unitPrice		= unitPrice;
		setDaysShop();
		setTotalPrice();
	}
	
	// CONTRUCTOR VACÍO
	public Article () {
		
	}
	
	
	// ID_CONSTRUCTOR
	public Article (String key) {
		super(key);
	}
	
	
	// GETTER
	public String getMeans() {
		return means;
	}
	
	public Dates getStartShop() {
		return startShop;
	}

	public Dates getEndShop() {
		return endShop;
	}
	
	public int getDaysShop() {
		return daysShop;
	}
	
	public Dates getPurchaseDay() {
		return purchaseDay;
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	public String getUnitPrice() {
		NumberFormat formatterPrice = NumberFormat.getCurrencyInstance(Main.config.getCurrency());
		return formatterPrice.format(unitPrice);
	}

	public String getTotalPrice() {
		NumberFormat formatterPrice = NumberFormat.getCurrencyInstance(Main.config.getCurrency());
		return formatterPrice.format(totalPrice);
	}
	
	// SETTER
	public void setMeans(String means) {
		this.means = means;
	}
	
	public void setStartShop(Dates startShop) {
		this.startShop = startShop;
		setDaysShop();
	}
	
	public void setEndShop(Dates endShop) {
		this.endShop = endShop;
		setDaysShop();
	}
	
	public void setPurchaseDay(Dates purchaseDay) {
		this.purchaseDay = purchaseDay;
	}
	
	public void setQuantity(int quantity) {
		this.quantity = quantity;
		setTotalPrice();
	}
	
	public void setUnitPrice(float unitPrice) {
		this.unitPrice = unitPrice;
		setTotalPrice();
	}
	
	public void setDaysShop() {
		daysShop = startShop.getDaysInterval(endShop.toString());
	}
	
	
	public void setTotalPrice() {
		float discount = 15f; // %
		totalPrice = unitPrice * quantity;
		System.out.println("totalPrice = " + totalPrice);
		if (quantity > 20) { // OFERTA DEL discount% 
			System.out.println("discount quantity");
			totalPrice = (totalPrice - (totalPrice * (discount / 100)));
			System.out.println("totalPrice = " + totalPrice);
		} else {
			System.out.println("no discount quantity");
		}
	}
	
	
	public Article copy() {
		return new Article(this.getKey(), this.getName(), this.getAuthor(), 
							this.getThematic(), this.getRelease(), this.means, 
							this.startShop, this.endShop, this.purchaseDay, 
							this.unitPrice, this.quantity);
	}
	
	// TOSTRING
	
	public String toString() {
		return "Article [" + Languaje.show("askKeyTitle") + " = "			+ getKey()		+ ", \n" +
				Languaje.show("askName")	 	+ " = "		+ getName()					+ ", \n" +
				Languaje.show("askAuthor")		+ " = "		+ getAuthor() 				+ ", \n" +
				Languaje.show("askThematic") 	+ " = " 	+ getThematic() 			+ ", \n" +
				Languaje.show("release") 		+ " = " 	+ getRelease() 				+ ", \n" +
				Languaje.show("addedDate") 		+ " = " 	+ getAddedDate() 			+ ", \n" +
				Languaje.show("periodReleaseAdded") + " = " + getPeriodReleaseAdded()	+ ", \n" + 
				Languaje.show("askMeans") + " = " 			+ means						+ ", \n" +
				Languaje.show("startShop") + " = "			+ startShop.toString()		+ ", \n" +
				Languaje.show("endShop") + " = "			+ endShop					+ ", \n" +
				Languaje.show("daysShop") + " = "			+ daysShop					+ ", \n" +
				Languaje.show("purchaseDay") + " = "		+ purchaseDay				+ ", \n" +
				Languaje.show("askUnitPrice") + " = "		+ getUnitPrice()			+ ", \n" +
				Languaje.show("askQuantity") + " = "		+ quantity					+ ", \n" +
				Languaje.show("totalPrice") + " = "			+ getTotalPrice()			+ ", \n" +
				"]";
	}

}
