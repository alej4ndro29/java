package modules.resource.classes.order;

import java.util.Comparator;

import modules.resource.classes.Resource;

public class OrderRelease implements Comparator<Resource>{

	@Override
	public int compare(Resource o1, Resource o2) {
		// TODO Auto-generated method stub
		if(o1.getRelease().compare(o2.getRelease()) == -1)
			return 1;
		if(o1.getRelease().compare(o2.getRelease()) == 1)
			return -1;
		return 0;
	}


}
