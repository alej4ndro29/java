package modules.resource.classes.order;

import java.util.Collections;

import javax.swing.JOptionPane;

import main.Main;
import modules.resource.classes.Resource;
import modules.resource.classes.Singleton;
import utils.ButtonBox;
import utils.languajes.Languaje;

public class OrderMenu {
	
	public static void orderResource() {
		switch (Main.resourceOption) {
			case Resource.BOOK:
				orderBook();
				break;
			case Resource.BOOKPRESENTATION:
				orderBookPresentation();
				break;
			case Resource.ARTICLE:
				orderArticle();
				break;
			case Resource.MOVIE:
				orderMovie();
				break;
		}
	}
	
	
	public static void orderBook() {
		String[] options = {Languaje.show("askKeyTitle"), 
							Languaje.show("periodReleaseAdded"), 
							Languaje.show("release")};
		if (Singleton.book.isEmpty()) {
			JOptionPane.showMessageDialog(null, Languaje.show("noBooks"), "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			switch (ButtonBox.buttonsOptions("Option order:", "Order", options)) {
				case 0:
					Collections.sort(Singleton.book, new OrderKey());
					break;
				case 1:
					Collections.sort(Singleton.book, new OrderPeriodReleaseAdded());
					break;
				case 2:
					Collections.sort(Singleton.book, new OrderRelease());
					break;
				default:
					break;
			}
		}
	}
	
	public static void orderBookPresentation() {
		String[] options = {Languaje.show("askKeyTitle"), 
				Languaje.show("periodReleaseAdded"), 
				Languaje.show("release")};		
		if (Singleton.bookPresentation.isEmpty()) {
			JOptionPane.showMessageDialog(null, Languaje.show("noBooks"), "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			switch (ButtonBox.buttonsOptions(Languaje.show("optionOrder") + ":", Languaje.show("order"), options)) {
				case 0:
					Collections.sort(Singleton.bookPresentation, new OrderKey());
					break;
				case 1:
					Collections.sort(Singleton.bookPresentation, new OrderPeriodReleaseAdded());
					break;
				case 2:
					Collections.sort(Singleton.bookPresentation, new OrderRelease());
					break;
				default:
					break;
			}
		}
	}
	
	public static void orderArticle() {
		String[] options = {Languaje.show("askKeyTitle"), 
				Languaje.show("periodReleaseAdded"), 
				Languaje.show("release")};	
		if (Singleton.article.isEmpty()) {
			JOptionPane.showMessageDialog(null, Languaje.show("noArticles"), "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			switch (ButtonBox.buttonsOptions(Languaje.show("optionOrder") + ":", Languaje.show("order"), options)) {
				case 0:
					Collections.sort(Singleton.article, new OrderKey());
					break;
				case 1:
					Collections.sort(Singleton.article, new OrderPeriodReleaseAdded());
					break;
				case 2:
					Collections.sort(Singleton.article, new OrderRelease());
					break;
				default:
					break;
			}
		}
	}
	
	
	public static void orderMovie() {
		String[] options = {Languaje.show("askKeyTitle"), 
				Languaje.show("periodReleaseAdded"), 
				Languaje.show("release")};	
		if (Singleton.movie.isEmpty()) {
			JOptionPane.showMessageDialog(null, Languaje.show("noMovies"), "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			switch (ButtonBox.buttonsOptions(Languaje.show("optionOrder") + ":", Languaje.show("order"), options)) {
				case 0:
					Collections.sort(Singleton.movie, new OrderKey());
					break;
				case 1:
					Collections.sort(Singleton.movie, new OrderPeriodReleaseAdded());
					break;
				case 2:
					Collections.sort(Singleton.movie, new OrderRelease());
					break;
				default:
					break;
			}
		}
	}
}
