package utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import classes.dates.Dates;
import main.Main;

public class ValidateData {
	private static final String dateTemplate0 = "(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/]((175[7-9])|(17[6-9][0-9])|(1[8-9][0-9][0-9])|([2-9][0-9][0-9][0-9]))";
	private static final String dateTemplate1 = "(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-]((175[7-9])|(17[6-9][0-9])|(1[8-9][0-9][0-9])|([2-9][0-9][0-9][0-9]))";
	private static final String keyTemplate = "([0-9]{3}-[A-Z]{1})";
	
	public static boolean validateKey(String key) { // COMPRUEBA FORMATO DE LA CLAVE
		Pattern pattern = Pattern.compile(keyTemplate);
		Matcher matcher = pattern.matcher(key);
		return matcher.matches();
	}
	
	public static String inputKey(String message, String title) { // PEDIR CLAVE
		String key = "";
		boolean check = true;
		do {
			check = true;
			key = Strings.inputStringNoClose(message, title);
			if (validateKey(key) == false) {
				check = false;
			} 
		} while (!check);
		return key;
	}
	
	public static boolean validateDate(String date) { // COMPRUEBA FORMATO DE LA FECHA dd/mm/yyyy
		Pattern pattern = null;
		Matcher matcher = null; 
		if (Main.config.getFormatDate() == "dd/MM/yyyy") {
			pattern = Pattern.compile(dateTemplate0);
			matcher = pattern.matcher(date);
		} 
		if (Main.config.getFormatDate() == "dd-MM-yyyy") {
			pattern = Pattern.compile(dateTemplate1);
			matcher = pattern.matcher(date);
		}
		return matcher.matches();
	}
	
	
	public static boolean leapYear(int year) { // COMPRUEBA AÑO BISIESTO
		if ((year % 4) == 0 && (year % 100) != 0 || (year % 400) == 0)
			return true;
		else 
			return false;
	}
	
	
	public static Dates inputDate(String message, String title) { // SOLICITA FECHA CON EL FORMATO dd/mm/yyyy
		String date = "";
		Dates dateObj = null;
		String[] parameters = null;
		int day = 0, month = 0, year = 0;
		int[] dayMonths={0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
		boolean check = true;
		do {
			check = true;
			dayMonths[2] = 28;
			date = Strings.inputStringNoClose(message, "Date");
			if (validateDate(date) == false) {
				check = false;
			} else {
				if (Main.config.getFormatDate() == "dd/MM/yyyy") {
					parameters = date.split("/");
				}
				if (Main.config.getFormatDate() == "dd-MM-yyyy") {
					parameters = date.split("-");
				}
				day = Integer.parseInt(parameters[0]);
				month = Integer.parseInt(parameters[1]);
				year = Integer.parseInt(parameters[2]);
				System.out.println("day = " + day);
				System.out.println("month = " + month);
				System.out.println("year = " + year);
				
				if (month == 2 && leapYear(year) == true) { // SI EL AÑO ES BISIESTO
					dayMonths[2] = 29;
				}

				if (day > dayMonths[month]) { // SI EL DIA SUPERA EL MÁXIMO DEL MES
					System.out.println("incorrect day");
					check = false;
				}
					
			}
						
		} while (!check);
		
		dateObj = new Dates(date);
		return dateObj;
	}
	
	
	public static Dates inputDateBeforeToday (String message, String title) { // SOLICITAR FECHA ANTES DE HOY
		Dates dateObj = null;
		boolean check = false;
		do {
			dateObj = inputDate(message, title);
			if (!dateObj.isAfterToday())
				check = true;
		} while (!check);
		return dateObj;
	}
	
	
	public static Dates inputDateAfterToday (String message, String title) { // SOLICITAR FECHA DESPUÉS DE HOY
		Dates dateObj = null;
		boolean check = false;
		do {
			dateObj = inputDate(message, title);
			if (dateObj.isAfterToday())
				check = true;
		} while (!check);
		return dateObj;
	}
	
	
	public static Dates inputDateAfterX (String message, String title, String date1) { // INTRODUCIR FECHA DESPUES DE X
		Dates dateObj = null; // DEBE SER DESPUÉS DE dateObj (DATE2)
		Dates dateComp = new Dates(date1); // DATE 1
		boolean check = false;
		do {
			dateObj = inputDate(message, title);
			if (dateComp.getDaysInterval(dateObj.toString()) > 0)
				check = true;
		} while (!check);
		return dateObj;
	}

	
	public static Dates inputDateBeforeX (String message, String title, String date1) { // INTRODUCIR FECHA ANTES DE X
		Dates dateObj = null; // DEBE SER DESPUÉS DE dateObj (DATE2)
		Dates dateComp = new Dates(date1); // DATE 1
		boolean check = false;
		do {
			dateObj = inputDate(message, title);
			if (dateComp.getDaysInterval(dateObj.toString()) < 0)
				check = true;
		} while (!check);
		return dateObj;
	}
	
	
	public static Dates inputDateBetweenXandY (String message, String title, String x, String y) {
		Dates dateObj = null;
		boolean check = false;
			do {
				dateObj = inputDate(message, title);
				// System.out.println("dateObj.isAfterOrEqualX(x) = " + dateObj.isAfterOrEqualX(x));
				// System.out.println("dateObj.isBeforeOrEqualX(y) = " + dateObj.isBeforeOrEqualX(y));
				if (dateObj.isAfterOrEqualX(x) && dateObj.isBeforeOrEqualX(y))
					check = true;
			} while (!check);
		return dateObj;
	}
	
	
	public static Dates today() { // PASA LA FECHA DE HOY
		Dates dateObj = new Dates(); 
		dateObj.setToday();
		return dateObj;
	}
	
}








