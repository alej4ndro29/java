package utils;

import javax.swing.JOptionPane;

public class Strings {

	public static String inputString(String message, String title) { // INTRODUCIR STRING
		String string = "";
		string = JOptionPane.showInputDialog(null, message, title, JOptionPane.QUESTION_MESSAGE);
		System.out.println("string = " + string);
		return string;
	}
	
	
	
	public static String inputStringNoClose(String message, String title) { //INTRODUCIR STRING NO CERRAR VENTANA
		String string = "";
		do {
			string = JOptionPane.showInputDialog(null, message, title, JOptionPane.QUESTION_MESSAGE);
			System.out.println("string = " + string);
		} while (string == null);
		return string;
	}
	
}
