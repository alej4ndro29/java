package utils.languajes;

import main.Main;

public class Esp {
	public static String message (String option) {
		String s = "";
		
		switch (option) {
			case "book":
				s = "Libro";
				break;
			case "bookPresentation":
				s = "Libro con presentación";
				break;
			case "article":
				s = "Artículo";
				break;
			case "movie":
				s = "Película";
				break;
			case "config":
				s = "Configuración";
				break;
			case "option":
				s = "Opción";
				break;
			case "languaje":
				s = "Idioma";
				break;
			case "errKeyRegistered":
				s = "Clave ya registrada";
				break;
			case "askKey":
				s = "Clave 000-A";
				break;
			case "askKeyTitle":
				s = "Clave";
				break;
			case "askName":
				s = "Nombre";
				break;
			case "askAuthor":
				s = "Autor";
				break;
			case "askThematic":
				s = "Temática";
				break;
			case "askRelease":
				s = "Lanzamiento " + Main.config.getFormatDate();
				break;
			case "askEditorial":
				s = "Editorial";
				break;
			case "askPresentationStart":
				s = "Inicio de la presentación " + Main.config.getFormatDate();
				break;
			case "presentationStart":
				s = "Inicio de la presentación";
				break;
			case "askPresentationEnd":
				s = "Final de la presentación " + Main.config.getFormatDate();
				break;
			case "presentationEnd":
				s = "Final de la presentación";
				break;
			case "askMeans":
				s = "Medio";
				break;
			case "askStartShop":
				s = "Inicio de la fecha de compra " + Main.config.getFormatDate();
				break;
			case "askEndShop":
				s = "Fin de la fecha de compra " + Main.config.getFormatDate();
				break;
			case "askPurchaseDay":
				s = "Día de compra " + Main.config.getFormatDate();
				break;
			case "askUnitPrice":
				s = "Precio por unidad";
				break;
			case "askQuantity":
				s = "Cantidad";
				break;
			case "askPreview":
				s = "Avance";
				break;
			case "askCompany":
				s = "Compañía";
				break;
				
			case "create":
				s = "Crear";
				break;
			case "read":
				s = "Leer";
				break;
			case "update":
				s = "Actualizar";
				break;
			case "delete":
				s = "Eliminar";
				break;
				
			case "chooseTitle":
				s = "-- Clave -- Nombre --";
				break;
				
			case "errNoExist":
				s = "No existe";
				break;
				
			case "all":
				s = "Todos";
				break;
			case "one":
				s = "Uno";
				break;
			case "allOrOne":
				s = "¿Todos o uno?";
				break;
				
			case "noBooks":
				s = "No hay libros registrados";
				break;
			case "noArticles":
				s = "No hay artículos registrados";
				break;
			case "noMovies":
				s = "No hay películas registradas";
				break;

			case "dummies":
				s = "Dummies";
				break;
			case "formatDate":
				s = "Formato de fecha";
				break;
			case "currency":
				s = "Moneda";
				break;
			case "enable":
				s = "Activar";
				break;
			case "disable":
				s = "Desactivar";
				break;
			case "euro":
				s = "Euro";
				break;
			case "dollar":
				s = "Dólar";
				break;
			case "periodReleaseAdded":
				s = "PeriodoLanzamientoAñadido";
				break;
			case "release":
				s = "Lanzamiento";
				break;
			case "optionOrder":
				s = "Opción para ordenar";
				break;
			case "order":
				s = "Ordenar";
				break;
			case "remove":
				s = "Eliminar";
				break;
			case "deleted":
				s = " eliminado";
				break;
			case "object":
				s = "Objeto";
				break;
			case "apply":
				s = "Aplicar";
				break;
			case "applyChanges":
				s = "¿Aplicar cambios?";
				break;
			case "startShop":
				s = "Inicio de compra";
				break;
			case "endShop":
				s = "Fin de compra";
				break;
			case "purchaseDay":
				s = "Día de compra";
				break;
			case "addedDate":
				s = "Día registrado";
				break;
			case "presentationDays":
				s = "Días de presentación";
				break;
			case "daysShop":
				s = "Días de compra";
				break;
			case "totalPrice":
				s = "Precio total";
				break;
				
			case "yes":
				s = "Sí";
				break;
			case "no":
				s = "No";
				break;
			case "errDummies":
				s = ("Dummies desactivados," + "\n" + "¿quieres activarlos?");
				break;
			case "howMany":
				s = "¿Cuántos?";
				break;
			default:
				System.err.println("Esp-Bad option --> " + option);
				break;
			}
			
		return s;
	}
}
