package utils.languajes;

import classes.config.Config;
import main.Main;

public class Languaje {
	public static String show(String option) {
		String s = "";
		
		if (Main.config.getLanguage() == Config.ENG) {
			s = Eng.message(option);
		}
		
		if (Main.config.getLanguage() == Config.ESP) {
			s = Esp.message(option);
		}
		
		if (Main.config.getLanguage() == Config.VAL) {
			s = Val.message(option);
		}
		
		
//		if (LanguajeTester.config.getLanguage() == Config.ENG) {
//			
//		}
//		
//		if (LanguajeTester.config.getLanguage() == Config.ESP) {
//			s = Esp.message(option);
//		}
//		
//		if (LanguajeTester.config.getLanguage() == Config.VAL) {
//			
//		}
		
		return s;
	}
}
