package utils.languajes;

import main.Main;

public class Val {
	public static String message (String option) {
		String s = "";
		
		switch (option) {
			case "book":
				s = "Llibre";
				break;
			case "bookPresentation":
				s = "Llibre amb presentació";
				break;
			case "article":
				s = "Article";
				break;
			case "movie":
				s = "Pel·lícula";
				break;
			case "config":
				s = "Configuració";
				break;
			case "option":
				s = "Opció";
				break;
			case "languaje":
				s = "Idioma";
				break;
			case "errKeyRegistered":
				s = "Clau ja registrada";
				break;
			case "askKey":
				s = "Clau 000-A";
				break;
			case "askKeyTitle":
				s = "Clau";
				break;
			case "askName":
				s = "Nom";
				break;
			case "askAuthor":
				s = "Autor";
				break;
			case "askThematic":
				s = "Temàtica";
				break;
			case "askRelease":
				s = "Llançament " + Main.config.getFormatDate();
				break;
			case "askEditorial":
				s = "Editorial";
				break;
			case "askPresentationStart":
				s = "Inici de la presentació " + Main.config.getFormatDate();
				break;
			case "presentationStart":
				s = "Inici de la presentació";
				break;
			case "askPresentationEnd":
				s = "Fi de la presentació " + Main.config.getFormatDate();
				break;
			case "presentationEnd":
				s = "Fi de la presentació";
				break;
			case "askMeans":
				s = "Medi";
				break;
			case "askStartShop":
				s = "Inici de la data de compra " + Main.config.getFormatDate();
				break;
			case "askEndShop":
				s = "Fi de la data de compra " + Main.config.getFormatDate();
				break;
			case "askPurchaseDay":
				s = "Día de compra " + Main.config.getFormatDate();
				break;
			case "askUnitPrice":
				s = "Preu per unitat";
				break;
			case "askQuantity":
				s = "Quantitat";
				break;
			case "askPreview":
				s = "Avanç";
				break;
			case "askCompany":
				s = "Companyia";
				break;
				
			case "create":
				s = "Crear";
				break;
			case "read":
				s = "Llegir";
				break;
			case "update":
				s = "Actualitzar";
				break;
			case "delete":
				s = "Eliminar";
				break;
				
			case "chooseTitle":
				s = "-- Clau -- Nom --";
				break;
				
			case "errNoExist":
				s = "No existeix";
				break;
				
			case "all":
				s = "Tots";
				break;
			case "one":
				s = "Un";
				break;
			case "allOrOne":
				s = "Tots o un?";
				break;
				
			case "noBooks":
				s = "No hi ha llibres registrats";
				break;
			case "noArticles":
				s = "No hi ha articles registrats";
				break;
			case "noMovies":
				s = "No hi ha pel·licules registrades";
				break;

			case "dummies":
				s = "Dummies";
				break;
			case "formatDate":
				s = "Format de data";
				break;
			case "currency":
				s = "Moneda";
				break;
			case "enable":
				s = "Activar";
				break;
			case "disable":
				s = "Desactivar";
				break;
			case "euro":
				s = "Euro";
				break;
			case "dollar":
				s = "Dòlar";
				break;
			case "periodReleaseAdded":
				s = "PeríodeLlançamentAfegit";
				break;
			case "release":
				s = "Llançament";
				break;
			case "optionOrder":
				s = "Opció per ordenar";
				break;
			case "order":
				s = "Ordenar";
				break;
			case "remove":
				s = "Eliminar";
				break;
			case "deleted":
				s = " eliminat";
				break;
			case "object":
				s = "Objecte";
				break;
			case "apply":
				s = "Aplicar";
				break;
			case "applyChanges":
				s = "Aplicar canvi?";
				break;
			case "startShop":
				s = "Inici de compra";
				break;
			case "endShop":
				s = "Fi de compra";
				break;
			case "purchaseDay":
				s = "Dia de compra";
				break;
			case "addedDate":
				s = "Dia registrat";
				break;
			case "presentationDays":
				s = "Dies de presentació";
				break;
			case "daysShop":
				s = "Dies de compra";
				break;
			case "totalPrice":
				s = "Preu total";
				break;
				
			case "yes":
				s = "Sí";
				break;
			case "no":
				s = "No";
				break;
			case "errDummies":
				s = ("Dummies desactivats," + "\n" + "vols activar-los?");
				break;
			case "howMany":
				s = "Quants?";
				break;
			default:
				System.err.println("Val-Bad option --> " + option);
				break;
		}
		
		return s;
	}
}
