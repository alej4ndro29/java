package tests;

import java.util.ArrayList;

import modules.resource.classes.Article;
import modules.resource.classes.Book;
import modules.resource.classes.BookPresentation;
import modules.resource.classes.Movie;
import modules.resource.classes.Singleton;
import modules.resource.utils.ChooseObject;
import modules.resource.utils.crud.Read;

public class ReadTester {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Singleton.book = new ArrayList<Book>();
		Singleton.bookPresentation = new ArrayList<BookPresentation>();
		Singleton.article = new ArrayList <Article>();
		Singleton.movie	= new ArrayList<Movie>();
		
//		Book book1 = null;
//		String key = "000-A";
//		book1 = new Book (key);
//		Singleton.book.add(book1);
//		
//		key = "001-A";
//		book1 = new Book (key);
//		Singleton.book.add(book1);
//		
//		key = "002-A";
//		book1 = new Book (key);
//		Singleton.book.add(book1);
//		
//		key = "003-A";
//		book1 = new Book (key);
//		Singleton.book.add(book1);
//		
//		Read.readBook();
		
//		BookPresentation book1 = null;
//		String key = "000-A";
//		book1 = new BookPresentation(key);
//		Singleton.bookPresentation.add(book1);
//		
//		key = "001-A";
//		book1 = new BookPresentation(key);
//		Singleton.bookPresentation.add(book1);
//		
//		key = "002-A";
//		book1 = new BookPresentation(key);
//		Singleton.bookPresentation.add(book1);
//		
//		key = "003-A";
//		book1 = new BookPresentation(key);
//		Singleton.bookPresentation.add(book1);
//		
//		Read.readBookPresenation("000-S");
		
//		Article book1 = null;
//		String key = "000-A";
//		book1 = new Article(key);
//		Singleton.article.add(book1);
//		
//		key = "001-A";
//		book1 = new Article(key);
//		Singleton.article.add(book1);
//		
//		key = "002-A";
//		book1 = new Article(key);
//		Singleton.article.add(book1);
//		
//		key = "003-A";
//		book1 = new Article(key);
//		Singleton.article.add(book1);	
//		
//		Read.readArticle("003-A");
		
		Movie book1 = null;
		String key = "000-A";
		book1 = new Movie(key);
		Singleton.movie.add(book1);
		
		key = "001-A";
		book1 = new Movie(key);
		Singleton.movie.add(book1);
		
		key = "002-A";
		book1 = new Movie(key);
		Singleton.movie.add(book1);
		
		key = "003-A";
		book1 = new Movie(key);
		Singleton.movie.add(book1);
		
		Read.readMovie("000-A");
		
//		Create.createMovie();
		
		
	}

}
