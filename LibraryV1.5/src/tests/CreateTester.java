package tests;

import java.util.ArrayList;

import classes.dates.Dates;
import modules.resource.classes.*;
import modules.resource.classes.Singleton;
import modules.resource.utils.crud.Create;

public class CreateTester {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Singleton.book = new ArrayList<Book>();
		Singleton.bookPresentation = new ArrayList<BookPresentation>();
		Singleton.article = new ArrayList <Article>();
		Singleton.movie	= new ArrayList<Movie>();
		

		Book book1 = null;
		String key = "000-A";
		book1 = new Book (key);
		Singleton.book.add(book1);

		BookPresentation bookPresentation1 = null;
		key = "000-A";
		bookPresentation1 = new BookPresentation(key);
		Singleton.bookPresentation.add(bookPresentation1);
		
		Article article1 = null;
		key = "000-A";
		article1 = new Article(key);
		Singleton.article.add(article1);
		
		Movie movie1 = null;
		key = "000-A";
		movie1 = new Movie(key);
		Singleton.movie.add(movie1);
		
		
		do {
			Create.createBook();
//			Create.createBookPresentation();
//			Create.createArticle();
//			Create.createMovie();
		} while(true);
		
	}

}
