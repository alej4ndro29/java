package examen;

import utils.*;

public class Examen {

	public static void main(String[] args) {
		
		int repeatOption = 0;
		boolean exit = false;
		
		String[] options = {"Ex 1", "Ex 2", "Ex 3"};
		
		do {
			
			switch (ComboBox.comboBox("Exercise:", "Selector", options)) {
			case 0:
				repeatOption = FuntionsExamen.ej1();
				break;
			case 1:
				repeatOption = FuntionsExamen.ej2();
				break;
			case 2:
				repeatOption = FuntionsExamen.ej3();
				break;							
			default:
				exit = true;
				break;
			}
			
			
			if (repeatOption == 2) {
				exit = true;
			}
			
		} while (!exit);
		
	}

}
