package examen;

import javax.swing.JOptionPane;

import utils.*;

public class FuntionsExamen {
	
	public static int ej1 () { // EJERCICIO 1
		int repeatOption = 0, n = 0, num = 0, aux = 0, rest = 0, product = 0;
		do {
			System.out.println("Ej 1");
			
			n = Numbers.inputNumberPos("How many times?", "How many times?");
			
			for (int i = 0 ; i < n ; i++) { // REPETIR n VECES
				System.out.println("i = " + i);
				product = 1;
				num = Numbers.inputNumberGreaterX("Number, -1 for exit.", "Number", -2);
				
				if (num != -1) {
				
					aux = num;
					
					while (num != 0) { // CALCULAR MÚLTIPLOS
						rest = (num % 10);
						num = (num / 10);
						System.out.println("rest = "+ rest);
						System.out.println("num = "+ num);
						
						if ((rest % 2) == 0 || (rest % 3) == 0) { // MULTIPLICAR MÚLTIPLOS DE 2 O 3
							product = (product * rest);
							System.out.println("Multiple: product = "+ product);
						}
						
					}
					
					num = aux;
					
					if (product == 1) { // SI NO HAY MÚLTIPLOS DE 3 O 2
						JOptionPane.showMessageDialog(null, num + " does not have multiples of 2 or 3 ", "Result", JOptionPane.INFORMATION_MESSAGE);
						
					} else if (product >= 20) { // SI EL PRODUCTO ES MAYOR O IGUAL A 20
						JOptionPane.showMessageDialog(null, "Product of the multiples of 2 or 3 of " + num + " --> " + product + 
								"\n" + product + " >= 20", "Result", JOptionPane.INFORMATION_MESSAGE);
						
					} else { // SI EL PRODUCTO ES MENOR QUE 20
						JOptionPane.showMessageDialog(null, "Product of the multiples of 2 or 3 of " + num + " --> " + product + 
								"\n" + product + " < 20", "Result", JOptionPane.INFORMATION_MESSAGE);
					}
					
				} else { // SALIR
					System.out.println("exit");
					i = n;
				}
				
			}
			
			
			
			repeatOption = ButtonBox.repeatButton();
		} while (repeatOption == 0);
		
		return repeatOption;
	}
	
	/////////////////////////
	
	public static int ej2 () { // EJERCICIO 2
		int repeatOption = 0, num = 0, sum = 0;
		do {
			System.out.println("Ej 2");
			sum = 0;
			
			do { // MENU CONTINUAR
				
				num = Numbers.inputNumber("Number:", "Number");
				
				if (Digits.countDigitsPair(num) == 2) { // CONTAR Y COMPROBAR SI TIENE 2 DÍGITOS PARES
					System.out.println(num + " 2 pair digits.");
					sum = (sum + num);
					System.out.println("sum = " + sum);
				}
				
			} while (ButtonBox.continueButton() == 0);
			
			if (sum == 0) { // SI NINGÚN NÚMERO HA TENIDO 2 DÍGITOS PARES
				JOptionPane.showMessageDialog(null, "Any number has met the conditions", "Result", JOptionPane.INFORMATION_MESSAGE);
			} else { 
				JOptionPane.showMessageDialog(null, "The sum is: " + sum, "Result", JOptionPane.INFORMATION_MESSAGE);
				
			}
			
			repeatOption = ButtonBox.repeatButton();
		} while (repeatOption == 0);
		
		return repeatOption;
	}
	
	/////////////////////////
	
	public static int ej3 () { // EJERCICIO 3
		int repeatOption = 0, num = 0, pairCont = 0, contAverage = 0, average = 0;
		do {
			System.out.println("Ej 3");
			pairCont = 0;
			average = 0;
			contAverage = 0;
			
			while (pairCont != 3) {
				num = Numbers.inputNumberPos("Number", "Number");
				if ((num % 2) == 0) { // SI SE INTRODUCE NÚMERO PAR
					
					if (Dividers.countPairDividers(num) == 2) { // COMPROBAR DIVISORES PARES
						average = (average + num);
						contAverage++;
						System.out.println("average = " + average);
						System.out.println("contAverage = " + contAverage);
					}
					
					pairCont++;
				}
				System.out.println("pairCont = " + pairCont);
			}
			
			if (average == 0) { // SI NINGÚN NÚMERO HA CUMPLIDO LAS ANTERIORES CONDICIONES
				JOptionPane.showMessageDialog(null, "Any number has met the conditions", "Result", JOptionPane.INFORMATION_MESSAGE);
			} else {
				average = (average / contAverage);
				JOptionPane.showMessageDialog(null, "The average is: " + average, "Result", JOptionPane.INFORMATION_MESSAGE);
			}
			
			repeatOption = ButtonBox.repeatButton();
		} while (repeatOption == 0);
		
		return repeatOption;
	}

}
