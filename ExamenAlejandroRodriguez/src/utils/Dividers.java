package utils;

public class Dividers {
	public static int sumDividers (int number) { // SUMAR DIVISORES 
			int sum = 0;
			number = Math.abs(number);
			
			for (int i = 1 ; i <= number ; i++) {
				if ((number % i) == 0) {
					System.out.println(i + " divider " + number);
					sum = sum + i;
				}
			}
			
			return sum;
		}

	////////////////////////////
	
	public static int countPairDividers (int number) { // CONTAR DIVISORES PARES 
		int cont = 0;
		number = Math.abs(number);
		
		for (int i = 1 ; i <= number ; i++) {
			if ((number % i) == 0 && (i % 2) == 0) {
				System.out.println(i + " divider " + number);
				cont++;
			}
		}
		System.out.println(cont + " dividers pair");
		
		return cont;
	}
	
}
