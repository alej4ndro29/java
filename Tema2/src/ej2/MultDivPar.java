package ej2;

import javax.swing.JOptionPane;

public class MultDivPar {

	public static void main(String[] args) {
		String snum = "", cad = "";
		int num = 0, product = 1;
		boolean check0 = false, interrupt = false;
		
		while (interrupt == false) {
			// SOLICITAR num
			do {
				 snum = JOptionPane.showInputDialog(null, "Introduce un número.\n" + "0 para salir.", "Número", JOptionPane.QUESTION_MESSAGE);
				 try {
					 num = Integer.parseInt(snum);
					 check0 = true;
					 if (num < 0) {
						 check0 = false;
					 }
					 System.out.println("comprobador = " + check0);
				 } catch (Exception e) {
					 check0 = false;
					 System.out.println("Error: " + e.getMessage());
				 }
			} while (check0 != true);
			
			//SALIR
			if (num == 0) {
				interrupt = true;
				System.out.println("interrupt = " + interrupt + "\nSalir");
			} else {
				
				cad = ("Divisores de " + num + "\n");
				
				//CALCULAR DIVISORES
				for (int i = 1 ; i <= num ; i++) {
					if ((num % i) == 0) {
						//MULTIPLICAR DIVISORES
						System.out.println(i + " es divisor de " + num);
						cad = (cad + i + ", ");
						product = product * i;
					}
					
				}
				
				JOptionPane.showMessageDialog(null, cad + "\nLA multiplicación de los divisores es:\n" + product, "Resultado", JOptionPane.INFORMATION_MESSAGE);
				
			}
			
		}
		
	}

}
