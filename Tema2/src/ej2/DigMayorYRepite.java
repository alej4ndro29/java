package ej2;

import javax.swing.JOptionPane;

public class DigMayorYRepite {

	public static void main(String[] args) {
		String snum = "";
		int n = 0, num = 0 , aux = 0 , resto = 0; 
		int max = 0, cont = 0; 
		boolean check0 = false, check1 = false;
		
		
		// SOLICITAR n
		do {
			 snum = JOptionPane.showInputDialog(null, "¿Cuántas veces?", "Veces", JOptionPane.QUESTION_MESSAGE);
			 try {
				 n = Integer.parseInt(snum);
				 check0 = true;
				 if (num < 0) {
					 check0 = false;
				 }
				 System.out.println("comprobador = " + check0);
			 } catch (Exception e) {
				 check0 = false;
				 System.out.println("Error: " + e.getMessage());
			 }
		} while (check0 != true);
		
		
		//REPETIR n VECES
		for (int i = 0 ; i < n ; i++) {
			// SOLICITAR num
			do {
				 snum = JOptionPane.showInputDialog(null, "Introduce un número", "Número", JOptionPane.QUESTION_MESSAGE);
				 try {
					 num = Integer.parseInt(snum);
					 check0 = true;
					 if (num <= 0) {
						 check0 = false;
					 }
					 System.out.println("comprobador = " + check0);
				 } catch (Exception e) {
					 check0 = false;
					 System.out.println("Error: " + e.getMessage());
				 }
			} while (check0 != true);
			
			aux = num;
			
			check1 = false;
			
			while (aux != 0) {
				resto = aux % 10;
				aux = aux / 10;
				
				if (!check1) {
					max = resto;
					cont = 0;
					check1 = true;
				} else if (resto > max) {
					max = resto;
					cont = 0;
				} else if (resto == max) {
					cont++;
				}
				
				System.out.println("num = " + num + " resto = " + resto + " aux = " + aux + " max = " + max + " cont = " + cont);
				
			}
			
			JOptionPane.showMessageDialog(null, num + ":\nEl dígito mayor es " + max + "\nSe repite: " + cont + " veces.", "Resultado", JOptionPane.INFORMATION_MESSAGE);
			
		}
		
		
	}

}
