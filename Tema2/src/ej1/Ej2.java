package ej1;

import javax.swing.JOptionPane;

public class Ej2 {

	public static void main(String[] args) {
		String snum = "";
		int num = 0 , mayor = 0 , cont = 0;
		boolean interrupt = false , interrupt2 = false , check = false;
		
		
		while (!interrupt) {
			do {
				//SOLICITAR num
				 snum = JOptionPane.showInputDialog(null, "Introduce un número, salir con 0", "Número", JOptionPane.QUESTION_MESSAGE);
				 try {
					 num = Integer.parseInt(snum);
					 check = true;
					 System.out.println("comprobador = " + check);
				 } catch (Exception e) {
					 check = false;
					 System.out.println("comprobador = " + check);
				 }
			} while (check != true);
			
			if (num == 0) {
				//SALIR
				interrupt = true;
			} else {
				if (!interrupt2) {
					//PRIMER num
					mayor = num;
					interrupt2 = true;
				} else if (num > mayor) {
					//ENTRA UN num MAYOR
					mayor = num;
					cont = 0;
				} else if (num == mayor) {
					//ENTRA UN num IGUAL
					cont++;
				}
				
				System.out.println("cont = " + cont);
				
			}
			
		}
		
		JOptionPane.showMessageDialog(null, "El número mayor ha sido: " + mayor + "\n\n" + "Se ha repetido " + cont + " veces.", "Resultado", JOptionPane.INFORMATION_MESSAGE);
				
	}

}
