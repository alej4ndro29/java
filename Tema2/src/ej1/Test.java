package ej1;

import javax.print.attribute.standard.PrinterLocation;
import javax.swing.JOptionPane;

public class Test {

	public static void main(String[] args) {
		String snum = "";
		int num = 0, numchar = 0 , k = 0;
		boolean check = false;
		
		do {
			 snum = JOptionPane.showInputDialog(null, "Introduce un número", "Número", JOptionPane.QUESTION_MESSAGE);
			 try {
				 num = Integer.parseInt(snum);
				 check = true;
				 if (num < 0) {
					 check = false;
				 }
				 System.out.println("comprobador = " + check);
			 } catch (Exception e) {
				 check = false;
				 System.out.println("Error: " + e.getMessage());
			 }
		} while (check != true);
		
		numchar = Integer.toString(num).length();
		
		System.out.println(num + " tiene " + numchar + " dígitos.");
		
		///////////////////////////////
		do {
			 snum = JOptionPane.showInputDialog(null, "Introduce k", "Número", JOptionPane.QUESTION_MESSAGE);
			 try {
				 k = Integer.parseInt(snum);
				 check = true;
				 if (k < 0 || k > numchar) {
					 check = false;
				 }
				 System.out.println("comprobador = " + check);
			 } catch (Exception e) {
				 check = false;
				 System.out.println("Error: " + e.getMessage());
			 }
		} while (check != true);
		
		snum = "1";
		
		for (int i = 1; i <= k ; i++) {
			snum = (snum + "0");
			System.out.println(i);
		}
		
		System.out.println(snum);
		
	}

}
