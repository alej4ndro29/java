package ej1;

import javax.swing.JOptionPane;

public class Ej1 {

	public static void main(String[] args) {
		String snum="" , cad = "";
		int num = 0 , suma = 0 , resto = 0 , aux = 0;
		int continuar = 0;
		boolean comprobador = false;
		
		
		do {
			cad = "";
			suma = 0;
			//SOLICITAR NUM
			do {
				 snum = JOptionPane.showInputDialog(null, "Introduce un número", "Número", JOptionPane.QUESTION_MESSAGE);
				 try {
					 num = Integer.parseInt(snum);
					 comprobador = true;
					 if (num < 0) {
						 comprobador = false;
					 }
					 System.out.println("comprobador = " + comprobador);
				 } catch (Exception e) {
					 comprobador = false;
					 //System.err.println("Error: "+e.getMessage());
					 //System.out.println("comprobador = " + comprobador);
					 System.out.println("Error: " + e.getMessage());
				 }
			} while (comprobador != true);
			
			aux = num;
			
			//COMPROBAR DÍGITOS
			while (num > 0) {
				resto = (num % 10);
				num = (num / 10);
				System.out.println("resto = " + resto);
				System.out.println("num = " + num);
				if ((resto % 2) == 0) {
					cad = (cad + resto + ", ");
					suma = (suma + resto); 
				}
					
			}
			
			System.out.println("cad = " + cad);
			System.out.println("suma = " + suma);
			
			//MOSTRAR RESULTADO
			if (cad == "") {
				JOptionPane.showMessageDialog(null, aux + " no tiene ningún dígito par.", "Resultado", JOptionPane.INFORMATION_MESSAGE);
			} else {
				JOptionPane.showMessageDialog(null, "Los números pares de " + aux + " son:" + "\n" + cad + "\n\n" + "Y la suma es:" + "\n" + suma);
			}
			
			continuar = JOptionPane.showConfirmDialog(null, "¿Continuar?");
		} while (continuar == JOptionPane.OK_OPTION);
		
	}

}
