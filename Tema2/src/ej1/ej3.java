package ej1;

import javax.swing.JOptionPane;

public class ej3 {

	public static void main(String[] args) {
		String snum = "";
		int n = 0, aux = 0, k = 0, nchar = 0, resto = 0;
		int continua = 0;
		boolean correct = false;

		do {
			// SOLICITAR n
			do {
				snum = JOptionPane.showInputDialog(null, "Introduce n", "Solicitar n", JOptionPane.QUESTION_MESSAGE);
				try {
					n = Integer.parseInt(snum);
					correct = true;
					System.out.println("correct = " + correct);
				} catch (Exception e) {
					correct = false;
					System.err.println(e.getMessage());
				}
			} while (!correct);
			
			aux = n;
			
			// CONTAR CARACTERES DE n
			nchar = Integer.toString(n).length();
			System.out.println(n + " tiene " + nchar + " dígitos.");
			
				/*
				while (aux != 0) {
					resto = aux % 10;
					aux = aux / 10;
					nchar++;
					System.out.println("resto = " + resto + " aux = " + aux + " nchar = " + nchar);
				}
				*/
			
			// SOLICITAR k
			do {
				snum = JOptionPane.showInputDialog(null, "Introduce k", "Solicitar k", JOptionPane.QUESTION_MESSAGE);
				try {
					k = Integer.parseInt(snum);
					correct = true;
					if (k > nchar) {
						JOptionPane.showMessageDialog(null, n + " no tiene tantos dígitos.", "Error", JOptionPane.WARNING_MESSAGE);
						correct = false;
					}
					
					System.out.println("correct = " + correct);
				} catch (Exception e) {
					correct = false;
					System.err.println(e.getMessage());
				}
			} while (!correct);			
			
			
			// CALCULAR DÍGITO k DE n
			aux = n;
			for (int i = 0; i < k; i++) {
				resto = aux % 10;
				aux = aux / 10;
				System.out.println("Repetición " + (i+1));
			}
			
			// MOSTRAR DÍGITO k DE n
			JOptionPane.showMessageDialog(null, "El dígito " + k + " de " + n + " es " + resto, "Resultado", JOptionPane.INFORMATION_MESSAGE);
			
			// ¿CONTINUAR?
			continua = JOptionPane.showConfirmDialog(null, "¿Continuar?");
		} while (continua == JOptionPane.OK_OPTION);

	}

}
