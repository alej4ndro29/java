package ej1;

import javax.swing.JOptionPane;

public class Ej6 {

	public static void main(String[] args) {
		String snum = "" , cad = "";
		int num = 0, n = 0, product = 0;
		int opcion = 0;
		boolean comprobador = false;
		
		//SOLICITAR n
		do {
			 snum = JOptionPane.showInputDialog(null, "Introduce el número máximo.", "Número", JOptionPane.QUESTION_MESSAGE);
			 try {
				 n = Integer.parseInt(snum);
				 comprobador = true;
				 System.out.println("comprobador = " + comprobador);
			 } catch (Exception e) {
				 comprobador = false;
				 System.out.println("comprobador = " + comprobador);
			 }
		} while (!comprobador);
		
		do {
			product = 1;
			//SOLICITAR num
			do {
				 snum = JOptionPane.showInputDialog(null, "Introduce un número entre 1 y " + n, "Número", JOptionPane.QUESTION_MESSAGE);
				 try {
					 num = Integer.parseInt(snum);
					 comprobador = true;
					 if (num < 1 || num > n) {
						 comprobador = false;
					 }
					 System.out.println("comprobador = " + comprobador);
				 } catch (Exception e) {
					 comprobador = false;
					 System.out.println("comprobador = " + comprobador);
				 }
			} while (comprobador != true);
			cad = ("Los divisores de " + num + " son:" + "\n");
			
			
			//COMPROBAR DIVISORES
			for (int i = 1 ; i <= num ; i++) {
				if ((num % i) == 0) {
					cad = (cad + i + ", ");
					product = product * i;
					System.out.println(i + " es múltiplo de " + num);
					System.out.println("product = " + product);
				}
			}
			
			if (product <= 10) {
				JOptionPane.showMessageDialog(null, cad + "\n\n" + "El produco de los divisores no supera 10:" + "\n" + product, "Resultado", JOptionPane.INFORMATION_MESSAGE);
			} else {
				JOptionPane.showMessageDialog(null, cad + "\n\n" + "El produco de los divisores supera 10:" + "\n" + product, "Resultado", JOptionPane.INFORMATION_MESSAGE);
			}
			
			opcion = JOptionPane.showConfirmDialog(null, "¿Continuar?");
		} while (opcion == JOptionPane.OK_OPTION);
		
	
	}

}
