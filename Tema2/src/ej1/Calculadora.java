package ej1;

import javax.swing.JOptionPane;

public class Calculadora {

	public static void main(String[] args) {
		String snum="";
		int num1 = 0, num2 = 0;
		int continuar = 0 , option = 0;
		boolean check = false, check2 = false;
			
		do {
			option = JOptionPane.showOptionDialog(
					null,
					"¿Qué deseas realizar?",
					"Operación",
					JOptionPane.YES_NO_CANCEL_OPTION,
					JOptionPane.QUESTION_MESSAGE,
					null,
					new Object[] {"+", "-", "*" , "/", "Salir"}, //0+ 1- 2* 3/ 4S
					null);

			if (option == 4 || option == JOptionPane.CLOSED_OPTION) {
				//SALIR
				System.out.println("salir");
				continuar = 0;
				
			} else {
				//OPERACIONES
				System.out.println("seguir");
				
				switch (option) {
					case 0:
						//SUMAR
						do {
							//num1
							do {
								snum = JOptionPane.showInputDialog(null, "Introduce el primer número:" , "Primer valor" , JOptionPane.QUESTION_MESSAGE);
								try {
									num1 = Integer.parseInt(snum);
									check = true;
									System.out.println("check = " + check);
								} catch (Exception e) {
									check = false;
									System.out.println("check = " + check);
								}
							} while (!check);
							
							//num2
							do {
								snum = JOptionPane.showInputDialog(null, "Introduce el segundo número:" , "Segundo valor" , JOptionPane.QUESTION_MESSAGE);
								try {
									num2 = Integer.parseInt(snum);
									check = true;
									System.out.println("check = " + check);
								} catch (Exception e) {
									check = false;
									System.out.println("check = " + check);
								}
							} while (!check);
							
							JOptionPane.showMessageDialog(null, num1 + " + " + num2 + " = " + (num1+num2), "Suma", JOptionPane.INFORMATION_MESSAGE);
						
						continuar = JOptionPane.showConfirmDialog(null, "¿Continuar?");
						
						
					} while (continuar == JOptionPane.OK_OPTION);
						
						break;
					case 1:
						//RESTAR
						do {
							//num1
							do {
								snum = JOptionPane.showInputDialog(null, "Introduce el primer número:" , "Primer valor" , JOptionPane.QUESTION_MESSAGE);
								try {
									num1 = Integer.parseInt(snum);
									check = true;
									System.out.println("check = " + check);
								} catch (Exception e) {
									check = false;
									System.out.println("check = " + check);
								}
							} while (!check);
							
							//num2
							do {
								snum = JOptionPane.showInputDialog(null, "Introduce el segundo número:" , "Segundo valor" , JOptionPane.QUESTION_MESSAGE);
								try {
									num2 = Integer.parseInt(snum);
									check = true;
									System.out.println("check = " + check);
								} catch (Exception e) {
									check = false;
									System.out.println("check = " + check);
								}
							} while (!check);
							
							JOptionPane.showMessageDialog(null, num1 + " - " + num2 + " = " + (num1-num2), "Resta", JOptionPane.INFORMATION_MESSAGE);
							continuar = JOptionPane.showConfirmDialog(null, "¿Continuar?");
							
						
						} while (continuar == JOptionPane.OK_OPTION);
						break;
					case 2:
						//MULTIPLICAR
						do {
							//num1
							do {
								//PEDIR num1
								snum = JOptionPane.showInputDialog(null, "Introduce el primer número:" , "Primer valor" , JOptionPane.QUESTION_MESSAGE);
								try {
									num1 = Integer.parseInt(snum);
									check = true;
									
									//COMPROBAR SI ES 0
									if (num1 == 0) {
										do {
											option = JOptionPane.showOptionDialog(
													null,
													"¿Multiplicar por 0?",
													"Operación",
													JOptionPane.YES_NO_CANCEL_OPTION,
													JOptionPane.WARNING_MESSAGE,
													null,
													new Object[] {"Sí" , "No"}, //0-Sí 1-No
													null);
											if (option == JOptionPane.CLOSED_OPTION) {
												check2 = false;
											} else {
												check2 = true;
											}
										} while (!check2);
										if (option == 1) {
											check = false;
										}
										System.out.println("check = " + check);
									} // end comprobar 0
									
								} catch (Exception e) {
									check = false;
									System.out.println("check = " + check);
								}
							} while (!check);
							
							//num2
							do {
								snum = JOptionPane.showInputDialog(null, "Introduce el segundo número:" , "Segundo valor" , JOptionPane.QUESTION_MESSAGE);
								try {
									num2 = Integer.parseInt(snum);
									check = true;
									
									//COMPROBAR SI ES 0
									if (num2 == 0) {
										do {
											option = JOptionPane.showOptionDialog(
													null,
													"¿Multiplicar por 0?",
													"Operación",
													JOptionPane.YES_NO_CANCEL_OPTION,
													JOptionPane.WARNING_MESSAGE,
													null,
													new Object[] {"Sí" , "No"}, //0-Sí 1-No
													null);
											if (option == JOptionPane.CLOSED_OPTION) {
												check2 = false;
											} else {
												check2 = true;
											}
										} while (!check2);
										if (option == 1) {
											check = false;
										}
										System.out.println("check = " + check);
									} // end comprobar 0
									
									System.out.println("check = " + check);
								} catch (Exception e) {
									check = false;
									System.out.println("check = " + check);
								}
							} while (!check);
							
							JOptionPane.showMessageDialog(null, num1 + " * " + num2 + " = " + (num1*num2), "Multiplicación", JOptionPane.INFORMATION_MESSAGE);
						
							continuar = JOptionPane.showConfirmDialog(null, "¿Continuar?");

						} while (continuar == JOptionPane.OK_OPTION); 
						break;
					case 3:
						//DIVIDIR
						do {
							//num1
							do {
								snum = JOptionPane.showInputDialog(null, "Introduce el primer número:" , "Primer valor" , JOptionPane.QUESTION_MESSAGE);
								try {
									num1 = Integer.parseInt(snum);
									check = true;
									System.out.println("check = " + check);
								} catch (Exception e) {
									check = false;
									System.out.println("check = " + check);
								}
							} while (!check);
							
							//num2
							do {
								snum = JOptionPane.showInputDialog(null, "Introduce el segundo número:" , "Segundo valor" , JOptionPane.QUESTION_MESSAGE);
								try {
									num2 = Integer.parseInt(snum);
									check = true;
									if (num2 == 0) {
										JOptionPane.showMessageDialog(null, "No puedes dividir entre 0", "Error", JOptionPane.WARNING_MESSAGE);
										check = false;
									}
									System.out.println("check = " + check);
								} catch (Exception e) {
									check = false;
									System.out.println("check = " + check);
								}
							} while (!check);
							
							JOptionPane.showMessageDialog(null, num1 + " / " + num2 + " = " + (num1/num2), "División", JOptionPane.INFORMATION_MESSAGE);
							
							continuar = JOptionPane.showConfirmDialog(null, "¿Continuar?");
						} while (continuar == JOptionPane.OK_OPTION);
						break;
				}
				
				// ¿CONTINUAR?
				// continuar = JOptionPane.showConfirmDialog(null, "¿Continuar?");
			}
			
		} while (continuar != 0);
		
	}

}
