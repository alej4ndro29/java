package classes.config;

import javax.swing.JOptionPane;

import main.Main;
import utils.ButtonBox;
import utils.languajes.Languaje;

public class FunctionsConfig {

	public static void configMenu() {
		String options[] = {Languaje.show("languaje"), 
							Languaje.show("dummies"), 
							Languaje.show("formatDate"), 
							Languaje.show("currency")};
		switch (ButtonBox.buttonsOptions(Languaje.show("config") + ":", Languaje.show("config"), options)) {
			case 0:
				configLanguajeMenu();
				break;
			case 1:
				if (Main.config.getActiveUser() == null || Main.config.getActiveUser().getType() != "admin")
					JOptionPane.showMessageDialog(null, "You don't have permission for this", "Error", JOptionPane.ERROR_MESSAGE);
				else
					configDummiesMenu();
				break;
			case 2:
				configDateMenu();
				break;
			case 3:
				configCurrencyMenu();
				break;
		}
	}
	
	public static void configLanguajeMenu() {
		String options[] = {"ENG", "ESP", "VAL"};
		switch (ButtonBox.buttonsOptions(Languaje.show("languaje") + ":", Languaje.show("languaje"), options)) {
			case Config.ENG:
				Main.config.setLanguage(Config.ENG);
				break;
			case Config.ESP:
				Main.config.setLanguage(Config.ESP);
				break;
			case Config.VAL:
				Main.config.setLanguage(Config.VAL);
				break;
		}
	}

	
	public static void configDummiesMenu() {
		String options[] = {Languaje.show("enable"), Languaje.show("disable")};
		switch (ButtonBox.buttonsOptions(Languaje.show("dummies"), Languaje.show("dummies"), options)) {
			case 0:
				Main.config.setDummies(true);
				break;
			case 1:
				Main.config.setDummies(false);
				break;
		}
	}
	
	public static void configDateMenu() {
		String options[] = {"dd/mm/yyyy", "dd-mm-yyyy"};
		switch (ButtonBox.buttonsOptions("DateFormat", "DateFormat", options)) {
			case 0:
				Main.config.setFormatDate("dd/MM/yyyy");
				break;
			case 1:
				Main.config.setFormatDate("dd-MM-yyyy");
				break;
		}
	}
	
	public static void configCurrencyMenu() {
		String options[] = {Languaje.show("euro"), Languaje.show("dollar")};
		switch (ButtonBox.buttonsOptions(Languaje.show("currency"), Languaje.show("currency"), options)) {
			 case 0:
				Main.config.setCurrencyEUR();
			 	break;
			 case 1:
				Main.config.setCurrencyDOL(); 
				break;
		}
	}
}
