package classes.config;

import java.util.ArrayList;
import java.util.Locale;

import modules.login.classes.Admin;
import modules.login.classes.Client;
import modules.login.classes.SingletonUser;
import modules.login.classes.User;
import modules.resource.classes.Article;
import modules.resource.classes.Book;
import modules.resource.classes.BookPresentation;
import modules.resource.classes.Movie;
import modules.resource.classes.SingletonResource;

public class Config {
	public final static int ENG = 0;
	public final static int ESP = 1;
	public final static int VAL = 2;
	
	private int language;
	private boolean dummies;
	private String formatDate;
	private Locale currency;
	private User activeUser;
	
	
	public Config() {
		this.language = ENG;
		this.dummies = false;
		this.formatDate = "dd/MM/yyyy";
//		this.formatDate = "dd-MM-yyyy";
		this.currency = new Locale("es", "ES");
		
		SingletonResource.article = new ArrayList <Article>();
		SingletonResource.book = new ArrayList<Book>();
		SingletonResource.bookPresentation = new ArrayList<BookPresentation>();
		SingletonResource.movie	= new ArrayList<Movie>();
		
		SingletonUser.admin = new ArrayList<Admin>();
		SingletonUser.client = new ArrayList<Client>();
	}
	
	public int getLanguage() {
		return language;
	}
	
	public boolean getDummies() {
		return dummies;
	}
	
	public String getFormatDate() {
		return formatDate;
	}
	
	public Locale getCurrency() {
		return currency;
	}
	
	public User getActiveUser() {
		return activeUser;
	}
	
	public void setLanguage(int language) {
		this.language = language;
	}
	
	public void setDummies(boolean dummies) {
		this.dummies = dummies;
	}
	
	public void setActiveUser(User user) {
		this.activeUser = user;
	}		
	
	public void setActiveUser() {
		this.activeUser = null;
	}	
	
	public void setFormatDate(String format) {
		this.formatDate = format;
	}
	
	public void setCurrencyEUR() {
		this.currency = new Locale("fr", "FR");
	}
	
	public void setCurrencyDOL() {
		this.currency = new Locale("en", "US");
	}
	
}
