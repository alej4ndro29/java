package tests;

import java.util.ArrayList;

import modules.resource.classes.Article;
import modules.resource.classes.Book;
import modules.resource.classes.BookPresentation;
import modules.resource.classes.Movie;
import modules.resource.classes.SingletonResource;
import modules.resource.utils.ChooseObject;
import modules.resource.utils.crud.ResourceRead;

public class ReadTester {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SingletonResource.book = new ArrayList<Book>();
		SingletonResource.bookPresentation = new ArrayList<BookPresentation>();
		SingletonResource.article = new ArrayList <Article>();
		SingletonResource.movie	= new ArrayList<Movie>();
		
//		Book book1 = null;
//		String key = "000-A";
//		book1 = new Book (key);
//		Singleton.book.add(book1);
//		
//		key = "001-A";
//		book1 = new Book (key);
//		Singleton.book.add(book1);
//		
//		key = "002-A";
//		book1 = new Book (key);
//		Singleton.book.add(book1);
//		
//		key = "003-A";
//		book1 = new Book (key);
//		Singleton.book.add(book1);
//		
//		Read.readBook();
		
//		BookPresentation book1 = null;
//		String key = "000-A";
//		book1 = new BookPresentation(key);
//		Singleton.bookPresentation.add(book1);
//		
//		key = "001-A";
//		book1 = new BookPresentation(key);
//		Singleton.bookPresentation.add(book1);
//		
//		key = "002-A";
//		book1 = new BookPresentation(key);
//		Singleton.bookPresentation.add(book1);
//		
//		key = "003-A";
//		book1 = new BookPresentation(key);
//		Singleton.bookPresentation.add(book1);
//		
//		Read.readBookPresenation("000-S");
		
//		Article book1 = null;
//		String key = "000-A";
//		book1 = new Article(key);
//		Singleton.article.add(book1);
//		
//		key = "001-A";
//		book1 = new Article(key);
//		Singleton.article.add(book1);
//		
//		key = "002-A";
//		book1 = new Article(key);
//		Singleton.article.add(book1);
//		
//		key = "003-A";
//		book1 = new Article(key);
//		Singleton.article.add(book1);	
//		
//		Read.readArticle("003-A");
		
		Movie book1 = null;
		String key = "000-A";
		book1 = new Movie(key);
		SingletonResource.movie.add(book1);
		
		key = "001-A";
		book1 = new Movie(key);
		SingletonResource.movie.add(book1);
		
		key = "002-A";
		book1 = new Movie(key);
		SingletonResource.movie.add(book1);
		
		key = "003-A";
		book1 = new Movie(key);
		SingletonResource.movie.add(book1);
		
		ResourceRead.readMovie("000-A");
		
//		Create.createMovie();
		
		
	}

}
