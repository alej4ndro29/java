package tests;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import modules.resource.classes.Article;
import modules.resource.classes.Book;
import modules.resource.classes.BookPresentation;
import modules.resource.classes.Movie;
import modules.resource.classes.SingletonResource;
import modules.resource.utils.Find;
import utils.ValidateData;

public class FindTester {
	public static void main(String[] args) {
		SingletonResource.article = new ArrayList <Article> ();
		SingletonResource.book = new ArrayList<Book>();
		SingletonResource.bookPresentation = new ArrayList<BookPresentation>();
		SingletonResource.movie	= new ArrayList<Movie>();
		
//		BookPresentation object = new BookPresentation("000-A");
//		Singleton.bookPresentation.add(object);
//		object = new BookPresentation("001-A");
//		Singleton.bookPresentation.add(object);
//		object = new BookPresentation("002-A");
//		Singleton.bookPresentation.add(object);
//		object = new BookPresentation("003-A");
//		Singleton.bookPresentation.add(object);
//		
//		while (true) {
//			String key = ValidateData.inputKey("Key 000-A", "Key");
//			JOptionPane.showMessageDialog(null, Find.findBookPresentation(key), "Position", JOptionPane.DEFAULT_OPTION);
//		}
		
		
		Book object = new Book("999-Z");
		SingletonResource.book.add(object);
		object = new Book("001-A");
		SingletonResource.book.add(object);
		object = new Book("002-A");
		SingletonResource.book.add(object);
		object = new Book("003-A");
		SingletonResource.book.add(object);
		
//		System.out.println(Singleton.book.size());
		
		while (true) {
			String key = ValidateData.inputKey("Key 000-A", "Key");
			JOptionPane.showMessageDialog(null, Find.findBook(key), "Position", JOptionPane.INFORMATION_MESSAGE);
		}
		
	}
}
