package modules.login.classes;

import classes.dates.Dates;
import utils.MD5;
import utils.ValidateData;

public abstract class User {
	private String DNI;
	private String password;
	private String name;
	private String surnames;
	private Dates birthdate;
	private Dates registrationDate;
	private String type;
	
	public User(String DNI, String password, String name, String surnames, Dates birthdate, String type) {
		this.DNI = DNI;
		this.password = password;
		this.name = name;
		this.surnames = surnames;
		this.birthdate = birthdate;
		this.registrationDate = ValidateData.today();
		this.type = type;
	}
	
	public String getDNI() {
		return DNI;
	}

	public String getPassword() {
		return password;
	}
	
	public String getName() {
		return name;
	}

	public String getSurnames() {
		return surnames;
	}

	public Dates getBirthdate() {
		return birthdate;
	}
	
	public int getYearsOld() {
		return birthdate.getYearsInterval(ValidateData.today().toString());
	}

	public Dates getRegistrationDate() {
		return registrationDate;
	}
	
	public String getType() {
		return type;
	}
	
	public boolean comparePassword(String pass) {
		return MD5.enc(pass).equals(this.password);
	}
	
	
	public void setDNI(String DNI) {
		this.DNI = DNI;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSurnames(String surnames) {
		this.surnames = surnames;
	}

	public void setBirthdate(Dates birthdate) {
		this.birthdate = birthdate;
	}

	public void setRegistrationDate(Dates registrationDate) {
		this.registrationDate = registrationDate;
	}

	
	public String toString() {
		return "User [DNI=" + DNI + ", name=" + name + ", surnames=" + surnames + ", birthdate=" + birthdate
				+ ", registrationDate=" + registrationDate + "]";
	}
	
	
	
}
