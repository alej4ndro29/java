package modules.login.classes.dummies;

import classes.dates.Dates;
import modules.login.classes.Admin;
import modules.login.classes.SingletonUser;
import utils.MD5;

public class DumUser {
	
	public static void createDefaultAdmin() {
		Admin admin = null;
		String DNI = "12345678-A";
		String password = MD5.enc("admin");
		String name = "admin";
		String surnames = "admin";
		Dates birthdate = new Dates();
		Dates contractDate = new Dates();
		
		admin = new Admin(DNI, password, name, surnames, birthdate, contractDate);
		
		SingletonUser.admin.add(admin);		
	}
	
	
	
}
