package modules.login.classes;

import classes.dates.Dates;

public class Admin extends User {
	private Dates contractDate;
	
	public Admin(String DNI, String password, String name, String surnames, Dates birthdate, Dates contractDate) {
		super(DNI, password, name, surnames, birthdate, "admin");
		this.contractDate = contractDate;
	}

	public Dates getContractDate() {
		return contractDate;
	}

	public void setContractDate(Dates contractDate) {
		this.contractDate = contractDate;
	}
	
	public Admin copy() {
		return new Admin(this.getDNI(), this.getPassword(), this.getName(), this.getSurnames(), this.getBirthdate(), this.contractDate);
	}
	
	public String toString() {
		return "User [ DNI = " + getDNI() 			+ "\n" +
				"name = "		+ getName()			+ "\n" +
				"surnames = "	+ getSurnames()		+ "\n" +
				"birthDate = " 	+ getBirthdate()	+ "\n" +
				"yearsOld = "	+ getYearsOld()		+ "\n" +
				"type = "		+ getType()			+ "\n" +
				"contractDate = " + contractDate + "]";
	}
	
	
	
}
