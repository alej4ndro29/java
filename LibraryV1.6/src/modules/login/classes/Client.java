package modules.login.classes;

import classes.dates.Dates;

public class Client extends User {
	private int purchases;
	public Client(String DNI, String password, String name, String surnames, Dates birthdate) {
		super(DNI, password, name, surnames, birthdate, "client");
		this.purchases = 0;
	}
	
	public Client(String DNI, String password, String name, String surnames, Dates birthdate, int purchases) {
		super(DNI, password, name, surnames, birthdate, "client");
		this.purchases = purchases;
	}
	
	public int getPurchases() {
		return purchases;
	}
	public void setPurchases(int purchases) {
		this.purchases = purchases;
	}
	
	public Client copy() {
		return new Client(this.getDNI(), this.getPassword(), this.getName(), this.getSurnames(), this.getBirthdate(), this.purchases);
	}
	
	public String toString() {
		return "User [ DNI = " + getDNI() 			+ "\n" +
				"name = "		+ getName()			+ "\n" +
				"surnames = "	+ getSurnames()		+ "\n" +
				"birthDate = " 	+ getBirthdate()	+ "\n" +
				"yearsOld = "	+ getYearsOld()		+ "\n" +
				"type = "		+ getType()			+ "\n" +
				"purchases = "	+ purchases			+ "]";
	} 

}
