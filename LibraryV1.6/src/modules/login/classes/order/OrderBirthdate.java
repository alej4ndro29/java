package modules.login.classes.order;

import java.util.Comparator;

import modules.login.classes.User;

public class OrderBirthdate implements Comparator<User>{

	@Override
	public int compare(User o1, User o2) {
		// TODO Auto-generated method stub
		if(o1.getBirthdate().compare(o2.getBirthdate()) == -1)
			return 1;
		if(o1.getBirthdate().compare(o2.getBirthdate()) == 1)
			return -1;
		return 0;
	}


}
