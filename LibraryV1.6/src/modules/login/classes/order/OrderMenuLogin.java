package modules.login.classes.order;

import java.util.Collections;

import modules.login.classes.SingletonUser;
import utils.ButtonBox;

public class OrderMenuLogin {
	public static void orderUsers() {
		String[] options = {"DNI", "Brithdate"};
		switch (ButtonBox.buttonsOptions("Order:", "Order", options)) {
			case 0:
				Collections.sort(SingletonUser.admin, new OrderDNI());
				Collections.sort(SingletonUser.client, new OrderDNI());
				break;
			case 1:
				Collections.sort(SingletonUser.admin, new OrderBirthdate());
				Collections.sort(SingletonUser.client, new OrderBirthdate());
				break;
		}
	}
}
