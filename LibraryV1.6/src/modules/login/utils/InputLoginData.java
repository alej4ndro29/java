package modules.login.utils;

import classes.dates.Dates;
import main.Main;
import modules.login.classes.Admin;
import modules.login.classes.Client;
import modules.login.classes.User;
import utils.MD5;
import utils.Strings;
import utils.ValidateData;

public class InputLoginData {
	public static User askAdmin(String DNI) {
		String password = MD5.enc(Strings.inputStringNoClose("Password", "Password"));
		String name = Strings.inputStringNoClose("Name", "Name");
		String surnames = Strings.inputStringNoClose("Surnames", "Surnames"); 
		Dates birthdate = ValidateData.inputDateBeforeToday("Birthdate " + Main.config.getFormatDate(), "Birthdate");
		Dates contractDate = ValidateData.inputDateBeforeToday("Contract date", "Contract date");
		return new Admin(DNI, password, name, surnames, birthdate, contractDate);
	}
	
	public static User askClient(String DNI) {
		String password = MD5.enc(Strings.inputStringNoClose("Password", "Password"));
		String name = Strings.inputStringNoClose("Name", "Name");
		String surnames = Strings.inputStringNoClose("Surnames", "Surnames"); 
		Dates birthdate = ValidateData.inputDateBeforeToday("Birthdate " + Main.config.getFormatDate(), "Birthdate");
		return new Client(DNI, password, name, surnames, birthdate);
	}
}
