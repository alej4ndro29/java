package modules.login.utils;

import javax.swing.JOptionPane;

import main.Main;
import modules.login.utils.crud.LoginCreate;
import modules.login.utils.crud.LoginUpdate;
import utils.ButtonBox;

public class LoginMenu {
	public static void loginMainMenu() {
		String[] options = {"Login", "Register", "Recover Pass"};
		switch (ButtonBox.buttonsOptions("Login", "Login", options)) {
			case 0:
				loginOption();
				break;
			case 1:
				LoginCreate.createUser();
				break;
			case 2:
				LoginUpdate.recPassword();
				break;
		}
		
	}

	public static void loginOption() {
		boolean check = false;
		do {
			check = true;
			String DNI = JOptionPane.showInputDialog("DNI: 00000000-X");
			if (DNI == null) {
				check = true;
			} else
			if (FindUser.FindPosition(DNI) == -1) {
				JOptionPane.showMessageDialog(null, DNI + " not exist", "Error", JOptionPane.ERROR_MESSAGE);
				check = false;
			} else {
//				System.out.println(FindUser.getUser(DNI));
				Main.config.setActiveUser(FindUser.getUser(DNI));
				if (!Main.config.getActiveUser().comparePassword(JOptionPane.showInputDialog("Password:"))) {
					JOptionPane.showMessageDialog(null, "Invalid password", "Error", JOptionPane.ERROR_MESSAGE);
					check = false;
					Main.config.setActiveUser();
				}
					
			}
		} while (!check);
	}
	
}
