package modules.login.utils.crud;

import javax.swing.JOptionPane;

import main.Main;
import modules.login.classes.SingletonUser;
import modules.login.utils.FindUser;
import utils.ValidateData;

public class LoginDelete {
	public static void deleteUserByAdmin() {
		String DNI = "";
		String names = "Dni + Names" + "\n";
		boolean check = false;

		for (int i = 0; i <= (SingletonUser.admin.size() - 1); i++) {
			names = names + SingletonUser.admin.get(i).getDNI() + " - " + SingletonUser.admin.get(i).getName() + "\n";
		}
		for (int i = 0; i <= (SingletonUser.client.size() - 1); i++) {
			names = names + SingletonUser.client.get(i).getDNI() + " - " + SingletonUser.client.get(i).getName() + "\n";
		}

		do {
			check = true;
			DNI = ValidateData.inputDNI(names, "DNI");
			if (FindUser.FindPosition(DNI) == -1) {
				JOptionPane.showMessageDialog(null, DNI + " invalid", "Error", JOptionPane.ERROR_MESSAGE);
				check = false;
			}
		} while (!check);
		

		if (JOptionPane.showConfirmDialog(null, "Remove? \n" + FindUser.getUser(DNI).toString()) == 0) {
			if (FindUser.getUser(DNI).getType() == "admin") {
				SingletonUser.admin.remove(FindUser.FindPosition(DNI));
			}
			if (FindUser.getUser(DNI).getType() == "client") {
				SingletonUser.client.remove(FindUser.FindPosition(DNI));
			}
			JOptionPane.showMessageDialog(null, "Deleted", "Deleted", JOptionPane.DEFAULT_OPTION);
		}
	}
	
	public static void deleteByClient() {
		String DNI = Main.config.getActiveUser().getDNI(); 
		if (JOptionPane.showConfirmDialog(null, "Do you want delete your user?") == 0) {
			SingletonUser.client.remove(FindUser.FindPosition(DNI));
			Main.config.setActiveUser();
		}
	}
	
}
