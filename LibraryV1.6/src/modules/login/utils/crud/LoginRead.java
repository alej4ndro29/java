package modules.login.utils.crud;

import javax.swing.JOptionPane;

import modules.login.classes.SingletonUser;
import modules.login.utils.FindUser;
import utils.ButtonBox;
import utils.ValidateData;
import utils.languajes.Languaje;

public class LoginRead {
	public static void readUser() {
		String DNI = "";
		String s = "";
		String names = "Dni + Names" + "\n";
		String[] options = {Languaje.show("all"), Languaje.show("one")};
		boolean check = false;
		
		for (int i = 0; i <= (SingletonUser.admin.size() - 1); i++) {
			names = names + SingletonUser.admin.get(i).getDNI() + " - " + SingletonUser.admin.get(i).getName() +"\n";
		}
		for (int i = 0; i <= (SingletonUser.client.size() - 1); i++) {
			names = names + SingletonUser.client.get(i).getDNI() + " - " + SingletonUser.client.get(i).getName() + "\n";
		}
		
		
		switch (ButtonBox.buttonsOptionsNoClose(Languaje.show("allOrOne"), Languaje.show("allOrOne"), options)) {
			case 0:
				for (int i = 0; i <= (SingletonUser.admin.size() - 1); i++) {
					s = s + SingletonUser.admin.get(i).toString() + "\n\n";
				}
				for (int i = 0; i <= (SingletonUser.client.size() - 1); i++) {
					s = s + SingletonUser.client.get(i).toString() + "\n\n";
				}
				break;
			case 1:
				do {
					check = true;
					DNI = ValidateData.inputDNI(names, "DNI");
					if (FindUser.FindPosition(DNI) == -1) {
						JOptionPane.showMessageDialog(null, DNI + " invalid", "Error", JOptionPane.ERROR_MESSAGE);
						check = false;
					}
				} while (!check);
					
				s = FindUser.getUser(DNI).toString();
				break;
		}
		
		JOptionPane.showMessageDialog(null, s, "Read", JOptionPane.INFORMATION_MESSAGE);
		
	}
}
