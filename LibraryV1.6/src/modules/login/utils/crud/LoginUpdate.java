package modules.login.utils.crud;

import javax.swing.JOptionPane;

import main.Main;
import modules.login.classes.Admin;
import modules.login.classes.Client;
import modules.login.classes.SingletonUser;
import modules.login.classes.User;
import modules.login.utils.FindUser;
import utils.ComboBox;
import utils.MD5;
import utils.Strings;
import utils.ValidateData;

public class LoginUpdate {
	public static void recPassword() {
		String DNI = ValidateData.inputDNI("DNI", "DNI");
		User userCopy = FindUser.getUser(DNI);
		int position = -1;
		if (userCopy == null) {
			JOptionPane.showMessageDialog(null, DNI + " not found", "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			position = FindUser.FindPosition(DNI);
			String birthdate = ValidateData.inputDate("Birthdate", "Birthdate").toString();
			if (birthdate.equals(userCopy.getBirthdate().toString())) {
				userCopy.setPassword(MD5.enc(Strings.inputStringNoClose("New password", "New password")));
				if (userCopy.getType() == "admin")
					SingletonUser.admin.set(position, (Admin) userCopy);
				if (userCopy.getType() == "client") 
					SingletonUser.client.set(position, (Client) userCopy);
				JOptionPane.showMessageDialog(null, "Updated password", "Updated password", JOptionPane.INFORMATION_MESSAGE);
			}
			else {
				JOptionPane.showMessageDialog(null, "Invalid credentials", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	public static void updateUser() {
		String DNI = "";
		String names = "Dni + Names" + "\n";
		boolean check = false;
		
		for (int i = 0; i <= (SingletonUser.admin.size() - 1); i++) {
			names = names + SingletonUser.admin.get(i).getDNI() + " - " + SingletonUser.admin.get(i).getName() +"\n";
		}
		for (int i = 0; i <= (SingletonUser.client.size() - 1); i++) {
			names = names + SingletonUser.client.get(i).getDNI() + " - " + SingletonUser.client.get(i).getName() + "\n";
		}
		
		do {
			check = true;
			DNI = ValidateData.inputDNI(names, "DNI");
			if (FindUser.FindPosition(DNI) == -1) {
				JOptionPane.showMessageDialog(null, DNI + " invalid", "Error", JOptionPane.ERROR_MESSAGE);
				check = false;
			}
		} while (!check);
		
		updateUser(DNI);
		
	}
	
	public static void updateUser(String DNI) {
		String[] optionsAdmin = {"Password", "Name", "Surnames", "Birthdate", "ContractDate", "DNI"};
		String[] optionsClient = {"Password", "Name", "Surnames", "Birthdate", "DNI"};
		String newDNI = "";
		boolean check = false;
		User userCopy = null;
		
		userCopy = FindUser.getUser(DNI);
		
		if (userCopy.getType() == "admin") {
			userCopy = (Admin) userCopy;
			switch (ComboBox.comboBox("Option to update", "Option", optionsAdmin)) {
				case 0: // PASSWORD
					userCopy.setPassword(MD5.enc(Strings.inputStringNoClose("Password", "Password")));
					break;
				case 1: // NAME
					userCopy.setName(Strings.inputStringNoClose("Name", "Name"));
					break;
				case 2: // SURNAMES
					userCopy.setSurnames(Strings.inputStringNoClose("Surnames", "Surnames"));
					break;
				case 3: // BIRTHDATE
					userCopy.setBirthdate(ValidateData.inputDateBeforeX("Birthdate", "Birthdate", ((Admin)userCopy).getRegistrationDate().toString()));
					break;
				case 4: // CONTRACTDATE
					((Admin)userCopy).setContractDate(ValidateData.inputDateAfterX("Contract date", "Contract", ((Admin)userCopy).getBirthdate().toString()));
					break;
				case 5: // DNI
					do {
						check = true;
						newDNI = ValidateData.inputDNI("New DNI", "DNI");
						if (FindUser.FindPosition(newDNI) != -1) {
							JOptionPane.showMessageDialog(null, newDNI + " exists", "Error", JOptionPane.ERROR_MESSAGE);
							check = false;
						}
					} while(!check);
					userCopy.setDNI(newDNI);
					break;
			}
			
			if (JOptionPane.showConfirmDialog(null, "Update? \n" + ((Admin)userCopy).toString()) == 0) {
				SingletonUser.admin.set(FindUser.FindPosition(DNI), ((Admin)userCopy));
			}
			
		}
		
		if (userCopy.getType() == "client") {
			switch (ComboBox.comboBox("Option to update", "Option", optionsClient)) {
				case 0: // PASSWORD
					userCopy.setPassword(MD5.enc(Strings.inputStringNoClose("Password", "Password")));
					break;
				case 1: // NAME
					userCopy.setName(Strings.inputStringNoClose("Name", "Name"));
					break;
				case 2: // SURNAME
					userCopy.setSurnames(Strings.inputStringNoClose("Surnames", "Surnames"));
					break;
				case 3: // BIRTHDATE
					userCopy.setBirthdate(ValidateData.inputDateBeforeToday("Birthdate", "Birthdate"));
					break;
				case 4:
					do {
						check = true;
						newDNI = ValidateData.inputDNI("New DNI", "DNI");
						if (FindUser.FindPosition(newDNI) != -1) {
							JOptionPane.showMessageDialog(null, newDNI + " exists", "Error", JOptionPane.ERROR_MESSAGE);
							check = false;
						}
					} while(!check);
					userCopy.setDNI(newDNI);
					break;
			}
			
			if (JOptionPane.showConfirmDialog(null, "Update? \n" + ((Client)userCopy).toString()) == 0) {
				Main.config.setActiveUser(userCopy);
				SingletonUser.client.set(FindUser.FindPosition(DNI), ((Client)userCopy));
			}
		
		}
	}
	
}
