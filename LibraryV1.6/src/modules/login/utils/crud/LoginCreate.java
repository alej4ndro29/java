package modules.login.utils.crud;

import javax.swing.JOptionPane;

import main.Main;
import modules.login.classes.Admin;
import modules.login.classes.Client;
import modules.login.classes.SingletonUser;
import modules.login.utils.FindUser;
import modules.login.utils.InputLoginData;
import utils.ButtonBox;
import utils.ValidateData;

public class LoginCreate {
	public static void createUser() {
		boolean check = false;
		boolean newAdmin = false;
		String DNI = null;
		String[] options = {"Yes", "No"};
		
		do {
			DNI = ValidateData.inputDNI("DNI: 00000000-A", "DNI");
			if (FindUser.FindPosition(DNI) != -1) {
				JOptionPane.showMessageDialog(null, DNI + " already exists", "Error", JOptionPane.ERROR_MESSAGE);
			} else {
				check = true;
			}
		} while (!check); 
		
		if (Main.config.getActiveUser() != null && Main.config.getActiveUser().getType() == "admin") { // SI HAY UN ADMIN LOGUEADO
			newAdmin = ButtonBox.buttonsBoolean("Admin user?", "admin?", options);
		}
		
		if (newAdmin) {
			SingletonUser.admin.add((Admin) InputLoginData.askAdmin(DNI));
		} else {
			SingletonUser.client.add((Client) InputLoginData.askClient(DNI));
			
		}
		
	}
}
