package modules.login.utils;


import modules.login.classes.Admin;
import modules.login.classes.Client;
import modules.login.classes.SingletonUser;
import modules.login.classes.User;

public class FindUser {
	
	public static int FindPosition (String DNI) {
		
		if (SingletonUser.admin.isEmpty() && SingletonUser.client.isEmpty()) {
			System.out.println("Empty");
		} else {
			System.out.println("noEmpty");
			for (int i = 0; i < SingletonUser.admin.size(); i++) {
				if (SingletonUser.admin.get(i).getDNI().equals(DNI)) {
					return i;
				}
			}
			for (int i = 0; i < SingletonUser.client.size(); i++) {
				if (SingletonUser.client.get(i).getDNI().equals(DNI)) {
					return i;
				}
			}
		}
			
		
		return -1;
	}
	
	public static User getUser(String DNI) {
		Admin admin = null;
		Client client = null;
		if (SingletonUser.admin.isEmpty() && SingletonUser.client.isEmpty()) {
			System.out.println("Empty");
		} else {
			System.out.println("noEmpty");
			for (int i = 0; i < SingletonUser.admin.size(); i++) {
				if (SingletonUser.admin.get(i).getDNI().equals(DNI)) {
					admin = SingletonUser.admin.get(i).copy();
					return admin;
				}
			}
			for (int i = 0; i < SingletonUser.client.size(); i++) {
				if (SingletonUser.client.get(i).getDNI().equals(DNI)) {
					client = SingletonUser.client.get(i).copy();
					return client;
				}
			}
		}
			
		return null;
	}
	
	public static String FindType(String DNI) {
		if (getUser(DNI) == null)
			return DNI + " not exist";
		else 
			return getUser(DNI).getType();
	}
	
}
