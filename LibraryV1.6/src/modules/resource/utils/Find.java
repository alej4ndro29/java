package modules.resource.utils;

import modules.resource.classes.*;

public class Find {
	
	/*
	 * return -1 CUANDO LA CLAVE NO ESTÁ GUARDADA
	 * SI ESTÁ GUARDADA DEVUELVE LA POSICIÓN
	 * DEL ArrayList
	 */
	
	public static int findBook (String key) { // ENCONTRAR LIBRO
		return findBook(key, 0);
	}
	
	
	public static int findBook (String key, int pos) { // ENCONTRAR LIBRO
		if (SingletonResource.book.size() == pos) {
			return -1;
		} else {
			if (SingletonResource.book.get(pos).getKey().equals(key)) {
				return pos;
			} else {
				return findBook(key, (pos + 1));
			}
		}
	}
	
	public static int findBookPresentation (String key) { // ENCONTRAR LIBRO CON PRESENTACIÓN
		return findBookPresentation(key, 0);
	}
	
	public static int findBookPresentation (String key, int pos) { // ENCONTRAR LIBRO CON PRESENTACIÓN
		if (SingletonResource.bookPresentation.size() == pos) {
			return -1;
		} else {
			if (SingletonResource.bookPresentation.get(pos).getKey().equals(key)) {
				return pos;
			} else {
				return findBookPresentation(key, (pos + 1));
			}
		}
	}
	
	public static int findArticle (String key) { // ENCONTRAR ARTÍCULO
		return findArticle(key, 0);
	}
	
	public static int findArticle (String key, int pos) { // ENCONTRAR ARTÍCULO
		if (SingletonResource.article.size() == pos) {
			return -1;
		} else {
			if (SingletonResource.article.get(pos).getKey().equals(key)) {
				return pos;
			} else {
				return findArticle(key, (pos + 1));
			}
		}
	}
	
	public static int findMovie (String key) { // ENCONTRAR PELÍCULA
		return findMovie(key, 0);
	}
	
	public static int findMovie (String key, int pos) { // ENCONTRAR PELÍCULA
		if (SingletonResource.movie.size() == pos) {
			return -1;
		} else {
			if (SingletonResource.movie.get(pos).getKey().equals(key)) {
				return pos;
			} else {
				return findMovie(key, (pos + 1));
			}
		}
		
	}
	
}


