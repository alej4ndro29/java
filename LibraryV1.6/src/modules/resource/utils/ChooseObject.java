package modules.resource.utils;

import javax.swing.JOptionPane;

import modules.resource.classes.SingletonResource;
import utils.ValidateData;
import utils.languajes.Languaje;

public class ChooseObject {
	/*
	 * MENÚ PARA ELEGIR UNA DE LAS POSICIONES
	 * DEL ARRAYLIST
	 */
	public static String book() {
		String key = "";
		String cad = "";
		int position = 0;
		Boolean check = false;
		
		cad = Languaje.show("chooseTitle") + "\n";
		for (int i = 0; i <= (SingletonResource.book.size() - 1); i++) {
			cad = cad + SingletonResource.book.get(i).getKey() + " -- " + SingletonResource.book.get(i).getName() + "\n";
		}
		
		do {
			key = ValidateData.inputKey(cad, Languaje.show("askKeyTitle"));
			position = Find.findBook(key);
			if (position == -1) {
				JOptionPane.showMessageDialog(null, key + " " + Languaje.show("errNoExist"), "Error", JOptionPane.ERROR_MESSAGE);
			} else {
				check = true;
			}
		} while (!check);
		
		return key;
		
	}
	
	
	public static String bookPresentation() {
		String key = "";
		String cad = "";
		int position = 0;
		Boolean check = false;
		
		cad = Languaje.show("chooseTitle") + "\n";
		for (int i = 0; i <= (SingletonResource.bookPresentation.size() - 1); i++) {
			cad = cad + SingletonResource.bookPresentation.get(i).getKey() + " -- " + SingletonResource.bookPresentation.get(i).getName() + "\n";
		}
		
		do {
			key = ValidateData.inputKey(cad, Languaje.show("askKeyTitle"));
			position = Find.findBookPresentation(key);
			if (position == -1) {
				JOptionPane.showMessageDialog(null, key + " " + Languaje.show("errNoExist"), "Error", JOptionPane.ERROR_MESSAGE);
			} else {
				check = true;
			}
		} while (!check);
		
		return key;
	}

	public static String article() {
		String key = "";
		String cad = "";
		int position = 0;
		Boolean check = false;
		
		cad = Languaje.show("chooseTitle") + "\n";
		for (int i = 0; i <= (SingletonResource.article.size() - 1); i++) {
			cad = cad + SingletonResource.article.get(i).getKey() + " -- " + SingletonResource.article.get(i).getName() + "\n";
		}
		
		do {
			key = ValidateData.inputKey(cad, Languaje.show("askKeyTitle"));
			position = Find.findArticle(key);
			if (position == -1) {
				JOptionPane.showMessageDialog(null, key + " " + Languaje.show("errNoExist"), "Error", JOptionPane.ERROR_MESSAGE);
			} else {
				check = true;
			}
		} while (!check);
		
		return key;
	}
	
	public static String movie() {
		String key = "";
		String cad = "";
		int position = 0;
		Boolean check = false;
		
		cad = Languaje.show("chooseTitle") + "\n";
		for (int i = 0; i <= (SingletonResource.movie.size() - 1); i++) {
			cad = cad + SingletonResource.movie.get(i).getKey() + " -- " + SingletonResource.movie.get(i).getName() + "\n";
		}
		
		do {
			key = ValidateData.inputKey(cad, "Key");
			position = Find.findMovie(key);
			if (position == -1) {
				JOptionPane.showMessageDialog(null, key + " " + Languaje.show("errNoExist"), "Error", JOptionPane.ERROR_MESSAGE);
			} else {
				check = true;
			}
		} while (!check);
		
		return key;
	}

	
}
