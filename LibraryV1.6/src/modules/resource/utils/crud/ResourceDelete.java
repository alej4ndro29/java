package modules.resource.utils.crud;

import javax.swing.JOptionPane;

import main.Main;
import modules.resource.classes.Resource;
import modules.resource.classes.SingletonResource;
import modules.resource.utils.ChooseObject;
import modules.resource.utils.Find;
import utils.languajes.Languaje;

public class ResourceDelete {
	
	public static void deleteResource() {
		switch (Main.resourceOption) {
			case Resource.BOOK:
				deleteBook();
				break;
			case Resource.BOOKPRESENTATION:
				deleteBookPresentation();
				break;
			case Resource.ARTICLE:
				deleteArticle();
				break;
			case Resource.MOVIE:
				deleteMovie();
				break;
		}
	}
	
	public static void deleteBook() {
		String key = "";
		int position = -1;
		
		if (SingletonResource.book.isEmpty()) {
			JOptionPane.showMessageDialog(null, Languaje.show("noBooks"), "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			key = ChooseObject.book();
			position = Find.findBook(key);
			//JOptionPane.showConfirmDialog(null, "Remove? \n" + Singleton.book.get(position).toString());
			//System.out.println(JOptionPane.showConfirmDialog(null, "Remove? \n" + Singleton.book.get(position).toString()));
			if (JOptionPane.showConfirmDialog(null, Languaje.show("remove") + "? \n" + SingletonResource.book.get(position).toString()) == 0) {
				SingletonResource.book.remove(position);
				JOptionPane.showMessageDialog(null, key + " deleted", "Ok", JOptionPane.INFORMATION_MESSAGE);
			}
		}
			
	}
	
	public static void deleteBookPresentation() {
		String key = "";
		int position = -1;
		
		if (SingletonResource.bookPresentation.isEmpty()) {
			JOptionPane.showMessageDialog(null, Languaje.show("noBooks"), "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			key = ChooseObject.bookPresentation();
			position = Find.findBookPresentation(key);
			if (JOptionPane.showConfirmDialog(null, Languaje.show("remove") + " \n" + SingletonResource.bookPresentation.get(position).toString()) == 0) {
				SingletonResource.bookPresentation.remove(position);
				JOptionPane.showMessageDialog(null, key + Languaje.show("deleted"), "Ok", JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}
	
	public static void deleteArticle() {
		String key = "";
		int position = -1;
		
		if (SingletonResource.article.isEmpty()) {
			JOptionPane.showMessageDialog(null, Languaje.show("noArticles"), "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			key = ChooseObject.article();
			position = Find.findArticle(key);
			if (JOptionPane.showConfirmDialog(null, Languaje.show("remove") + " \n" + SingletonResource.article.get(position).toString()) == 0) {
				SingletonResource.article.remove(position);
				JOptionPane.showMessageDialog(null, key + Languaje.show("deleted"), "Ok", JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}
	
	public static void deleteMovie() {
		String key = "";
		int position = -1;
		
		if (SingletonResource.movie.isEmpty()) {
			JOptionPane.showMessageDialog(null, Languaje.show("noMovies"), "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			key = ChooseObject.movie();
			position = Find.findMovie(key);
			if (JOptionPane.showConfirmDialog(null, Languaje.show("remove") + " \n" + SingletonResource.movie.get(position).toString()) == 0) {
				SingletonResource.movie.remove(position);
				JOptionPane.showMessageDialog(null, key + Languaje.show("deleted"), "Ok", JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}
}
