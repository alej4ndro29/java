package modules.resource.utils.crud;

import javax.swing.JOptionPane;

import main.Main;
import modules.resource.classes.Resource;
import modules.resource.classes.SingletonResource;
import modules.resource.utils.ChooseObject;
import modules.resource.utils.Find;
import utils.ButtonBox;
import utils.languajes.Languaje;

public class ResourceRead {
	
	public static void readResource() {
		switch (Main.resourceOption) {
			case Resource.BOOK:
				readBook();
				break;
			case Resource.BOOKPRESENTATION:
				readBookPresentation();
				break;
			case Resource.ARTICLE:
				readArticle();
				break;
			case Resource.MOVIE:
				readMovie();
				break;
		}
	}
	
	
	public static void readBook() {
		String cad = "";
		String[] options = {Languaje.show("all"), Languaje.show("one")};
		int size = SingletonResource.book.size();
		int position = 0;
		
		if (SingletonResource.book.isEmpty()) {
			JOptionPane.showMessageDialog(null, Languaje.show("noBooks"), "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			
			switch (ButtonBox.buttonsOptionsNoClose(Languaje.show("allOrOne"), Languaje.show("allOrOne"), options)) {
			case 0:
				for (int i = 0; i <= (size - 1); i++) {
					cad = cad + SingletonResource.book.get(i).toString() + "\n\n";
				}
				JOptionPane.showMessageDialog(null, cad, Languaje.show("object"), JOptionPane.INFORMATION_MESSAGE);
				break;

			case 1:
				position = Find.findBook(ChooseObject.book());
				JOptionPane.showMessageDialog(null, SingletonResource.book.get(position).toString(), Languaje.show("object"), JOptionPane.INFORMATION_MESSAGE);
				break;
			}
		}
	}
	
	public static void readBook(String key) {
		try {
			int position = 0;
			position = Find.findBook(key);
			JOptionPane.showMessageDialog(null, SingletonResource.book.get(position).toString(), Languaje.show("object"), JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception e) {
			System.err.println("Invalid key --> " + key);
		}
	}
	
	public static void readBook(int position) {
		try {
			JOptionPane.showMessageDialog(null, SingletonResource.book.get(position).toString(), Languaje.show("object"), JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception e) {
			System.err.println("invalid position --> " + position);
		}
	}
	
	//////////////////////////////////////////////////////
	
	public static void readBookPresentation() {
		String cad = "";
		String[] options = {Languaje.show("all"), Languaje.show("one")};
		int size = SingletonResource.bookPresentation.size();
		int position = 0;
		
		if (SingletonResource.bookPresentation.isEmpty()) {
			JOptionPane.showMessageDialog(null, Languaje.show("noBooks"), "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			
			switch (ButtonBox.buttonsOptionsNoClose(Languaje.show("allOrOne"), Languaje.show("allOrOne"), options)) {
			case 0:
				for (int i = 0; i <= (size - 1); i++) {
					cad = cad + SingletonResource.bookPresentation.get(i).toString() + "\n\n";
				}
				JOptionPane.showMessageDialog(null, cad, Languaje.show("object"), JOptionPane.INFORMATION_MESSAGE);
				break;

			case 1:
				position = Find.findBookPresentation(ChooseObject.bookPresentation());
				JOptionPane.showMessageDialog(null, SingletonResource.bookPresentation.get(position).toString(), Languaje.show("object"), JOptionPane.INFORMATION_MESSAGE);
				break;
			}
		}
	}
	
	public static void readBookPresentation(String key) {
		try {
			int position = 0;
			position = Find.findBookPresentation(key);
			JOptionPane.showMessageDialog(null, SingletonResource.bookPresentation.get(position).toString(), Languaje.show("object"), JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception e) {
			System.err.println("Invalid key --> " + key);
		}
	}
	
	public static void readBookPresentation(int position) {
		try {
			JOptionPane.showMessageDialog(null, SingletonResource.bookPresentation.get(position).toString(), Languaje.show("object"), JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception e) {
			System.err.println("invalid position --> " + position);
		}
	}
	
	//////////////////////////////////////////////////////
	
	public static void readArticle() {
		String cad = "";
		String[] options = {Languaje.show("all"), Languaje.show("one")};
		int size = SingletonResource.article.size();
		int position = 0;
		
		if (SingletonResource.article.isEmpty()) {
			JOptionPane.showMessageDialog(null, Languaje.show("noArticles"), "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			
			switch (ButtonBox.buttonsOptionsNoClose(Languaje.show("allOrOne"), Languaje.show("allOrOne"), options)) {
			case 0:
				for (int i = 0; i <= (size - 1); i++) {
					cad = cad + SingletonResource.article.get(i).toString() + "\n\n";
				}
				JOptionPane.showMessageDialog(null, cad, "Object", JOptionPane.INFORMATION_MESSAGE);
				break;

			case 1:
				position = Find.findArticle(ChooseObject.article());
				JOptionPane.showMessageDialog(null, SingletonResource.article.get(position).toString(), Languaje.show("object"), JOptionPane.INFORMATION_MESSAGE);
				break;
			}
		}
	}

	public static void readArticle(String key) {
		try {
			int position = 0;
			position = Find.findArticle(key);
			JOptionPane.showMessageDialog(null, SingletonResource.article.get(position).toString(), Languaje.show("object"), JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception e) {
			System.err.println("Invalid key --> " + key);
		}
	}
	
	public static void readArticle(int position) {
		try {
			JOptionPane.showMessageDialog(null, SingletonResource.article.get(position).toString(), Languaje.show("object"), JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception e) {
			System.err.println("invalid position --> " + position);
		}
	}
	
	//////////////////////////////////////////////////////

	public static void readMovie() {
		String cad = "";
		String[] options = {Languaje.show("all"), Languaje.show("one")};
		int size = SingletonResource.movie.size();
		int position = 0;
		
		if (SingletonResource.movie.isEmpty()) {
			JOptionPane.showMessageDialog(null, Languaje.show("noMovies"), "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			
			switch (ButtonBox.buttonsOptionsNoClose(Languaje.show("allOrOne"), Languaje.show("allOrOne"), options)) {
			case 0:
				for (int i = 0; i <= (size - 1); i++) {
					cad = cad + SingletonResource.movie.get(i).toString() + "\n\n";
				}
				JOptionPane.showMessageDialog(null, cad, "Object", JOptionPane.INFORMATION_MESSAGE);
				break;

			case 1:
				position = Find.findMovie(ChooseObject.movie());
				JOptionPane.showMessageDialog(null, SingletonResource.movie.get(position).toString(), Languaje.show("object"), JOptionPane.INFORMATION_MESSAGE);
				break;
			}
		}
	}
	
	public static void readMovie(String key) {
		try {
			int position = 0;
			position = Find.findMovie(key);
			JOptionPane.showMessageDialog(null, SingletonResource.movie.get(position).toString(), Languaje.show("object"), JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception e) {
			System.err.println("Invalid key --> " + key);
		}
	}

	public static void readMovie(int position) {
		try {
			JOptionPane.showMessageDialog(null, SingletonResource.movie.get(position).toString(), Languaje.show("object"), JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception e) {
			System.err.println("invalid position --> " + position);
		}
	}
	
}
