package modules.resource.utils.crud;

import javax.swing.JOptionPane;

import main.Main;
import modules.resource.classes.Article;
import modules.resource.classes.Book;
import modules.resource.classes.BookPresentation;
import modules.resource.classes.Movie;
import modules.resource.classes.Resource;
import modules.resource.classes.SingletonResource;
import modules.resource.utils.ChooseObject;
import modules.resource.utils.Find;
import utils.ComboBox;
import utils.Numbers;
import utils.Strings;
import utils.ValidateData;
import utils.languajes.Languaje;

public class ResourceUpdate {
	public static void updateResource() {
		switch (Main.resourceOption) {
		case Resource.BOOK:
			updateBook();
			break;
		case Resource.BOOKPRESENTATION:
			updateBookPresentation();
			break;
		case Resource.ARTICLE:
			updateArticle();
			break;
		case Resource.MOVIE:
			updateMovie();
			break;
		}
	}
	
	public static void updateBook() {
		String key = "";
		Boolean interrupt = false;
		int position = -1;
		String[] options = {Languaje.show("askKeyTitle"), 	// 0
							Languaje.show("askName"), 		// 1
							Languaje.show("askAuthor"), 	// 2
							Languaje.show("askThematic"), 	// 3
							Languaje.show("release"), 		// 4
							Languaje.show("askEditorial"), 	// 5
							Languaje.show("apply")			// 6
							};
		Book book = null;
		
		if (SingletonResource.book.isEmpty()) {
			JOptionPane.showMessageDialog(null, Languaje.show("noBooks"), "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			key = ChooseObject.book();
			position = Find.findBook(key);
			
			if (JOptionPane.showConfirmDialog(null, Languaje.show("update") + "? \n" + SingletonResource.book.get(position).toString()) == 0) {
				book = SingletonResource.book.get(position).copy();
				do {
					switch (ComboBox.comboBox(Languaje.show("option"), Languaje.show("option"), options)) {
						case 0:
							do {
								key = ValidateData.inputKey(Languaje.show("askKey"), Languaje.show("askKeyTitle"));
							} while(Find.findBook(key) != -1);
							book.setKey(key);
						break;
						case 1:
							book.setName(Strings.inputStringNoClose(Languaje.show("askName"), Languaje.show("askName")));
							break;
						case 2:
							book.setAuthor(Strings.inputStringNoClose(Languaje.show("askAuthor"), Languaje.show("askAuthor")));
							break;
						case 3:
							book.setThematic(Strings.inputStringNoClose(Languaje.show("askThematic"), Languaje.show("askThematic")));
							break;
						case 4:
							book.setRelease(ValidateData.inputDateBeforeToday(Languaje.show("askRelease"), Languaje.show("askRelease")));
							break;
						case 5:
							book.setEditorial(Strings.inputStringNoClose(Languaje.show("askEditorial"), Languaje.show("askEditorial")));
							break;
						default:
							interrupt = true;
							break;
					}
				} while(!interrupt);
				
				if (JOptionPane.showConfirmDialog(null, Languaje.show("applyChanges") + " \n\n" + book.toString()) == 0) {
					SingletonResource.book.set(position, book);
				}
			}
		}
	}
	
	public static void updateBookPresentation() {
		String key = "";
		int position = -1;
		Boolean interrupt = false;
		String[] options = {Languaje.show("askKeyTitle"), 		// 0
							Languaje.show("askName"), 			// 1
							Languaje.show("askAuthor"), 		// 2
							Languaje.show("askThematic"), 		// 3
							Languaje.show("release"), 			// 4
							Languaje.show("askEditorial"), 		// 5
							Languaje.show("presentationStart"),	// 6
							Languaje.show("presentationEnd"), 	// 7
							Languaje.show("apply")};			// 8
		BookPresentation book = null;
		
		if (SingletonResource.bookPresentation.isEmpty()) {
			JOptionPane.showMessageDialog(null, Languaje.show("noBooks"), "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			key = ChooseObject.bookPresentation();
			position = Find.findBookPresentation(key);
			if (JOptionPane.showConfirmDialog(null, Languaje.show("update") + "? \n" + SingletonResource.bookPresentation.get(position).toString()) == 0) {
				book = SingletonResource.bookPresentation.get(position).copy();
				do {
					switch (ComboBox.comboBox(Languaje.show("option"), Languaje.show("option"), options)) {
						case 0:
							do {
								key = ValidateData.inputKey(Languaje.show("askKey"), Languaje.show("askKeyTitle"));
							} while(Find.findBookPresentation(key) != -1);
							book.setKey(key);
						break;
						case 1:
							book.setName(Strings.inputStringNoClose(Languaje.show("askName"), Languaje.show("askName")));
							break;
						case 2:
							book.setAuthor(Strings.inputStringNoClose(Languaje.show("askAuthor"), Languaje.show("askAuthor")));
							break;
						case 3:
							book.setThematic(Strings.inputStringNoClose(Languaje.show("askThematic"), Languaje.show("askThematic")));
							break;
						case 4:
							book.setRelease(ValidateData.inputDateBeforeToday(Languaje.show("askRelease"), Languaje.show("askRelease")));
							break;
						case 5:
							book.setEditorial(Strings.inputStringNoClose(Languaje.show("askEditorial"), Languaje.show("askEditorial")));
							break;
						case 6:
							book.setPresentationStart(ValidateData.inputDateBeforeX(Languaje.show("askPresentationStart"), Languaje.show("askPresentationStart"), book.getPresentationEnd().toString()));
							break;
						case 7:
							book.setPresentationEnd(ValidateData.inputDateAfterX(Languaje.show("askPresentationEnd"), Languaje.show("askPresentationEnd"), book.getPresentationStart().toString()));
							break;
						default:
							interrupt = true;
							break;
					}
				} while(!interrupt);
				
				if (JOptionPane.showConfirmDialog(null, Languaje.show("applyChanges") + " \n\n" + book.toString()) == 0) {
					SingletonResource.bookPresentation.set(position, book);
				}
			}
		}
	}
	
	public static void updateArticle() {
		String key = "";
		int position = -1;
		Boolean interrupt = false;
		Article article = null;
		String[] options = {Languaje.show("askKeyTitle"), 		// 0
							Languaje.show("askName"), 			// 1
							Languaje.show("askAuthor"), 		// 2
							Languaje.show("askThematic"), 		// 3
							Languaje.show("release"), 			// 4
							Languaje.show("askMeans"),			// 5 
							Languaje.show("startShop"), 		// 6
							Languaje.show("endShop"), 			// 7 
							Languaje.show("purchaseDay"), 		// 8
							Languaje.show("askQuantity"), 		// 9
							Languaje.show("askUnitPrice"), 		// 10
							Languaje.show("apply") 				// 11
							};
		
		
		if (SingletonResource.article.isEmpty()) {
			JOptionPane.showMessageDialog(null, Languaje.show("noArticles"), "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			key = ChooseObject.article();
			position = Find.findArticle(key);
			if (JOptionPane.showConfirmDialog(null, Languaje.show("update") + "? \n" + SingletonResource.article.get(position).toString()) == 0) {
				article = SingletonResource.article.get(position).copy();
				do {
					switch (ComboBox.comboBox("Option", "Option", options)) {
					case 0:
						do {
							key = ValidateData.inputKey(Languaje.show("askKey"), Languaje.show("askKeyTitle"));
						} while(Find.findArticle(key) != -1);
						article.setKey(key);
					break;
					case 1:
						article.setName(Strings.inputStringNoClose(Languaje.show("askName"), Languaje.show("askName")));
						break;
					case 2:
						article.setAuthor(Strings.inputStringNoClose(Languaje.show("askAuthor"), Languaje.show("askAuthor")));
						break;
					case 3:
						article.setThematic(Strings.inputStringNoClose(Languaje.show("askThematic"), Languaje.show("askThematic")));
						break;
					case 4:
						article.setRelease(ValidateData.inputDateBeforeToday(Languaje.show("askRelease"), Languaje.show("askRelease")));
						break;
					case 5:
						article.setMeans(Strings.inputStringNoClose(Languaje.show("askMeans"), Languaje.show("askMeans")));
						break;
					case 6:
						article.setStartShop(ValidateData.inputDateBeforeX(Languaje.show("askStartShop"), Languaje.show("askStartShop"), article.getEndShop().toString()));
						break;
					case 7:
						article.setEndShop(ValidateData.inputDateAfterX(Languaje.show("askEndShop"), Languaje.show("askEndShop"), article.getStartShop().toString()));
						break;
					case 8:
						article.setPurchaseDay( ValidateData.inputDateBetweenXandY(Languaje.show("askPurchaseDay"), Languaje.show("askPurchaseDay"), article.getStartShop().toString(), article.getEndShop().toString()));
						break;
					case 9:
						article.setQuantity(Numbers.inputNumber(Languaje.show("askQuantity"), Languaje.show("askQuantity")));
						break;
					case 10:
						article.setUnitPrice(Numbers.inputFloat(Languaje.show("askUnitPrice"), Languaje.show("askUnitPrice")));
						break;
					default:
						interrupt = true;
						break;
					}
				} while (!interrupt);
				
				if (JOptionPane.showConfirmDialog(null, Languaje.show("applyChanges") + " \n\n" + article.toString()) == 0) {
					SingletonResource.article.set(position, article);
				}
			}
		}
	}
	
	public static void updateMovie() {
		String key = "";
		int position = -1;
		Boolean interrupt = false;
		Movie movie = null;
		
		String[] options = {Languaje.show("askKeyTitle"), 		// 0
							Languaje.show("askName"), 			// 1
							Languaje.show("askAuthor"), 		// 2
							Languaje.show("askThematic"), 		// 3
							Languaje.show("release"), 			// 4
							Languaje.show("presentationStart"),	// 5
							Languaje.show("presentationEnd"),	// 6
							Languaje.show("askPreview"), 		// 7
							Languaje.show("askPreview"), 		// 8
							Languaje.show("apply")				// 9
							};
		if (SingletonResource.movie.isEmpty()) {
			JOptionPane.showMessageDialog(null, Languaje.show("noMovies"), "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			key = ChooseObject.movie();
			position = Find.findMovie(key);
			if (JOptionPane.showConfirmDialog(null, Languaje.show("update") + "? \n" + SingletonResource.movie.get(position).toString()) == 0) {
				movie = SingletonResource.movie.get(position).copy();
				do {
					switch (ComboBox.comboBox(Languaje.show("option"), Languaje.show("option"), options)) {
					case 0:
						do {
							key = ValidateData.inputKey(Languaje.show("askKey"), Languaje.show("askKeyTitle"));
						} while(Find.findMovie(key) != -1);
						movie.setKey(key);
					break;
					case 1:
						movie.setName(Strings.inputStringNoClose(Languaje.show("askName"), Languaje.show("askName")));
						break;
					case 2:
						movie.setAuthor(Strings.inputStringNoClose(Languaje.show("askAuthor"), Languaje.show("askAuthor")));
						break;
					case 3:
						movie.setThematic(Strings.inputStringNoClose(Languaje.show("askThematic"), Languaje.show("askThematic")));
						break;
					case 4:
						movie.setRelease(ValidateData.inputDateBeforeToday(Languaje.show("askRelease"), Languaje.show("askRelease")));
						break;
					case 5:
						movie.setPresentationStart(ValidateData.inputDateBeforeX(Languaje.show("askPresentationStart"), Languaje.show("askPresentationStart"), movie.getPresentationEnd().toString()));
						break;
					case 6:
						movie.setPresentationEnd(ValidateData.inputDateAfterX(Languaje.show("askPresentationEnd"), Languaje.show("askPresentationEnd"), movie.getPresentationStart().toString()));
						break;
					case 7:
						movie.setPreview(ValidateData.inputDateBetweenXandY(Languaje.show("askPreview"), Languaje.show("askPreview"), movie.getPresentationStart().toString(), movie.getPresentationEnd().toString()));
						break;
					case 8:
						movie.setCompany(Strings.inputStringNoClose(Languaje.show("askCompany"), Languaje.show("askCompany")));
						break;
					default:
						interrupt = true;
						break;
					}
				} while (!interrupt);
				
				if (JOptionPane.showConfirmDialog(null, Languaje.show("applyChanges") + " \n\n" + movie.toString()) == 0) {
					SingletonResource.movie.set(position, movie);
				}
				
			}
		}
	}
}
