package modules.resource.classes.order;

import java.util.Comparator;

import modules.resource.classes.Resource;

public class OrderKey implements Comparator<Resource>{

	public int compare (Resource o1, Resource o2) {
		if(o1.getKey().compareTo(o2.getKey())>0)
			return 1;
		if(o1.getKey().compareTo(o2.getKey())<0)
			return -1;
		return 0;
	}
	
}
