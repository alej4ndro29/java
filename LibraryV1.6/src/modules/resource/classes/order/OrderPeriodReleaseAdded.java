package modules.resource.classes.order;

import java.util.Comparator;

import modules.resource.classes.Resource;

public class OrderPeriodReleaseAdded implements Comparator <Resource> {

	public int compare (Resource o1, Resource o2) { // U1 CLASE MADRE
		if(o1.getPeriodReleaseAdded()>o2.getPeriodReleaseAdded())
			return 1;
		if(o1.getPeriodReleaseAdded()<o2.getPeriodReleaseAdded())
			return -1;
		return 0;
	}
	
	
}
