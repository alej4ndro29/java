package modules.resource.classes.order;

import java.util.Collections;

import javax.swing.JOptionPane;

import main.Main;
import modules.resource.classes.Resource;
import modules.resource.classes.SingletonResource;
import utils.ButtonBox;
import utils.languajes.Languaje;

public class OrderMenu {
	
	public static void orderResource() {
		switch (Main.resourceOption) {
			case Resource.BOOK:
				orderBook();
				break;
			case Resource.BOOKPRESENTATION:
				orderBookPresentation();
				break;
			case Resource.ARTICLE:
				orderArticle();
				break;
			case Resource.MOVIE:
				orderMovie();
				break;
		}
	}
	
	
	public static void orderBook() {
		String[] options = {Languaje.show("askKeyTitle"), 
							Languaje.show("periodReleaseAdded"), 
							Languaje.show("release")};
		if (SingletonResource.book.isEmpty()) {
			JOptionPane.showMessageDialog(null, Languaje.show("noBooks"), "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			switch (ButtonBox.buttonsOptions("Option order:", "Order", options)) {
				case 0:
					Collections.sort(SingletonResource.book, new OrderKey());
					break;
				case 1:
					Collections.sort(SingletonResource.book, new OrderPeriodReleaseAdded());
					break;
				case 2:
					Collections.sort(SingletonResource.book, new OrderRelease());
					break;
				default:
					break;
			}
		}
	}
	
	public static void orderBookPresentation() {
		String[] options = {Languaje.show("askKeyTitle"), 
				Languaje.show("periodReleaseAdded"), 
				Languaje.show("release")};		
		if (SingletonResource.bookPresentation.isEmpty()) {
			JOptionPane.showMessageDialog(null, Languaje.show("noBooks"), "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			switch (ButtonBox.buttonsOptions(Languaje.show("optionOrder") + ":", Languaje.show("order"), options)) {
				case 0:
					Collections.sort(SingletonResource.bookPresentation, new OrderKey());
					break;
				case 1:
					Collections.sort(SingletonResource.bookPresentation, new OrderPeriodReleaseAdded());
					break;
				case 2:
					Collections.sort(SingletonResource.bookPresentation, new OrderRelease());
					break;
				default:
					break;
			}
		}
	}
	
	public static void orderArticle() {
		String[] options = {Languaje.show("askKeyTitle"), 
				Languaje.show("periodReleaseAdded"), 
				Languaje.show("release")};	
		if (SingletonResource.article.isEmpty()) {
			JOptionPane.showMessageDialog(null, Languaje.show("noArticles"), "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			switch (ButtonBox.buttonsOptions(Languaje.show("optionOrder") + ":", Languaje.show("order"), options)) {
				case 0:
					Collections.sort(SingletonResource.article, new OrderKey());
					break;
				case 1:
					Collections.sort(SingletonResource.article, new OrderPeriodReleaseAdded());
					break;
				case 2:
					Collections.sort(SingletonResource.article, new OrderRelease());
					break;
				default:
					break;
			}
		}
	}
	
	
	public static void orderMovie() {
		String[] options = {Languaje.show("askKeyTitle"), 
				Languaje.show("periodReleaseAdded"), 
				Languaje.show("release")};	
		if (SingletonResource.movie.isEmpty()) {
			JOptionPane.showMessageDialog(null, Languaje.show("noMovies"), "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			switch (ButtonBox.buttonsOptions(Languaje.show("optionOrder") + ":", Languaje.show("order"), options)) {
				case 0:
					Collections.sort(SingletonResource.movie, new OrderKey());
					break;
				case 1:
					Collections.sort(SingletonResource.movie, new OrderPeriodReleaseAdded());
					break;
				case 2:
					Collections.sort(SingletonResource.movie, new OrderRelease());
					break;
				default:
					break;
			}
		}
	}
}
