package modules.resource.classes;

import classes.dates.Dates;
import utils.languajes.Languaje;

public class Book extends Resource {
	
	private String editorial;
	
	// CONSTRUCTOR
	public Book(String key, String name, String author, String thematic, Dates release, String editorial) {
		super(key, name, author, thematic, release);
		this.editorial = editorial;

	}
	
	// CONSTRUCTOR VACÍO
	public Book() {
		super();
	}
	
	// ID_CONSTRUCTOR
	public Book(String key) {
		super(key);
	}
	
	// SETTERS 
	
	public void setEditorial (String editorial) {
		this.editorial = editorial;
	}
	
	
	// GETTERS
	
	public String getEditorial () {
		return editorial;		
	}

	
	public Book copy() {
		return new Book(this.getKey(), this.getName(), this.getAuthor(), 
						this.getThematic(), this.getRelease(), this.editorial);
	}
		
	// TOSTRING
	public String toString() {
		return 	"Book [" + Languaje.show("askKeyTitle") + " = "			+ getKey()		+ ", \n" +
				Languaje.show("askName")	 	+ " = "		+ getName()					+ ", \n" +
				Languaje.show("askAuthor")		+" = "		+ getAuthor() 				+ ", \n" +
				Languaje.show("askThematic") 	+ " = " 	+ getThematic() 			+ ", \n" +
				Languaje.show("release") 		+ " = " 	+ getRelease() 				+ ", \n" +
				Languaje.show("addedDate") 		+ " = " 	+ getAddedDate() 			+ ", \n" +
				Languaje.show("periodReleaseAdded") + " = " + getPeriodReleaseAdded()	+ ", \n" + 
				Languaje.show("askEditorial") + " = " 				+ editorial					+ 
				"]";
	}

	
}
