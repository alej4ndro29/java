package modules.resource.classes.dummies;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Random;
import main.Main;
import modules.resource.classes.Resource;
import utils.ButtonBox;
import utils.Numbers;
import utils.languajes.Languaje;

public class DumResource {
	public static void create() {
		String[] options = {Languaje.show("yes"), Languaje.show("no")};
		if(!Main.config.getDummies()) {
			Main.config.setDummies(ButtonBox.buttonsBoolean(Languaje.show("errDummies"), "Error", options));
			if(Main.config.getDummies())
				DumResource.create();
		} else {
			int repeats = Numbers.inputNumberPos(Languaje.show("howMany"), Languaje.show("howMany"));
			
			switch (Main.resourceOption) {
				case Resource.BOOK:
					DumBook.createDumBook(repeats);
					System.out.println("create " + repeats + " books");
					break;
				case Resource.BOOKPRESENTATION:
					DumBookPresentation.createBookPresentation(repeats);
					System.out.println("create " + repeats + " books");
					break;
				case Resource.ARTICLE:
					DumArticle.createDumArticle(repeats);
					System.out.println("create " + repeats + " articles");
					break;
				case Resource.MOVIE:
					DumMovie.createMovie(repeats);
					System.out.println("create " + repeats + " movies");
					break;
			}
		}
	}
	
	public static String randomKey() {
		NumberFormat formatter = new DecimalFormat("000");
		Random rand = new Random();
		String number = "";
		String letter = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		int num = rand.nextInt(1000);
		
		number = formatter.format(num);
		letter = String.valueOf(letter.charAt(rand.nextInt(25)));
		String key = number + "-" + letter;
		
		return key;
	}
	
}
