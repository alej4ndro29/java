package modules.resource.classes.dummies;

import java.util.Random;

import classes.dates.Dates;
import modules.resource.classes.BookPresentation;
import modules.resource.classes.SingletonResource;
import modules.resource.utils.Find;

public class DumBookPresentation {
	
	public static void createBookPresentation(int repeats) {
		BookPresentation book = null;
		Random random = new Random();
		Dates rel = new Dates();
		Dates start = new Dates();
		Dates end = new Dates();
		String key = "";
		
		String name 			= "Libro de muestra";
		String author 			= "Anónimo";
		String thematic			= "Didáctica";
		Dates release 			= rel;
		String editorial 		= "La editora";
		start.setToday();
		start.addDays(2);
		Dates presentationStart	= start;
		end.setToday();
		end.addDays(7);
		Dates presentationEnd 	= end;
		
		for (int i = 0; i < repeats; i++) {
			rel.setToday();
			rel.minusDays(random.nextInt(60) + 1);
			
			release = rel.copy();
			do {
				key = DumResource.randomKey();
			} while (Find.findBookPresentation(key) != -1);
			book = new BookPresentation(key, name, author, thematic, release, editorial, presentationStart, presentationEnd);
			SingletonResource.bookPresentation.add(book);
		}

	}
}
