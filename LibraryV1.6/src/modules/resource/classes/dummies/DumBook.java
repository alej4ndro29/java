package modules.resource.classes.dummies;

import java.util.Random;

import classes.dates.Dates;
import modules.resource.classes.Book;
import modules.resource.classes.SingletonResource;
import modules.resource.utils.Find;

public class DumBook {
	public static void createDumBook(int repeats) {
		
		Book book = null;		
		Random random = new Random();
		Dates rel = new Dates();
		String key = "";
		
		String name 			= "Libro de muestra";
		String author 			= "Anónimo";
		String thematic			= "Didáctica";
		Dates release 			= rel;
		String editorial 		= "La editora";
		
		for (int i = 0; i < repeats; i++) {
			rel.setToday();
			rel.minusDays(random.nextInt(60) + 1);
			
			release = rel.copy();
			do {
				key = DumResource.randomKey();
			} while (Find.findBook(key) != -1);
			
			book = new Book(key, name, author, thematic, release, editorial);
			
			SingletonResource.book.add(book);
			
		}
		
	}

}
