package modules.resource.classes.dummies;

import java.util.Random;

import classes.dates.Dates;
import modules.resource.classes.Movie;
import modules.resource.classes.SingletonResource;
import modules.resource.utils.Find;

public class DumMovie {

	public static void createMovie(int repeats) {
		Movie movie = null;		
		Random random = new Random();
		Dates rel = new Dates();
		Dates start = new Dates();
		Dates end = new Dates();
		Dates prev = new Dates();
		String key = "";
		
		String name 			= "Película de muestra";
		String author 			= "Anónimo";
		String thematic			= "Didáctica";
		Dates release 			= rel;
		start.setToday();
		start.addDays(2);
		Dates presentationStart	= start;
		end.setToday();
		end.addDays(9);
		Dates presentationEnd 	= end;
		prev.setToday();
		prev.addDays(5);
		Dates preview			= prev;
		String company 			= "Comp";
		
		for (int i = 0; i < repeats; i++) {
			rel.setToday();
			rel.minusDays(random.nextInt(60) + 1);
			
			release = rel.copy();
			do {
				key = DumResource.randomKey();
			} while(Find.findMovie(key) != -1);
		
			movie = new Movie(key, name, author, thematic, release, presentationStart, presentationEnd, preview, company);
			SingletonResource.movie.add(movie);
			
		}
		
	}
	
}
