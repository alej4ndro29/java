package modules.resource.classes;

import java.util.ArrayList;

import modules.resource.classes.Article;
import modules.resource.classes.Book;
import modules.resource.classes.BookPresentation;
import modules.resource.classes.Movie;

public class SingletonResource {
	public static ArrayList <Article> 			article;
	public static ArrayList <Book> 				book;
	public static ArrayList <BookPresentation> 	bookPresentation;
	public static ArrayList <Movie> 			movie;
}
