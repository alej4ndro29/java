package modules.resource.classes;

import classes.dates.Dates;
import utils.languajes.Languaje;

public class Movie extends Resource {

	private Dates 	presentationStart;
	private Dates	presentationEnd;
	private int 	presentationDays;
	private Dates 	preview;
	private String 	company;
	
	// CONSTRUCTOR
	public Movie(String key, String name, String author, String thematic, Dates release, Dates presentationStart, Dates presentationEnd, Dates preview, String company) {
		super(key, name, author, thematic, release);
		this.presentationStart	= presentationStart;
		this.presentationEnd 	= presentationEnd;
		this.presentationDays	= presentationStart.getDaysInterval(presentationEnd.toString());
		this.preview			= preview;		
		this.company 			= company;
	}
	
	// CONTRUCTOR VACÍO
	public Movie() {
		super();
	}
	
	// ID_CONTRUCTOR
	public Movie(String key) {
		super(key);
	}

	// SETTERS
	public void setPresentationStart(Dates presentationStart) {
		this.presentationStart = presentationStart;
		setPresentationDays();
	}
	
	public void setPresentationEnd(Dates presentationEnd) {
		this.presentationEnd = presentationEnd;
		setPresentationDays();
	}
	
	public void setPresentationDays() {
		this.presentationDays = presentationStart.getDaysInterval(presentationEnd.toString());
	}
	
	public void setPreview(Dates preview) {
		this.preview = preview;
	}
	
	public void setCompany(String company) {
		this.company = company;
	}
	
	
	// GETTERS
	public Dates getPresentationStart() {
		return presentationStart;
	}
	
	public Dates getPresentationEnd() {
		return presentationEnd;
	}
	
	public int getPresentationDays() {
		return presentationDays;
	}
	
	public Dates getPreview() {
		return preview;
	}
	
	public String getCompany() {
		return company;
	}
	
	
	public Movie copy() {
		return new Movie(this.getKey(), this.getName(), this.getAuthor(), 
						this.getThematic(), this.getRelease(), this.presentationStart, 
						this.presentationEnd, this.preview, this.company);
	}
	
	// TOSTRING
	public String toString() {
		return "Movie [" + Languaje.show("askKeyTitle") + " = "			+ getKey()		+ ", \n" +
				Languaje.show("askName")	 	+ " = "		+ getName()					+ ", \n" +
				Languaje.show("askAuthor")		+" = "		+ getAuthor() 				+ ", \n" +
				Languaje.show("askThematic") 	+ " = " 	+ getThematic() 			+ ", \n" +
				Languaje.show("release") 		+ " = " 	+ getRelease() 				+ ", \n" +
				Languaje.show("addedDate") 		+ " = " 	+ getAddedDate() 			+ ", \n" +
				Languaje.show("periodReleaseAdded") + " = " + getPeriodReleaseAdded()	+ ", \n" + 
				Languaje.show("presentationStart") 	+ " = "	+ presentationStart 		+ ", \n" +
				Languaje.show("presentationEnd") 	+ " = "	+ presentationEnd			+ ", \n" +
				Languaje.show("presentationDays") 	+ " = "	+ presentationDays			+ ", \n" +
				Languaje.show("askPreview") 		+ " = "	+ preview					+ ", \n" +
				Languaje.show("askCompany") 		+ " = " + company +
				"]";
	}
	
	
}
