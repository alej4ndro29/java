package utils;

import javax.swing.JOptionPane;

import main.Main;
import modules.login.classes.order.OrderMenuLogin;
import modules.login.utils.LoginMenu;
import modules.login.utils.crud.LoginCreate;
import modules.login.utils.crud.LoginDelete;
import modules.login.utils.crud.LoginRead;
import modules.login.utils.crud.LoginUpdate;
import modules.resource.classes.dummies.DumResource;
import modules.resource.classes.order.OrderMenu;
import modules.resource.utils.crud.ResourceCreate;
import modules.resource.utils.crud.ResourceDelete;
import modules.resource.utils.crud.ResourceRead;
import modules.resource.utils.crud.ResourceUpdate;
import utils.languajes.Languaje;

public class MainMenu {
	public static int guestMenu() {
			boolean exit = false;
			String[] options = {Languaje.show("book"),
					Languaje.show("bookPresentation"),
					Languaje.show("article"),
					Languaje.show("movie"),
					Languaje.show("config"),
					"Login"
					};
			
			String[] crudOptions = {Languaje.show("read"), 
									Languaje.show("order")};
			
			do {
				Main.resourceOption = ComboBox.comboBox(Languaje.show("option") + ":", Languaje.show("option"), options); // GUARDAR QUE SE HA ELEGIDO
				if (Main.resourceOption == -1) {
					exit = true;
				} else if ((Main.resourceOption == 4)) {
					System.out.println("config");
					classes.config.FunctionsConfig.configMenu();
					options[0] = Languaje.show("book");
					options[1] = Languaje.show("bookPresentation");
					options[2] = Languaje.show("article");
					options[3] = Languaje.show("movie");
					options[4] = Languaje.show("config");
				} else if (Main.resourceOption == 5){
					LoginMenu.loginMainMenu();
					return -2; // PARA CAMBIAR DE MENÚ
				} else {
					switch (ComboBox.comboBox(Languaje.show("option") + ":", Languaje.show("option"), crudOptions)) {
					case 0:
						ResourceRead.readResource();
						break;
					case 1:
						OrderMenu.orderResource();
						break;
					}
						
				}
		
			} while (!exit);
		
		return -1; // PARA SALIR DEL PROGRAMA
	}
	
	
	public static int adminMenu() {
		boolean exit = false;
		String[] options = {Languaje.show("book"),
				Languaje.show("bookPresentation"),
				Languaje.show("article"),
				Languaje.show("movie"),
				Languaje.show("config"),
				"Users",
				"Logout"
				};
		
		String[] crudOptions = { Languaje.show("create"),
				Languaje.show("read"), 
				Languaje.show("update"), 
				Languaje.show("dummies"),
				Languaje.show("order"),
				Languaje.show("delete") };
		
		do {
			Main.resourceOption = ComboBox.comboBox(Languaje.show("option") + ":", Languaje.show("option"), options); // GUARDAR QUE SE HA ELEGIDO
			if (Main.resourceOption == -1) {
				exit = true;
			} else if ((Main.resourceOption == 4)) {
				System.out.println("config");
				classes.config.FunctionsConfig.configMenu();
				options[0] = Languaje.show("book");
				options[1] = Languaje.show("bookPresentation");
				options[2] = Languaje.show("article");
				options[3] = Languaje.show("movie");
				options[4] = Languaje.show("config");
			} else if (Main.resourceOption == 5){
				adminUsersMenu();
			} else if (Main.resourceOption == 6){
				Main.config.setActiveUser();
				return -2; 
			} else {
				switch (ComboBox.comboBox(Languaje.show("option") + ":", Languaje.show("option"), crudOptions)) {
				case 0:
					ResourceCreate.createResource();
					break;
				case 1:
					ResourceRead.readResource();
					break;
				case 2:
					ResourceUpdate.updateResource();
					break;
				case 3:
					DumResource.create();
					break;
				case 4:
					OrderMenu.orderResource();
					break;
				case 5:
					ResourceDelete.deleteResource();
					break;
				}
					
			}
	
		} while (!exit);
	
		return -1; 
	}
	
	
	public static int clientMenu() {
		
		boolean exit = false;
		String[] options = {Languaje.show("book"),
				Languaje.show("bookPresentation"),
				Languaje.show("article"),
				Languaje.show("movie"),
				Languaje.show("config"),
				"My user",
				"Logout"
				};
		
		String[] crudOptions = { 
				Languaje.show("read"), 
				Languaje.show("update"), 
				Languaje.show("order"),
				};
		
		do {
			Main.resourceOption = ComboBox.comboBox(Languaje.show("option") + ":", Languaje.show("option"), options); // GUARDAR QUE SE HA ELEGIDO
			if (Main.resourceOption == -1) {
				exit = true;
			} else if ((Main.resourceOption == 4)) {
				System.out.println("config");
				classes.config.FunctionsConfig.configMenu();
				options[0] = Languaje.show("book");
				options[1] = Languaje.show("bookPresentation");
				options[2] = Languaje.show("article");
				options[3] = Languaje.show("movie");
				options[4] = Languaje.show("config");
			} else if (Main.resourceOption == 5){
				clientUserMenu();
				return -2;
			} else if (Main.resourceOption == 6){
				Main.config.setActiveUser();
				return -2; 
			} else {
				switch (ComboBox.comboBox(Languaje.show("option") + ":", Languaje.show("option"), crudOptions)) {
				case 0:
					ResourceRead.readResource();
					break;
				case 1:
					ResourceUpdate.updateResource();
					break;
				case 2:
					OrderMenu.orderResource();
					break;
				}
					
			}
	
		} while (!exit);
		
		return -1;
	}
	
	/////////////////////////////////////////////////////////////////
	
	public static void clientUserMenu() {
		String[] options = {"Read", "Update", "Delete"};
		
		switch (ButtonBox.buttonsOptions("Option:", "Option", options)) {
			case 0:
				JOptionPane.showMessageDialog(null, Main.config.getActiveUser().toString(), "Your user", JOptionPane.INFORMATION_MESSAGE);
				break;
			case 1:
				LoginUpdate.updateUser(Main.config.getActiveUser().getDNI());
				break;
			case 2:
				LoginDelete.deleteByClient();
				break;
		}
	}
	
	public static void adminUsersMenu() {
		String[] options = {"Create", "Read", "Update", "Order", "Delete"};
		
		switch (ButtonBox.buttonsOptions("Option:", "Option", options)) {
			case 0:
				LoginCreate.createUser();
				break;
			case 1:
				LoginRead.readUser();
				break;
			case 2:
				LoginUpdate.updateUser();
				break;
			case 3:
				OrderMenuLogin.orderUsers();
				break;
			case 4:
				LoginDelete.deleteUserByAdmin();
				break;
		}
		
	}
	
}
