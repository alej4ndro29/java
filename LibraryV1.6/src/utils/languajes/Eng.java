package utils.languajes;

import main.Main;

public class Eng {
	public static String message (String option) {
		String s = "";
		
		switch (option) {
			case "book":
				s = "Book";
				break;
			case "bookPresentation":
				s = "Book with presentation";
				break;
			case "article":
				s = "Article";
				break;
			case "movie":
				s = "Movie";
				break;
			case "config":
				s = "Configuration";
				break;
			case "option":
				s = "Option";
				break;
			case "languaje":
				s = "Languaje";
				break;
			case "errKeyRegistered":
				s = "Key already registered";
				break;
			case "askKey":
				s = "Key 000-A";
				break;
			case "askKeyTitle":
				s = "Key";
				break;
			case "askName":
				s = "Name";
				break;
			case "askAuthor":
				s = "Author";
				break;
			case "askThematic":
				s = "Thematic";
				break;
			case "askRelease":
				s = "Release " + Main.config.getFormatDate();
				break;
			case "askEditorial":
				s = "Editorial";
				break;
			case "askPresentationStart":
				s = "Start of the presentation " + Main.config.getFormatDate();
				break;
			case "presentationStart":
				s = "Start of the presentation";
				break;
			case "askPresentationEnd":
				s = "End of the presentation " + Main.config.getFormatDate();
				break;
			case "presentationEnd":
				s = "End of the presentation";
				break;
			case "askMeans":
				s = "Means";
				break;
			case "askStartShop":
				s = "Start of the purchase date " + Main.config.getFormatDate();
				break;
			case "askEndShop":
				s = "End of the purchase date " + Main.config.getFormatDate();
				break;
			case "askPurchaseDay":
				s = "Day of purchase " + Main.config.getFormatDate();
				break;
			case "askUnitPrice":
				s = "Price by unit";
				break;
			case "askQuantity":
				s = "Quantity";
				break;
			case "askPreview":
				s = "Preview";
				break;
			case "askCompany":
				s = "Company";
				break;
				
			case "create":
				s = "Create";
				break;
			case "read":
				s = "Read";
				break;
			case "update":
				s = "Update";
				break;
			case "delete":
				s = "Delete";
				break;
				
			case "chooseTitle":
				s = "-- Keys -- Name --";
				break;
				
			case "errNoExist":
				s = "Not exist";
				break;
				
			case "all":
				s = "All";
				break;
			case "one":
				s = "One";
				break;
			case "allOrOne":
				s = "All or one?";
				break;
				
			case "noBooks":
				s = "No books registered";
				break;
			case "noArticles":
				s = "No articles registered";
				break;
			case "noMovies":
				s = "No movies registered";
				break;
				
			case "dummies":
				s = "Dummies";
				break;
			case "formatDate":
				s = "Format date";
				break;
			case "currency":
				s = "Currency";
				break;
			case "enable":
				s = "Enable";
				break;
			case "disable":
				s = "Disable";
				break;
			case "euro":
				s = "Euro";
				break;
			case "dollar":
				s = "Dollar";
				break;
			case "periodReleaseAdded":
				s = "PeriodReleaseAdded";
				break;
			case "release":
				s = "Release";
				break;
			case "optionOrder":
				s = "Option to order";
				break;
			case "order":
				s = "Order";
				break;
			case "remove":
				s = "Remove";
				break;
			case "deleted":
				s = " deleted";
				break;
			case "object":
				s = "Object";
				break;
			case "apply":
				s = "Apply";
				break;
			case "applyChanges":
				s = "Apply changes?";
				break;
			case "startShop":
				s = "Start shop";
				break;
			case "endShop":
				s = "End shop";
				break;
			case "purchaseDay":
				s = "Purchase day";
				break;
			case "addedDate":
				s = "Added date";
				break;
			case "presentationDays":
				s = "Presentation days";
				break;
			case "daysShop":
				s = "Days shop";
				break;
			case "totalPrice":
				s = "Total price";
				break;
				
			case "yes":
				s = "Yes";
				break;
			case "no":
				s = "No";
				break;
			case "errDummies":
				s = ("Dummies are disabled," + "\n" + "do you want enable it?");
				break;
			case "howMany":
				s = "How many?";
				break;
			default:
				System.err.println("Eng-Bad option --> " + option);
				break;
			}
		
		return s;
	}
}
