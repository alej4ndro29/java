package utils;

public class Digits {
	public static int sumDigits (int number) { // SUMA DIGITOS
			int sum = 0, rest = 0;
			number = Math.abs(number);
			
			while (number != 0) {
				rest = number % 10;
				sum = sum + rest;
				number = number / 10;
			}
			
			return sum;
		}
	
	/////////////////////////
	
	public static int sumPairDigits (int number) { // SUMA DIGITOS PARES
		int sum = 0, rest = 0;
		number = Math.abs(number);
		
		while (number != 0) {
			rest = number % 10;
			if ((rest % 2) == 0 ) {
				sum = sum + rest;
			}
			number = number / 10;
		}
		
		return sum;
	}

	/////////////////////////
	
	public static int sumOddDigits (int number) { // SUMA DIGITOS IMPARES
		int sum = 0, rest = 0;
		number = Math.abs(number);
		
		while (number != 0) {
			rest = number % 10;
			if ((rest % 2) != 0 ) {
				sum = sum + rest;
			}
			number = number / 10;
		}
		
		return sum;
	}

	/////////////////////////
	
	public static int multiplyDigits (int number) { // MULTIPLICA DÍGITOS
			int product = 1, rest = 0;
			number = Math.abs(number);
			
			while (number != 0) {
				rest = number % 10;
				product = product * rest;
				number = number / 10;
			}
			
			return product;
		}
	
	/////////////////////////
	
	public static int multiplyPairDigits (int number) { // MULTIPLICA DÍGITOS PARES
		int product = 1, rest = 0;
		number = Math.abs(number);
		
		while (number != 0) {
			rest = number % 10;
			if ((rest % 2) == 0 ) {
				product = product * rest;
			}
			number = number / 10;
		}
		
		return product;
	}
	
	/////////////////////////
	
	public static int multiplyOddDigits (int number) { // MULTIPLICA DÍGITOS IMPARES
		int product = 1, rest = 0;
		number = Math.abs(number);
		
		while (number != 0) {
			rest = number % 10;
			if ((rest % 2) != 0 ) {
				product = product * rest;
			}
			number = number / 10;
		}
		return product;
	}

	/////////////////////////
	
	public static int extractDigit (int num, int digit) { // EXTRAER DÍGITO INDICADO
			num = Math.abs(num);
			int countDigits = Integer.toString(num).length();
			int rest = 0;
			
			if (countDigits < digit) {
				System.out.println(num + " don't have " + countDigits + " digits");
			} else {
				for (int i = 0 ; i < digit ; i++) {
					rest = (num % 10);
					num = (num / 10);
				}
			}
			
			return rest;
		}

	/////////////////////////
	
	public static int pairCont (int number) { // EXTRAE CANTIDAD DE DIGITOS PARES
			int cont = 0, rest = 0;
			number = Math.abs(number);
			
			while (number != 0) {
				rest = number % 10;
				if ((rest % 2) == 0 ) {
					cont++;
				}
				number = number / 10;
			}
			
			return cont;
		}
	
	/////////////////////////
	
	public static int oddCont (int number) { // EXTRAE CANTIDAD DE DIGITOS PARES
			int cont = 0, rest = 0;
			number = Math.abs(number);
			
			while (number != 0) {
				rest = number % 10;
				if ((rest % 2) != 0 ) {
					cont++;
				}
				number = number / 10;
			}
			
			return cont;
		}
	
	/////////////////////////
	
	public static int averageMultX (int number, int mult) { // MEDIA DE LOS DÍGITOS MÚLTIPLOS DE X 
			int average = 0, rest = 0, cont = 0;
			number = Math.abs(number);
			
			while (number != 0) {
				rest = (number % 10);
				number = (number / 10);
				
				if (rest % mult == 0) { 
					average = (average + rest);
					cont++;
				}
				
			}
			
			if (cont != 0) {
				average = (average / cont);
			}
			
			return average;
		}
	
	/////////////////////////
	
	public static int countDigitsPair (int number) { // CONTAR DÍGITOS PARES 
			int pairs = 0 , rest = 0;
			number = Math.abs(number);
			
			while (number != 0) {
				rest = (number % 10);
				number = (number / 10);
				if (rest % 2 == 0) {
					pairs++;
				}
			}
			
			return pairs;
		}

	/////////////////////////
	
	public static int countDigitsOdd (int number) { // CONTAR DÍGITOS PARES 
			int odds = 0 , rest = 0;
			number = Math.abs(number);
			
			while (number != 0) {
				rest = (number % 10);
				number = (number / 10);
				if (rest % 2 != 0) {
					odds++;
				}
			}
			
			return odds;
		}
}

