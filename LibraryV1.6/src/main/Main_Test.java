package main;


import javax.swing.JOptionPane;

import classes.config.Config;
import modules.login.classes.dummies.DumUser;
import modules.login.utils.FindUser;
import modules.resource.classes.dummies.DumArticle;
import modules.resource.classes.dummies.DumBook;
import modules.resource.classes.dummies.DumBookPresentation;
import modules.resource.classes.dummies.DumMovie;

public class Main_Test {
	public static Config config = new Config();
	public static void main(String[] args) {
		boolean check = false;
		DumBook.createDumBook(5);
		DumBookPresentation.createBookPresentation(5);
		DumArticle.createDumArticle(5);
		DumMovie.createMovie(5);
		
		DumUser.createDefaultAdmin();
		
		System.out.println(FindUser.FindPosition("12345678-A"));
		System.out.println(FindUser.FindType("12345678-A"));
		
		String[] options = {"Login", "Register", "Recover Pass"};
		
		do {
			check = true;
			String DNI = JOptionPane.showInputDialog("DNI:");
			if (FindUser.FindPosition(DNI) == -1) {
				JOptionPane.showMessageDialog(null, DNI + " not exist", "Error", JOptionPane.ERROR_MESSAGE);
				check = false;
			} else {
				System.out.println(FindUser.getUser(DNI));
				config.setActiveUser(FindUser.getUser(DNI));
				if (!config.getActiveUser().comparePassword(JOptionPane.showInputDialog("pass"))) {
					System.out.println("bad password");
					check = false;
					config.setActiveUser();
				}
					
			}
		} while (!check);
		
		System.out.println("exit");
		
		//ButtonBox.buttonsOptions("Login:", "Login", options);
		
	}
}
