package main;

import classes.config.Config;
import modules.login.classes.dummies.DumUser;
import modules.login.utils.FindUser;
import modules.resource.classes.dummies.DumArticle;
import modules.resource.classes.dummies.DumBook;
import modules.resource.classes.dummies.DumBookPresentation;
import modules.resource.classes.dummies.DumMovie;
import utils.MainMenu;

public class Main {
	
	public static int resourceOption = -1;
	public static Config config = new Config();
	
	public static void main(String[] args) {
		boolean check = false;
		int control = 0;
		
		DumBook.createDumBook(5);
		DumBookPresentation.createBookPresentation(5);
		DumArticle.createDumArticle(5);
		DumMovie.createMovie(5);
		
		DumUser.createDefaultAdmin();
		/*
		 * ADMINISTRADOR POR DEFECTO
		 * USUARIO: 12345678-A
		 * CONTRASEÑA: admin
		 */
		
//		config.setActiveUser(FindUser.getUser("12345678-A")); // CARGAR EL PROGRAMA CON ADMIN LOGUEADO 
		
		do {
			if (config.getActiveUser() == null) {
				control = MainMenu.guestMenu();
			}
			if (config.getActiveUser() != null && config.getActiveUser().getType() == "admin") {
				control = MainMenu.adminMenu();
			}
			if (config.getActiveUser() != null && config.getActiveUser().getType() == "client") {
				control = MainMenu.clientMenu();
			}
			
			if (control == -1) // SALIR DEL PROGRAMA
				check = true;
			
		} while (!check);
		
		
	}

}
